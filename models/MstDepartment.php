<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_department".
 *
 * @property int $id
 * @property string $department_name
 * @property string $department_desc
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 */
class MstDepartment extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'mst_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['department_name', 'department_desc'], 'required'],
            [['department_name'], 'unique', 'on' => 'insert'],
            [['is_active', 'created_by', 'updated_by'], 'integer'],
            [['department_name'], 'string', 'max' => 128],
            [['department_desc'], 'string', 'max' => 225],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'department_name' => 'Department Name',
            'department_desc' => 'Department Desc',
            'is_active' => 'Aktif',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_department')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }

    public static function listData() {
        $models = self::find()->select(['id', 'department_name'])->where(['is_active' => '1'])->orderBy('department_name')->all();
        if ($models !== null) {
            return $models;
        } else {
            return ['empty' => 'Empty'];
        }
    }

}
