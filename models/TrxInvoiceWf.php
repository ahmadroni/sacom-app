<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_invoice_wf".
 *
 * @property int $id
 * @property int $invoice_id
 * @property string $role_from
 * @property string $role_to
 * @property int $position
 * @property int $submited
 * @property int $ready
 *
 * @property TrxInvoice $invoice
 */
class TrxInvoiceWf extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_invoice_wf';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'role_from', 'role_to', 'position', 'submited', 'ready'], 'required'],
            [['invoice_id', 'position', 'submited', 'ready'], 'integer'],
            [['role_from', 'role_to'], 'string', 'max' => 64],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'role_from' => 'Role From',
            'role_to' => 'Role To',
            'position' => 'Position',
            'submited' => 'Submited',
            'ready' => 'Ready',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(TrxInvoice::className(), ['id' => 'invoice_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = $this->updated_at = time();
            //$this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            //$this->updated_at = time();
            //$this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_invoice_wf')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
