<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_callcenter_wf".
 *
 * @property int $id
 * @property int $callcenter_id
 * @property string $role_from
 * @property string $role_to
 * @property int $position
 * @property int $submited
 * @property int $ready
 *
 * @property TrxCallcenter $callcenter
 */
class TrxCallCenterWf extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_callcenter_wf';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'callcenter_id', 'position', 'submited', 'ready'], 'integer'],
            [['callcenter_id', 'role_from', 'role_to', 'position', 'submited', 'ready'], 'required'],
            [['role_from', 'role_to'], 'string', 'max' => 64],
            [['callcenter_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxCallcenter::className(), 'targetAttribute' => ['callcenter_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'callcenter_id' => 'Callcenter ID',
            'role_from' => 'Role From',
            'role_to' => 'Role To',
            'position' => 'Position',
            'submited' => 'Submited',
            'ready' => 'Ready',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallcenter()
    {
        return $this->hasOne(TrxCallCenter::className(), ['id' => 'callcenter_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = $this->updated_at = time();
            // $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            //$this->updated_at = time();
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_callcenter_wf')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function updateWorkflow($id, $role){
        $result = false;
        // current
        $model = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'role_from'=>$role])->one();
        if($model !== null){
            $model->submited = 1;
            $model->update();
            
            //next
            $next = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'role_from'=>$model->role_to])->one();
            if($next !== null){
                $next->ready = 1;
                $next->update();
            }
            $result = true;
        }        
        return $result;
    }
    
    public static function rejectWorkflow($id, $role){
        $result = false;
        // current
        $model = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'role_from'=>$role])->one();
        if($model !== null){
            $model->ready = 0;
            $model->update();
            
            //before
            $next = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'position'=>($model->position - 1)])->one();
            if($next !== null){
                $next->submited = 0;
                $next->update();
            }
            $result = true;
        }        
        return $result;
    }

    public static function addWorkflow($id, $role) {
        $check = TrxCallCenterWf::find()->where(['callcenter_id' => $id])->count();
        if ($check == 0) {
            $models = MstWorkflow::find()->where(['wf_type' => 'CALL-CENTER'])->orderBy('position')->all();
            foreach ($models as $model) {
                $item = new TrxCallCenterWf();
                $item->callcenter_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = ($model->role_from == $role ? 1 : 0);
                $item->save();
            }
        }   
    }
}
