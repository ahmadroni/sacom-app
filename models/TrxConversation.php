<?php

namespace app\models;

use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "trx_conversation".
 *
 * @property int $id
 * @property string $title
 * @property string $category
 * @property int $location_id
 * @property int $areas
 * @property string $types
 * @property string $risks
 * @property string $posibility
 * @property string $level
 * @property string $conversation
 * @property string $action_deal
 * @property int $status
 * @property int $is_active
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_by
 * @property int $updated_at
 *
 * @property MstArea $areas0
 * @property MstLocation $location
 * @property TrxConversationDetail[] $trxConversationDetails
 */
class TrxConversation extends \yii\db\ActiveRecord {

    const OPEN = "OPEN";
    const IN_PROGRESS = "IN_PROGRESS";
    const CLOSE = "CLOSE";

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'trx_conversation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title', 'category', 'location_id', 'areas', 'types', 'risks', 'posibility', 'risk_level', 'level', 'conversation'], 'required'], //,'action_deal','pic_comunicate'
            [['location_id', 'areas', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['appreciation','status'], 'safe'],
            [['title'], 'string', 'max' => 225],
            [['category', 'types', 'risks', 'posibility', 'level'], 'string', 'max' => 20],
            [['conversation', 'action_deal'], 'string', 'max' => 512],
            [['areas'], 'exist', 'skipOnError' => true, 'targetClass' => MstArea::className(), 'targetAttribute' => ['areas' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['created_at','updated_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Topik',
            'category' => 'Kategori',
            'location_id' => 'Lokasi',
            'areas' => 'Area',
            'types' => 'Tipe',
            'risks' => 'Resiko',
            'posibility' => 'Kemungkinan',
            'risk_level' => 'Tingkat Resiko',
            'level' => 'Kelas',
            'conversation' => 'Pembicaraan',
            'action_deal' => 'Tindakan Yang Segera Dilakukan',
            'pic_comunicate' => 'Pihak Yang Diajak Komunikasi',
            'appreciation' => 'Apreasiasi Terhadap Yang dilakukan',
            'status' => 'Is Close',
            'is_active' => 'Is Active',
            'created_by' => 'Post By',
            'created_at' => 'Tanggal',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    public function getRiskLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'risks'])
                        ->where(['type' => 'SafetyRisk']);
    }

    public function getPosibilityLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'posibility'])
                        ->where(['type' => 'SafetyPosibility']);
    }

    public function getCategoryLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'category'])
                        ->where(['type' => 'SafetyCategory']);
    }

    public function getTypeLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'types'])
                        ->where(['type' => 'SafetyType']);
    }

    public function getLevelLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'level'])
                        ->where(['type' => 'SafetyLevel']);
    }

    public function getLevelLookupMap() {
        $model = MstLookupMap::find()->where(['code_from' => $this->risks, 'code_to' => $this->posibility])->one();
        if ($model !== null)
            return $model;
        else
            return null;
    }

    public function getPostBy() {
        return $this->hasOne(MstUser::className(), ['id' => 'created_by']);
    }
    
    public function getPostUser()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'created_by']);
    }
    
    public function getPicComunicate() {
        return $this->hasOne(MstUser::className(), ['id' => 'pic_comunicate']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea() {
        return $this->hasOne(MstArea::className(), ['id' => 'areas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation() {
        return $this->hasOne(MstLocation::className(), ['id' => 'location_id']);
    }
    
    public function getIsClose(){
        return $this->status == self::CLOSE ? true : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxConversationDetails() {
        return $this->hasMany(TrxConversationDetail::className(), ['conversation_id' => 'id']);
    }

    public function getTrxConversationComments() {
        return $this->hasMany(TrxConversationComment::className(), ['conversation_id' => 'id']);
    }

    public function getCommentCount() {
        return $this->hasMany(TrxConversationComment::className(), ['conversation_id' => 'id'])->count();
    }

    public static function countByUser() {
        $result = 0;
        $userId = Yii::$app->user->identity->id;
        $model = TrxConversationDetail::find()->where(['for_user' => $userId, 'status' => self::OPEN])->count();
        if ($model > 0) {
            $result = $model;
        }
        return $result;
    }

    public static function listByUser() {
        $userId = Yii::$app->user->identity->id;
        $models = TrxConversation::find()
                ->joinWith(['trxConversationDetails d'])
                ->where(['d.for_user' => $userId, 'd.status' => self::OPEN])
                ->all();
        return $models;
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->status = self::OPEN;
            //$this->created_at = 
            $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public function getAllowComment() {
        $result = false;
        $userId = Yii::$app->user->identity->id;
        $model = TrxConversationDetail::find()->where(['conversation_id' => $this->id, 'for_user' => $userId])->count();
        if ($model > 0) {
            $result = true;
        }
        return $result;
    }
    
    public function getHasFollowUp(){
        $result = false;
        $userId = Yii::$app->user->identity->id;
        $model = TrxConversationDetail::find()->where(['conversation_id' => $this->id, 'for_user' => $userId, 'status'=>self::OPEN])->count();
        if ($model > 0) {
            $result = true;
        }
        return $result;
    }
    
    public static function responseStatus(){
        return [
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
        ];
    }
    
    public static function statusList(){
        return [
            ['code' => self::OPEN, 'name'=>'Open'],
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
            ['code' => self::CLOSE, 'name'=>'Close'],
        ];
    }

    public function getIsAuthor() {
        $result = false;
        $userId = Yii::$app->user->identity->id;
        if ($userId == $this->created_by) {
            $result = true;
        }
        return $result;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_conversation')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        $location = MstLocation::findOne($this->location_id);
        $userInput = ($this->postUser == null ? Yii::$app->user->identity->username : $this->postUser->full_name);
        $cc = explode(";", $location->safety_email);
        $resetLink = Url::to(['trx-safety/detail','id'=>$this->id], true);
        $receivers = $this->trxConversationDetails;
        foreach($receivers as $receiver){
            $receiverName = $receiver->forUserProfile == null ? $receiver->forUser->email : $receiver->forUserProfile->full_name;
            $receiverEmail = $receiver->forUser->email;
            Yii::$app->mailer->compose(['html' => 'safety-html', 'text' => 'safety-text'], 
                    ['resetLink' => $resetLink, 'receiver'=>$receiverName, 'userInput'=>$userInput, 'safetyType'=>'Safety'])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])
                    ->setCc($cc)
                    ->setTo($receiverEmail)
                    ->setSubject('Safety -  ' . $this->title)
                    ->send();
        }

        return false;
    }

}
