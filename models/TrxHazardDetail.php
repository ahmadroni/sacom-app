<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_hazard_detail".
 *
 * @property int $id
 * @property int $hazard_id
 * @property string $notes
 * @property int $for_user
 * @property int $agreement
 * @property string $do_date
 * @property string $status
 *
 * @property TrxHazard $hazard
 * @property MstUser $forUser
 */
class TrxHazardDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_hazard_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes', 'for_user', 'do_date', 'status'], 'required'],
            [['hazard_id', 'for_user'], 'integer'],
            [['do_date'], 'safe'],
            [['notes'], 'string', 'max' => 1024],
            [['status'], 'string', 'max' => 20],
            [['hazard_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxHazard::className(), 'targetAttribute' => ['hazard_id' => 'id']],
            [['for_user'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['for_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hazard_id' => 'Hazard',
            'notes' => 'Tindakan Antisipasi',
            'for_user' => 'Penanggung Jawab Area',
            'agreement' => 'Agreement',
            'do_date' => 'Batas Waktu',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHazard()
    {
        return $this->hasOne(TrxHazard::className(), ['id' => 'hazard_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForUser()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'for_user']);
    }

    public function getForUserProfile()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'for_user']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = $this->updated_at = time();
            // $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            //$this->updated_at = time();
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_hazard_detail')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
