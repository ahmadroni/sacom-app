<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_conversation_comment".
 *
 * @property int $id
 * @property int $conversation_id
 * @property string $content
 * @property string $status
 * @property int $created_at
 * @property int $created_by
 *
 * @property TrxConversation $conversation
 */
class TrxConversationComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_conversation_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['conversation_id', 'content', 'status'], 'required'],
            [['conversation_id', 'created_by'], 'integer'],
            [['content'], 'string', 'max' => 512],
            [['status'], 'string', 'max' => 20],
            [['conversation_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxConversation::className(), 'targetAttribute' => ['conversation_id' => 'id']],
            [['created_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'conversation_id' => 'Conversation ID',
            'content' => 'Follow Up',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversation()
    {
        return $this->hasOne(TrxConversation::className(), ['id' => 'conversation_id']);
    }
    
    public function getPostBy(){
        return $this->hasOne(MstUser::className(), ['id' => 'created_by']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = date('Y-m-d H:i:s');
            $this->created_by = Yii::$app->user->identity->id;
        } 
        return true;
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_conversation_comment')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
