<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_lookup".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $type
 * @property integer $position
 */
class MstLookup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_lookup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'type', 'position'], 'required'],
            [['position'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['code', 'type'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nama',
            'code' => 'Kode',
            'type' => 'Group',
            'position' => 'Urutan Data',
        ];
    }
    
    public static function listData($type){
        $models = self::find()->select(['code','name'])->where(['type'=>$type])->orderBy('position')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
    
    public static function itemData($type, $code){
        $model = self::find()->select(['code','name'])->where(['type'=>$type,'code'=>$code])->one();
        if($model !== null){
            return $model->name;
        }else {
            return "";
        }
    }
}
