<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_invoice".
 *
 * @property int $id
 * @property int $supplier_id
 * @property string $recieve_from
 * @property string $invoice_desc
 * @property string $status
 * @property string $category
 * @property int $is_active
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_by
 * @property int $updated_at
 *
 * @property MstSupplier $supplier
 * @property TrxInvoiceDetail[] $trxInvoiceDetails
 * @property TrxInvoiceHistory[] $trxInvoiceHistories
 * @property TrxInvoiceWf[] $trxInvoiceWfs
 */
class TrxInvoice extends \yii\db\ActiveRecord {
    const OPEN ='OPEN';
    const SENT_TO_TAX = 'SENT_TO_TAX';
    const TAX_VALIDATING = 'TAX_VALIDATING';
    const COMPLETED = 'COMPLETED';
    const INVOICE_PROBLEM = 'INVOICE_PROBLEM';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'trx_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['supplier_id', 'recieve_from', 'invoice_desc', 'category'], 'required'],
            [['supplier_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['recieve_from', 'status', 'category'], 'string', 'max' => 20],
            [['invoice_desc'], 'string', 'max' => 255],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstSupplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'supplier_id' => 'Supplier',
            'recieve_from' => 'Recieve From',
            'invoice_desc' => 'Invoice Desc',
            'status' => 'Status',
            'category' => 'Category',
            'is_active' => 'Is Active',
            'created_by' => 'Dibuat Oleh.',
            'created_at' => 'Tanggal',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier() {
        return $this->hasOne(MstSupplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxInvoiceDetails() {
        return $this->hasMany(TrxInvoiceDetail::className(), ['invoice_id' => 'id']);
    }

    public function getInvoiceHistory(){
        return $this->hasMany(TrxInvoiceHistory::className(),['invoice_id'=>'id']);
    }

    public function getDetailCount() {
        return $this->hasMany(TrxInvoiceDetail::className(), ['invoice_id' => 'id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxInvoiceHistories() {
        return $this->hasMany(TrxInvoiceHistory::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxInvoiceWfs() {
        return $this->hasMany(TrxInvoiceWf::className(), ['invoice_id' => 'id']);
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->status = 'OPEN';
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_invoice')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }

    public function getRecieveFrom() {
        return $this->hasOne(MstLookup::className(), ['code' => 'recieve_from'])
                        ->where(['type' => 'InvoiceReciept']);
    }

    public function getInvoiceStatus() {
        return $this->hasOne(MstLookup::className(), ['code' => 'status'])
                        ->where(['type' => 'InvoiceStatus']);
    }

    public function getInvoiceCategory() {
        return $this->hasOne(MstLookup::className(), ['code' => 'category'])
                        ->where(['type' => 'InvoiceCategory']);
    }

    public static function updateWorkflow($id, $role) {
        $result = false;
        // invoice
        $invoice = TrxInvoice::findOne($id);
        
        // current
        $model = TrxInvoiceWf::find()->where(['invoice_id' => $id, 'role_from' => $role,'submited'=>0,'ready'=>1])->one();
        if ($model !== null) {
            $model->submited = 1;
            $model->update();

            //next
            /*
            $models = MstWorkflow::find()->where(['wf_group' => 'APPROVAL','wf_type' => 'INVOICE', 'wf_name' => $invoice->category, 'role_from'=>$model->role_to])->orderBy('position')->all();
            foreach ($models as $model) {
                $item = new TrxInvoiceWf();
                $item->invoice_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = ($model->role_from == $role ? 1 : 0);
                $item->save();
            }
            */
            $next = TrxInvoiceWf::find()->where(['invoice_id' => $id, 'role_from' => $model->role_to, 'position'=>($model->position + 1)])->one();
            if ($next !== null) {
                $next->ready = 1;
                $next->update();
            }
            //workflow
            $workflow = MstWorkflow::invoiceWfPosition('APPROVAL','INVOICE',$invoice->category,$role,$model->position);
            // update status workflow
            $invoice->status = $workflow->status;
            $invoice->update();
            // add history
            TrxInvoice::addHistory($id, Yii::$app->user->identity->id,$workflow->status_label);
            $result = true;
        }
        return $result;
    }

    public static function rejectWorkflow($id, $role) {
        $result = false;
        // invoice
        $invoice = TrxInvoice::findOne($id);
        // current
        $models = TrxInvoiceWf::find()->where(['invoice_id' => $id])->all();
        if ($models !== null) {
            foreach ($models as $model){
                if($model->position==1){
                    $model->ready=1;
                }else {
                    $model->ready=0;
                }
                $model->submited=0;
                $model->update();
            }

            //before         
            /*
            $next = TrxInvoiceWf::find()->where(['invoice_id' => $id, 'position' => ($model->position - 1)])->one();
            if ($next !== null) {
                $next->submited = 0;
                $next->update();
            }
             * 
             */
            /*
            $models = MstWorkflow::find()->where(['wf_group' => 'REJECTION','wf_type' => 'INVOICE', 'wf_name' => $invoice->category, 'role_from'=>$role])->orderBy('position')->all();
            foreach ($models as $model) {
                $item = new TrxInvoiceWf();
                $item->invoice_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = ($model->role_from == $role ? 1 : 0);
                $item->save();
            }
            */
            $invoice->status = 'INVOICE_PROBLEM';
            $invoice->update();
            
            $result = true;
        }
        return $result;
    }

    public static function addWorkflow($id, $role) {
        $invoice = TrxInvoice::findOne($id);
        $check = TrxInvoiceWf::find()->where(['invoice_id' => $id])->count();
        if ($check == 0) {
            //$models = MstWorkflow::find()->where(['wf_group' => 'APPROVAL','wf_type' => 'INVOICE', 'wf_name' => $invoice->category, 'role_from'=>$role])->orderBy('position')->all();
            $models = MstWorkflow::find()->where(['wf_group' => 'APPROVAL','wf_type' => 'INVOICE', 'wf_name' => $invoice->category])->orderBy('position')->all();
            foreach ($models as $model) {
                $item = new TrxInvoiceWf();
                $item->invoice_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = ($model->role_from == $role ? 1 : 0);
                $item->save();
            }
        }
    }

    public static function addHistory($id, $userId, $status){
        $history = new TrxInvoiceHistory();
        $history->invoice_id=$id;
        $history->user_id = $userId;
        $history->status = $status;
        $history->save();
    }
    
    public static function addWorkflowByUserId($id, $userId) {
        $invoice = TrxInvoice::findOne($id);
        $check = TrxInvoiceWf::find()->where(['invoice_id' => $id])->count();
        if ($check == 0) {
            $query = (new \yii\db\Query)
                ->select(['t1.id'])
                ->from('mst_workflow t1')
                ->join('INNER JOIN','auth_assignment t2','t1.role_from=t2.item_name')
                ->where(['t1.wf_type' => 'INVOICE','t1.wf_name' => $invoice->category, 't2.user_id' => $userId])
                ->scalar();
            
            $model = MstWorkflow::invoiceByUserId('INVOICE', $invoice->category, $userId);
            if($model != null) {
                $item = new TrxInvoiceWf();
                $item->invoice_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = 0;
                $item->save();
            }
        }
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            TrxInvoiceHistory::deleteAll(['invoice_id'=>$this->id]);
            TrxInvoiceDetail::deleteAll(['invoice_id'=>$this->id]);
            TrxInvoiceProblem::deleteAll(['invoice_id'=>$this->id]);
            TrxInvoiceWf::deleteAll(['invoice_id'=>$this->id]);
            return true;
        }
        return false;
    }

    public static function trxAlert() {
        $role = Yii::$app->user->identity->role;
        $query = "SELECT 'Invoice' as link_name, '/trx-invoice/list' as link, 'fa fa-shopping-cart text-green' as icon, count(wf.id) as jumlah ". 
                "FROM trx_invoice_wf wf ".
                "WHERE wf.role_from='".$role."' AND wf.submited=0 AND wf.ready=1 ".
                "UNION ".
                "SELECT 'DD Manual' as link_name, '/trx-manual/list' as link, 'fa fa-book text-yellow' as icon, count(wf.id) as jumlah ". 
                "FROM trx_manual_wf wf ".
                "WHERE wf.role_from='".$role."' AND wf.submited=0 AND wf.ready=1 ".
                "UNION ".
                "SELECT 'Call Center' as link_name, '/trx-callcenter/list' as link,'fa fa-headphones text-aqua' as icon, count(wf.id) as jumlah ". 
                "FROM trx_callcenter_wf wf ".
                "WHERE wf.role_from='".$role."' AND wf.submited=0 AND wf.ready=1 ";
        return Yii::$app->db->createCommand($query)->queryAll();
    }
    
    public static function trxAlertTotal() {
        /*SELECT count(wf.id) as jumlah 
                FROM trx_invoice_wf wf
                INNER JOIN auth_assignment au on wf.role_from=au.item_name
                WHERE au.user_id AND wf.submited=0 AND wf.ready=1 */
        $role = Yii::$app->user->identity->role;
        $query ="SELECT SUM(t.jumlah)as total FROM ( ". 
                "SELECT count(wf.id) as jumlah ". 
                "FROM trx_invoice_wf wf ".
                "WHERE wf.role_from='".$role."' AND wf.submited=0 AND wf.ready=1 ".
                "UNION ".
                "SELECT count(wf.id) as jumlah ". 
                "FROM trx_manual_wf wf ".
                "WHERE wf.role_from='".$role."' AND wf.submited=0 AND wf.ready=1 ".
                "UNION ".
                "SELECT count(wf.id) as jumlah ". 
                "FROM trx_callcenter_wf wf ".
                "WHERE wf.role_from='".$role."' AND wf.submited=0 AND wf.ready=1 ".
                ") t";
        return Yii::$app->db->createCommand($query)->queryScalar();
    }
    
    public static function statusList(){
        return [
            ['code' => self::OPEN, 'name'=>'Open'],
            ['code' => self::SENT_TO_TAX, 'name'=>'Sent To Tax'],
            ['code' => self::TAX_VALIDATING, 'name'=>'Tax Validating'],
            ['code' => self::COMPLETED, 'name'=>'Completed'],
            ['code' => self::INVOICE_PROBLEM, 'name'=>'Invoice Problem'],
        ];
    }

}
