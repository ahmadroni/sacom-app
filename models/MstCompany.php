<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_company".
 *
 * @property int $id
 * @property string $company_name
 * @property string $company_desc
 * @property int $propinsi_id
 * @property int $kota_id
 * @property string $address
 * @property string $email
 * @property string $no_telp
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property MstKota $kota
 * @property MstPropinsi $propinsi
 * @property TrxCallcenter[] $trxCallcenters
 */
class MstCompany extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    const TRANSPORTER_TYPE ='Com01';
    const CALL_CENTER_TYPE = 'Com02';
    const COMMON_TYPE='Com03';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_type','company_name', 'company_desc', 'propinsi_id', 'kota_id', 'address'], 'required'],
            [['propinsi_id', 'kota_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['company_name'], 'string', 'max' => 128],
            [['company_desc'], 'string', 'max' => 225],
            [['address'], 'string', 'max' => 150],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],
            [['no_telp'], 'string', 'max' => 20],
            [['kota_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstKota::className(), 'targetAttribute' => ['kota_id' => 'id']],
            [['propinsi_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstPropinsi::className(), 'targetAttribute' => ['propinsi_id' => 'id']],
            [['created_at','updated_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_type' => 'Company Type',
            'company_name' => 'Company Name',
            'company_desc' => 'Company Desc',
            'propinsi_id' => 'Propinsi',
            'kota_id' => 'Kota',
            'address' => 'Alamat',
            'email' => 'Email',
            'no_telp' => 'No Telp',
            'is_active' => 'Aktif',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(MstLookup::className(), ['code' => 'company_type'])
                        ->where(['type' => 'CompanyType']);
    }
    
    public function getKota()
    {
        return $this->hasOne(MstKota::className(), ['id' => 'kota_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropinsi()
    {
        return $this->hasOne(MstPropinsi::className(), ['id' => 'propinsi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxCallcenters()
    {
        return $this->hasMany(TrxCallcenter::className(), ['company_id' => 'id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_company')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function listData(){
        $models = self::find()->select(['id','company_name'])->where(['is_active'=>'1'])->orderBy('company_name')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
    
    public static function listDataByType($type){

        $items= array();
        // set loop
        $models = self::find()->where(['is_active'=> self::STATUS_ACTIVE,'company_type'=>$type])->orderBy('company_name')->all();
        if ($models !== null) {
            foreach ($models as $model){
                $items[] =['id'=>$model->id, 'name'=>$model->company_name ." - ". $model->kota->nama_kota]; 
            }
        } 
        return $items;
    }
}
