<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxManual;

/**
 * TrxManualSeach represents the model behind the search form of `app\models\TrxManual`.
 */
class TrxManualSeach extends TrxManual
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'location_id', 'quantity', 'unit', 'is_active', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['number', 'reason', 'category', 'gold_category', 'description', 'jde_no', 'sold_to', 'deliver_to', 'transporter', 'vehicle_no', 'driver_name', 'notes', 'sender'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxManual::find()->orderBy(['created_at'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'quantity' => $this->quantity,
            'unit' => $this->unit,
            'amount' => $this->amount,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'gold_category', $this->gold_category])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'jde_no', $this->jde_no])
            ->andFilterWhere(['like', 'sold_to', $this->sold_to])
            ->andFilterWhere(['like', 'deliver_to', $this->deliver_to])
            ->andFilterWhere(['like', 'transporter', $this->transporter])
            ->andFilterWhere(['like', 'vehicle_no', $this->vehicle_no])
            ->andFilterWhere(['like', 'driver_name', $this->driver_name])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'sender', $this->sender]);

        return $dataProvider;
    }
    
    public function searchByRole($params, $role)
    {
        $query = TrxManual::find()
                ->joinWith(['manualWf wf'])
                ->where(['wf.role_from' =>$role, 'wf.submited'=>0,'wf.ready'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'quantity' => $this->quantity,
            'unit' => $this->unit,
            'amount' => $this->amount,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'gold_category', $this->gold_category])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'jde_no', $this->jde_no])
            ->andFilterWhere(['like', 'sold_to', $this->sold_to])
            ->andFilterWhere(['like', 'deliver_to', $this->deliver_to])
            ->andFilterWhere(['like', 'transporter', $this->transporter])
            ->andFilterWhere(['like', 'vehicle_no', $this->vehicle_no])
            ->andFilterWhere(['like', 'driver_name', $this->driver_name])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'sender', $this->sender]);

        return $dataProvider;
    }
}
