<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MstLocation;

/**
 * MstLocationSeach represents the model behind the search form of `app\models\MstLocation`.
 */
class MstLocationSearch extends MstLocation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['location_name', 'location_desc','parent_id','inovation_email','safety_email','enable_inovation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MstLocation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'is_active' => $this->is_active,
            'enable_inovation'=>$this->enable_inovation,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);
        /*
        $query->orWhere([
            ['created_by' => $this->created_by],
        ]);
        */
        
        $query->andFilterWhere(['like', 'location_name', $this->location_name])
            ->andFilterWhere(['like', 'location_desc', $this->location_desc])
            ->andFilterWhere(['like', 'inovation_email', $this->inovation_email])
            ->andFilterWhere(['like', 'safety_email', $this->safety_email]);

        return $dataProvider;
    }
}
