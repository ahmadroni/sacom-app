<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxInvoice;

/**
 * TrxInvoiceSearch represents the model behind the search form of `app\models\TrxInvoice`.
 */
class TrxInvoiceSearch extends TrxInvoice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'supplier_id', 'is_active'], 'integer'],
            [['recieve_from', 'invoice_desc', 'status', 'category','created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxInvoice::find()->orderBy(['created_at' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'recieve_from', $this->recieve_from])
            ->andFilterWhere(['like', 'invoice_desc', $this->invoice_desc])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'category', $this->category]);

        return $dataProvider;
    }
    
    public function searchByRole($params, $role)
    {
        $query = TrxInvoice::find()
                ->joinWith(['trxInvoiceWfs wf'])
                ->where(['wf.role_from' =>$role, 'wf.submited'=>0,'wf.ready'=>1])
                ->orderBy(['created_at' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
        ]);

        $query->andFilterWhere(['like', 'recieve_from', $this->recieve_from])
            ->andFilterWhere(['like', 'invoice_desc', $this->invoice_desc])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'category', $this->category]);

        return $dataProvider;
    }
    
    public function searchByUserId($params, $userId)
    {
        $query = TrxInvoice::find()
                ->joinWith(['trxInvoiceWfs wf'])
                ->where(['wf.role_from' =>$role, 'wf.submited'=>0,'wf.ready'=>1])
                ->orderBy(['created_at' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
        ]);

        $query->andFilterWhere(['like', 'recieve_from', $this->recieve_from])
            ->andFilterWhere(['like', 'invoice_desc', $this->invoice_desc])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'category', $this->category]);

        return $dataProvider;
    }
}
