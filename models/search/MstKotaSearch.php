<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MstKota;

/**
 * MstKotaSearch represents the model behind the search form of `app\models\MstKota`.
 */
class MstKotaSearch extends MstKota
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','propinsi_id'], 'integer'],
            [['propinsi_id','nama_kota'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MstKota::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'propinsi_id' =>$this->propinsi_id,
        ]);

        $query->andFilterWhere(['like', 'nama_kota', $this->nama_kota]);

        return $dataProvider;
    }
}
