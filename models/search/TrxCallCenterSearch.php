<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxCallCenter;

/**
 * TrxCallcenterSearch represents the model behind the search form of `app\models\TrxCallcenter`.
 */
class TrxCallCenterSearch extends TrxCallCenter
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'sales_pic', 'is_active'], 'integer'],
            [['jenis_panggilan', 'jenis_media', 'isi_pertanyaan', 'isi_jawaban','created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxCallcenter::find()->orderBy(['created_at'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'sales_pic' => $this->sales_pic,
            'is_active' => $this->is_active,            
            //'created_at' => $this->created_at,
            //'updated_by' => $this->updated_by,
            //'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'jenis_panggilan', $this->jenis_panggilan])
            ->andFilterWhere(['like', 'jenis_media', $this->jenis_media])
            ->andFilterWhere(['like', 'isi_pertanyaan', $this->isi_pertanyaan])
            ->andFilterWhere(['like', 'isi_jawaban', $this->isi_jawaban]);

        return $dataProvider;
    }
    
    public function searchByRole($params, $role)
    {
        $query = TrxCallcenter::find()
                //->joinWith(['callCenterWf wf'])
                ->orderBy(['created_at'=>SORT_DESC]);
                //->where(['wf.role_from' =>$role, 'wf.submited'=>0,'wf.ready'=>0]);
                //->where(['wf.role_from' =>$role]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'sales_pic' => $this->sales_pic,
            'is_active' => $this->is_active,
            //'created_by' => $this->created_by,
            //'created_at' => $this->created_at,
            //'updated_by' => $this->updated_by,
            //'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'jenis_panggilan', $this->jenis_panggilan])
            ->andFilterWhere(['like', 'jenis_media', $this->jenis_media])
            ->andFilterWhere(['>=', 'created_at', $this->created_at])
            ->andFilterWhere(['<', 'created_at', date('Y-m-d',strtotime($this->created_at.' +1 days'))])
            ->andFilterWhere(['like', 'isi_pertanyaan', $this->isi_pertanyaan])
            ->andFilterWhere(['like', 'isi_jawaban', $this->isi_jawaban]);

        return $dataProvider;
    }
}
