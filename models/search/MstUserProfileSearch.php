<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MstUserProfile;

/**
 * MstUserProfileSeach represents the model behind the search form of `app\models\MstUserProfile`.
 */
class MstUserProfileSeach extends MstUserProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'department_id', 'location_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['full_name', 'email', 'no_hp', 'no_telp', 'no_ext', 'computer_name', 'computer_ip', 'cs_group', 'im_team', 'position', 'manager_id', 'safety_admin', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MstUserProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'department_id' => $this->department_id,
            'location_id' => $this->location_id,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'no_hp', $this->no_hp])
            ->andFilterWhere(['like', 'no_telp', $this->no_telp])
            ->andFilterWhere(['like', 'no_ext', $this->no_ext])
            ->andFilterWhere(['like', 'computer_name', $this->computer_name])
            ->andFilterWhere(['like', 'computer_ip', $this->computer_ip])
            ->andFilterWhere(['like', 'cs_group', $this->cs_group])
            ->andFilterWhere(['like', 'im_team', $this->im_team])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'manager_id', $this->manager_id])
            ->andFilterWhere(['like', 'safety_admin', $this->safety_admin]);

        return $dataProvider;
    }
}
