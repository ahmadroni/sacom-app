<?php

namespace app\models\search;

use app\models\MstUser;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxInovation;

/**
 * TrxInovationSearch represents the model behind the search form of `app\models\TrxInovation`.
 */
class TrxInovationSearch extends TrxInovation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'location_id', 'is_manufacturing_process', 'is_product_feature', 'is_product_raw_material', 'is_design', 'is_currently_use', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['title', 'description', 'third_party', 'current_stage', 'invention_keyword', 'inventors', 'detail_description', 'invention_reason', 'any_addtional', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $userId)
    {
        $user = MstUser::findOne($userId);
        $query = TrxInovation::find()->joinWith('location');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // set user id
        $this->created_by=$userId;

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'is_manufacturing_process' => $this->is_manufacturing_process,
            'is_product_feature' => $this->is_product_feature,
            'is_product_raw_material' => $this->is_product_raw_material,
            'is_design' => $this->is_design,
            'is_currently_use' => $this->is_currently_use,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'third_party', $this->third_party])
            ->andFilterWhere(['like', 'current_stage', $this->current_stage])
            ->andFilterWhere(['like', 'invention_keyword', $this->invention_keyword])
            ->andFilterWhere(['like', 'inventors', $this->inventors])
            ->andFilterWhere(['like', 'detail_description', $this->detail_description])
            ->andFilterWhere(['like', 'invention_reason', $this->invention_reason])
            ->andFilterWhere(['like', 'any_addtional', $this->any_addtional]);
        $query->andFilterWhere(['or',['trx_inovation.created_by'=>$this->created_by]]);

        return $dataProvider;
    }
}
