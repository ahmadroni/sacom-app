<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MstWorkflow;

/**
 * MstWorkflowSearch represents the model behind the search form of `app\models\MstWorkflow`.
 */
class MstWorkflowSearch extends MstWorkflow {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'position'], 'integer'],
            [['wf_type', 'role_from', 'role_to', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MstWorkflow::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'wf_type', $this->wf_type])
                ->andFilterWhere(['like', 'wf_name', $this->wf_name])
                ->andFilterWhere(['like', 'role_from', $this->role_from])
                ->andFilterWhere(['like', 'role_to', $this->role_to])
                ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

}
