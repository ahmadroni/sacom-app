<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrxConversation;
use yii\data\Sort;

/**
 * TrxConversationSearch represents the model behind the search form of `app\models\TrxConversation`.
 */
class TrxConversationSearch extends TrxConversation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'location_id', 'is_active', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['title', 'category', 'areas', 'types', 'risks', 'posibility'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxConversation::find()->orderBy(['updated_at'=>SORT_DESC]);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>[
                'pageSize' => 5,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'areas', $this->areas])
            ->andFilterWhere(['like', 'types', $this->types])
            ->andFilterWhere(['like', 'risks', $this->risks])
            ->andFilterWhere(['like', 'posibility', $this->posibility]);

        return $dataProvider;
    }
    
    public function searchByUser($params)
    {
        $userId = Yii::$app->user->identity->id;
        $query = TrxConversation::find()
                ->where(['created_by'=>$userId])
                ->orderBy(['created_at'=>SORT_DESC, 'updated_at'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'location_id' => $this->location_id,
            'is_active' => $this->is_active,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'areas', $this->areas])
            ->andFilterWhere(['like', 'types', $this->types])
            ->andFilterWhere(['like', 'risks', $this->risks])
            ->andFilterWhere(['like', 'posibility', $this->posibility]);

        return $dataProvider;
    }
}
