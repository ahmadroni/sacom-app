<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_kota".
 *
 * @property integer $id
 * @property string $nama_kota
 * @property integer $propinsi_id
 *
 * @property MstKecamatan[] $mstKecamatans
 * @property MstPropinsi $propinsi
 */
class MstKota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_kota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_kota', 'propinsi_id'], 'required'],
            [['id', 'propinsi_id'], 'integer'],
            [['nama_kota'], 'string', 'max' => 150],
            [['propinsi_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstPropinsi::className(), 'targetAttribute' => ['propinsi_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kota' => 'Nama Kota',
            'propinsi_id' => 'Propinsi ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstKecamatans()
    {
        return $this->hasMany(MstKecamatan::className(), ['kota_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropinsi()
    {
        return $this->hasOne(MstPropinsi::className(), ['id' => 'propinsi_id']);
    }
    
    public static function listData($propId){
        $models = self::find()->select(['id','nama_kota'])->where(['propinsi_id'=>$propId])->orderBy('nama_kota')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
    
    public static function listDataAll(){
        $models = self::find()->select(['id','nama_kota'])->orderBy('nama_kota')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
}
