<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_invoice_problem".
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $user_id
 * @property string $category
 * @property int $created_at
 *
 * @property TrxInvoice $invoice
 * @property MstUser $user
 */
class TrxInvoiceProblem extends \yii\db\ActiveRecord
{
    const KONTRAK = 'KONTRAK';
    const FAKTUR_PAJAK ='FAKTUR_PAJAK';
    const GRN ='GRN';
    const INTERNAL ='INTERNAL';
    const INVOICE = 'INVOICE';
    const TRANSPORTER_RATE='TRANSPORTER_RATE';
    const OTHERS ='OTHERS';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_invoice_problem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'user_id', 'category', 'notes'], 'required'],
            [['invoice_id', 'user_id'], 'integer'],
            [['category'], 'string', 'max' => 20],
            [['notes'], 'string', 'max' => 128],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'user_id' => 'User ID',
            'category' => 'Category',
            'notes' => 'Notes',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(TrxInvoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'user_id']);
    }

    public static function listCategory(){
        return [
            ['id'=>self::KONTRAK,'name'=>'Kontrak'],
            ['id'=>self::FAKTUR_PAJAK,'name'=>'Faktur Pajak'],
            ['id'=>self::GRN,'name'=>'GRN'],
            ['id'=>self::INTERNAL,'name'=>'Internal'],
            ['id'=>self::INVOICE,'name'=>'Invoice'],
            ['id'=>self::TRANSPORTER_RATE,'name'=>'Transporter Rate'],
            ['id'=>self::OTHERS,'name'=>'Other'],
        ];
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = date('Y-m-d H:i:s');
        } 
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_invoice_problem')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
