<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_customer".
 *
 * @property int $id
 * @property string $customer_name
 * @property string $address
 * @property int $propinsi_id
 * @property int $kota_id
 * @property string $email
 * @property string $nomor_hp
 * @property string $nomor_telp
 * @property int $sales_pic
 * @property int $category_id
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property MstCompany $company
 * @property MstKota $kota
 * @property MstPropinsi $propinsi
 * @property MstUser $salesPic
 * @property TrxCallcenter[] $trxCallcenters
 */
class MstCustomer extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    public $nama_propinsi;
    
    public $nama_kota;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_name', 'address', 'propinsi_id', 'kota_id', 'email', 'sales_pic', 'category_id'], 'required','on'=>'insert'],
            [['propinsi_id', 'kota_id', 'sales_pic', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['customer_name', 'address'], 'string', 'max' => 225],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],
            [['nomor_hp', 'nomor_telp'], 'string', 'max' => 20],
            [['kota_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstKota::className(), 'targetAttribute' => ['kota_id' => 'id']],
            [['propinsi_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstPropinsi::className(), 'targetAttribute' => ['propinsi_id' => 'id']],
            [['sales_pic'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['sales_pic' => 'id']],
            [['nama_kota','nama_propinsi','category_id'],'safe'],
            [['created_at','updated_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_name' => 'Nama Customer',
            'address' => 'Alamat',
            'propinsi_id' => 'Propinsi',
            'kota_id' => 'Kota',
            'email' => 'Email',
            'nomor_hp' => 'Nomor Hp',
            'nomor_telp' => 'Nomor Telp',
            'sales_pic' => 'Sales Pic',
            'category_id' => 'Jenis Customer',
            'is_active' => 'Aktif',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKota()
    {
        return $this->hasOne(MstKota::className(), ['id' => 'kota_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropinsi()
    {
        return $this->hasOne(MstPropinsi::className(), ['id' => 'propinsi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesPic()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'sales_pic']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxCallcenters()
    {
        return $this->hasMany(TrxCallcenter::className(), ['customer_id' => 'id']);
    }
    
    public function getCategory() {
        return $this->hasOne(MstLookup::className(), ['code' => 'category_id'])
                        ->where(['type' => 'CustomerCategory']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_customer')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function listData(){
        $models = self::find()->select(['id','customer_name'])->where(['is_active'=>'1'])->orderBy('customer_name')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
    
    public static function listData2() {
        $items=array();
        $models = self::find()->where(['is_active'=> self::STATUS_ACTIVE])->orderBy('customer_name')->all();
        if ($models !== null) {
            foreach ($models as $model){
                $items[] =[ 'id'=>$model->id, 'name'=>$model->customer_name ." - ". $model->kota->nama_kota]; 
            }
        } 
        return $items;
    }
    
}
