<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_kecamatan".
 *
 * @property integer $id
 * @property string $nama_kecamatan
 * @property integer $kota_id
 *
 * @property MstDesa[] $mstDesas
 * @property MstKota $kota
 */
class MstKecamatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_kecamatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_kecamatan', 'kota_id'], 'required'],
            [['id', 'kota_id'], 'integer'],
            [['nama_kecamatan'], 'string', 'max' => 150],
            [['kota_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstKota::className(), 'targetAttribute' => ['kota_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kecamatan' => 'Nama Kecamatan',
            'kota_id' => 'Kota ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstDesas()
    {
        return $this->hasMany(MstDesa::className(), ['kecamatan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKota()
    {
        return $this->hasOne(MstKota::className(), ['id' => 'kota_id']);
    }
}
