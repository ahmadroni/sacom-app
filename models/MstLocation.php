<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_location".
 *
 * @property int $id
 * @property string $location_name
 * @property string $location_desc
 * @property string $inovation_email
 * @property string $safety_email
 * @property int $parent_id
 * @property int $is_active
 * @property int $enable_inovation
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 */
class MstLocation extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'mst_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['location_name', 'location_desc'], 'required'],
            [['location_name'], 'unique', 'on' => 'insert'],
            [['parent_id','is_active', 'created_by', 'updated_by','enable_inovation'], 'integer'],
            [['location_name'], 'string', 'max' => 128],
            [['location_desc','inovation_email','safety_email'], 'string', 'max' => 225],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'location_name' => 'Location Name',
            'location_desc' => 'Location Desc',
            'inovation_email' => 'Inovation Email',
            'safety_email' => 'Safety Email',
            'parent_id'=>'Parent Location',
            'is_active' => 'Aktif',
            'enable_inovation'=>'Enable In Inovation',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if((int)$this->parent_id==0)
            $this->parent_id=0;
        
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = time();
            // $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }
    
    public function getParent(){
        return $this->hasOne(MstLocation::className(), ['id' => 'parent_id']);
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_location')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }

    public static function listData() {
        $models = self::find()->select(['id', 'location_name'])->where(['is_active' => '1'])->orderBy('location_name')->all();
        if ($models !== null) {
            return $models;
        } else {
            return ['empty' => 'Empty'];
        }
    }

    public static function listData2() {
        $models = self::find()->select(['id', 'location_name'])->where('is_active=1 AND id !=6')->orderBy('location_name')->all();
        if ($models !== null) {
            return $models;
        } else {
            return ['empty' => 'Empty'];
        }
    }

    public static function listInovation() {
        $models = self::find()->select(['id', 'location_name'])->where('is_active=1 AND id !=6 AND enable_inovation=1')->orderBy('location_name')->all();
        if ($models !== null) {
            return $models;
        } else {
            return ['empty' => 'Empty'];
        }
    }

}
