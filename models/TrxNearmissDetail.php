<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_nearmiss_detail".
 *
 * @property int $id
 * @property int $nearmiss_id
 * @property string $notes
 * @property int $for_user
 * @property string $do_date
 * @property string $status
 *
 * @property TrxNearmiss $nearmiss
 * @property MstUser $forUser
 */
class TrxNearmissDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_nearmiss_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes', 'for_user', 'do_date', 'status'], 'required'],
            [['nearmiss_id', 'for_user'], 'integer'],
            [['do_date'], 'safe'],
            [['notes'], 'string', 'max' => 1024],
            [['status'], 'string', 'max' => 20],
            [['nearmiss_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxNearmiss::className(), 'targetAttribute' => ['nearmiss_id' => 'id']],
            [['for_user'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['for_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nearmiss_id' => 'Nearmiss ID',
            'notes' => 'Recomended Solution',
            'for_user' => 'PIC',
            'do_date' => 'Date Line',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNearmiss()
    {
        return $this->hasOne(TrxNearmiss::className(), ['id' => 'nearmiss_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForUser()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'for_user']);
    }

    public function getForUserProfile()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'for_user']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
        } else {
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_nearmiss_detail')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
