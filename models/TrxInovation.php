<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_inovation".
 *
 * @property int $id
 * @property string $title
 * @property int $location_id
 * @property int $is_manufacturing_process
 * @property int $is_product_feature
 * @property int $is_product_raw_material
 * @property int $is_design
 * @property int $is_currently_use
 * @property string $description
 * @property string $third_party
 * @property string $current_stage
 * @property string $invention_keyword
 * @property string $inventors
 * @property string $detail_description
 * @property string $invention_reason
 * @property string $any_addtional
 * @property int $is_active
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class TrxInovation extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_inovation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'location_id', 'description', 'third_party', 'current_stage', 'invention_keyword', 'inventors', 'detail_description', 'invention_reason', 'any_addtional'], 'required'],
            [['location_id', 'is_manufacturing_process', 'is_product_feature', 'is_product_raw_material', 'is_design', 'is_currently_use', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['detail_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'third_party', 'invention_keyword', 'inventors', 'invention_reason', 'any_addtional'], 'string', 'max' => 128],
            [['description'], 'string', 'max' => 512],
            [['current_stage'], 'string', 'max' => 62],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Judul Inovasi Anda',//'Title of your invention',
            'location_id' => 'Region',
            'is_manufacturing_process' => 'Apakah Inovasi ini berhubungan dengan proses manufaktur',//'This invention relates to a manufacturing process',
            'is_product_feature' =>'Apakah Inovasi ini berhubungan dengan fitur produk' ,//'This invention relates to a product feature',
            'is_product_raw_material' => 'Apakah Inovasi ini berhubungan dengan bahan baku',//'This invention relates to a product raw material',
            'is_design' => 'Apakah Inovasi ini berhubungan dengan desain',//'This invention relates to a design',
            'is_currently_use' => 'Apakah Inovasi ini sedang digunakan',//'This invention is currently use',
            'description' => 'Deskripsi singkat dari penemuan ini',//'Brief description of the invention',
            'third_party' => 'Siapa lagi yang mengetahui tentang ini, perusahaan atau pihak ketiga? Tolong detailkan',//'Who else the company or a third party know about this? Please give details',
            'current_stage' => 'Tahap Pengembangan Saat Ini',//'Current Stage of Development',
            'invention_keyword' => 'Kata kunci inovasi',//'Invention Keywords',
            'inventors' => 'Penemu',//'Inventors',
            'detail_description' =>'Penjelasan rinci tentang inovasi',// 'Detailed Description of the Invention',
            'invention_reason' =>'Jelaskan mengapa inovasi ini memberikan nilai. Contoh: biaya rendah, tingkatkan produk, gunakan data keuangan nyata jika Tersedia',// 'Explain Why this Invention Offers Value. Example: Low costs, Improve product, Use real data of financials if Availabel',
            'any_addtional' =>'Rincian tambahan mengenai inovasi Ini',//'Any Addtional Details Regarding This Invention',
            'is_active' => 'Aktif',//'Is Active',
            'created_at' => 'Tanggal Submit Inovasi','Date Of Invention',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getLocation()
    {
        return $this->hasOne(MstLocation::className(), ['id' => 'location_id']);
    }
    
    public function getPostUser()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'created_by']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_inovation')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        $location = MstLocation::findOne($this->location_id);
        $userInput = ($this->postUser == null ? Yii::$app->user->identity->username : $this->postUser->full_name);
        $receivers = explode(";", $location->inovation_email);
        foreach($receivers as $receiver){
            Yii::$app->mailer->compose(['html' => 'inovation-html', 'text' => 'inovation-text'], 
                    ['inovationId' => $this->id, 'receiver'=>$receiver, 'userInput'=>$userInput])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])
                    ->setTo($receiver)
                    ->setSubject('Inovation From ' . $userInput)
                    ->send();
        }

        return false;
    }
}
