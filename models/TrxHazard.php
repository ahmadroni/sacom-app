<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_hazard".
 *
 * @property int $id
 * @property string $title
 * @property int $location_id
 * @property int $area_id
 * @property string $area_other
 * @property string $risk
 * @property string $posibility
 * @property string $risk_level
 * @property string $description
 * @property string $status
 * @property int $is_active
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_by
 * @property int $updated_at
 *
 * @property MstArea $area
 * @property MstLocation $location
 * @property TrxHazardDetail[] $trxHazardDetails
 */
class TrxHazard extends \yii\db\ActiveRecord
{
    const OPEN = "OPEN";
    const IN_PROGRESS = "IN_PROGRESS";
    const CLOSE = "CLOSE";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_hazard';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'location_id', 'area_id', 'area_other', 'risk', 'posibility', 'risk_level', 'description'], 'required'],
            [['location_id', 'area_id', 'is_active', 'created_by',  'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['area_other'], 'string', 'max' => 45],
            [['risk', 'posibility', 'risk_level', 'status'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 225],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstArea::className(), 'targetAttribute' => ['area_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['risk_level'],'safe'],
            [['created_at','updated_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Topik',
            'location_id' => 'Lokasi',
            'area_id' => 'Area',
            'area_other' => 'Detail Area',
            'risk' => 'Konsekuensi',
            'posibility' => 'Kemungkinan',
            'risk_level' => 'Tingkat Resiko',
            'description' => 'Penjelasan Singkat',
            'status' => 'Status',
            'is_active' => 'Is Active',
            'created_by' => 'Post By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(MstArea::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(MstLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxHazardDetails()
    {
        return $this->hasMany(TrxHazardDetail::className(), ['hazard_id' => 'id']);
    }
    
    public function getTrxHazardComments() {
        return $this->hasMany(TrxHazardComment::className(), ['hazard_id' => 'id']);
    }
    
    public function getPostBy(){
        return $this->hasOne(MstUser::className(), ['id' => 'created_by']);
    }
    
    public function getPostUser()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'created_by']);
    }
    
    
    public function getPosibilityLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'posibility'])
                        ->where(['type' => 'HazardPosibility']);
    }
    
    public function getRiskLookup() {
        return $this->hasOne(MstLookup::className(), ['code' => 'risk'])
                        ->where(['type' => 'HazardRisk']);
    }
    
    public function getLevelLookupMap() {
        $model = MstLookupMap::find()->where(['code_from' => $this->risk, 'code_to' => $this->posibility])->one();
        if ($model !== null)
            return $model;
        else
            return null;
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = 
            $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
            $this->status = self::OPEN;
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_hazard')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function statusList(){
        return [
            ['code' => self::OPEN, 'name'=>'Open'],
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
            ['code' => self::CLOSE, 'name'=>'Close'],
        ];
    }
    
    public static function responseStatus(){
        return [
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
        ];
    }
    
    public function getAllowComment() {
        $result = false;
        $userId = Yii::$app->user->identity->id;
        $model = TrxHazardDetail::find()->where(['hazard_id' => $this->id, 'for_user' => $userId])->count();
        if ($model > 0) {
            $result = true;
        }
        return $result;
    }
    
    public function getHasFollowUp(){
        $result = false;
        $userId = Yii::$app->user->identity->id;
        $model = TrxHazardDetail::find()->where(['hazard_id' => $this->id, 'for_user' => $userId, 'status'=>self::OPEN])->count();
        if ($model > 0) {
            $result = true;
        }
        return $result;
    }
    
    public function getIsAuthor() {
        $result = false;
        $userId = Yii::$app->user->identity->id;
        if ($userId == $this->created_by) {
            $result = true;
        }
        return $result;
    }
    
    public function getCommentCount() {
        return $this->hasMany(TrxHazardComment::className(), ['hazard_id' => 'id'])->count();
    }
    
    public function getIsClose(){
        return $this->status == self::CLOSE ? true : false;
    }
    
    public static function countByUser() {
        $result = 0;
        $userId = Yii::$app->user->identity->id;
        $model = TrxHazardDetail::find()->where(['for_user' => $userId, 'status' => self::OPEN])->count();
        if ($model > 0) {
            $result = $model;
        }
        return $result;
    }

    public static function listByUser() {
        $userId = Yii::$app->user->identity->id;
        $models = TrxHazard::find()
                ->joinWith(['trxHazardDetails d'])
                ->where(['d.for_user' => $userId, 'd.status' => self::OPEN])
                ->all();
        return $models;
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        $location = MstLocation::findOne($this->location_id);
        $userInput = ($this->postUser == null ? Yii::$app->user->identity->username : $this->postUser->full_name);
        $cc = explode(";", $location->safety_email);
        $resetLink = Url::to(['trx-hazard/detail','id'=>$this->id], true);
        $receivers = $this->trxHazardDetails;
        foreach($receivers as $receiver){
            $receiverName = $receiver->forUserProfile == null ? $receiver->forUser->email : $receiver->forUserProfile->full_name;
            $receiverEmail = $receiver->forUser->email;
            Yii::$app->mailer->compose(['html' => 'safety-html', 'text' => 'safety-text'], 
                    ['resetLink' => $resetLink, 'receiver'=>$receiverName, 'userInput'=>$userInput, 'safetyType'=>'Hazard'])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['supportName']])
                    ->setCc($cc)
                    ->setTo($receiverEmail)
                    ->setSubject('Hazard -  ' . $this->title)
                    ->send();
        }

        return false;
    }
}
