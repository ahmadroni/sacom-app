<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_manual_wf".
 *
 * @property int $id
 * @property int $manual_id
 * @property string $role_from
 * @property string $role_to
 * @property int $position
 * @property int $submited
 * @property int $ready
 *
 * @property TrxManual $manual
 */
class TrxManualWf extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_manual_wf';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'manual_id', 'position', 'submited', 'ready'], 'integer'],
            [['manual_id', 'role_from', 'role_to', 'position', 'submited', 'ready'], 'required'],
            [['role_from', 'role_to'], 'string', 'max' => 64],
            [['manual_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxManual::className(), 'targetAttribute' => ['manual_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manual_id' => 'Manual ID',
            'role_from' => 'Role From',
            'role_to' => 'Role To',
            'position' => 'Position',
            'submited' => 'Submited',
            'ready' => 'Ready',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManual()
    {
        return $this->hasOne(TrxManual::className(), ['id' => 'manual_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = $this->updated_at = time();
            // $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            //$this->updated_at = time();
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_manual_wf')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
