<?php

namespace app\models;

use Yii;

/**
 * This is the model class for_user table "trx_conversation_detail".
 *
 * @property int $id
 * @property int $conversation_id
 * @property string $notes
 * @property int $for_user
 * @property int $agreement
 * @property string $dead_line
 * @property string $status
 *
 * @property TrxConversation $conversation
 * @property MstUser $for_user0
 */
class TrxConversationDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_conversation_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes', 'for_user', 'status','dead_line','agreement'], 'required'],
            [['conversation_id', 'for_user', 'agreement'], 'integer'],
            [['dead_line'], 'safe'],
            [['notes'], 'string', 'max' => 1024],
            [['status'], 'string', 'max' => 20],
            [['conversation_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxConversation::className(), 'targetAttribute' => ['conversation_id' => 'id']],
            [['for_user'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['for_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'conversation_id' => 'Conversation ID',
            'notes' => 'Follow Up',
            'for_user' => 'Orang Yang Bertanggung Jawab',
            'agreement' => 'Setuju Dilakukan',
            'dead_line' => 'Dead Line',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConversation()
    {
        return $this->hasOne(TrxConversation::className(), ['id' => 'conversation_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForUser()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'for_user']);
    }

    public function getForUserProfile()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'for_user']);
    }
    
    public function befor_usereSave($insert) {
        if (!parent::befor_usereSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
        } else {
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_conversation_detail')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
