<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "mst_user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TrxConversationDetail[] $trxConversationDetails
 */
class MstUser extends ActiveRecord implements IdentityInterface {

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;

    public $password;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'mst_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['username', 'password', 'email'],'required','on'=>'insert'],
            [['id','username','status'],'required','on'=>'disabled'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => 'app\models\MstUser', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            //['email', 'unique', 'targetClass' => 'app\models\MstUser', 'message' => 'This email address has already been taken.'],
            //['password', 'required'],
            //['password', 'string', 'min' => 6],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
            // password save
            [['password'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'User ID',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getMstUserProfile() {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxConversationDetails() {
        return $this->hasMany(TrxConversationDetail::className(), ['for' => 'id']);
    }

    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }        
        //$this->setPassword($this->password);
        $this->generateAuthKey();
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = time();      
        } else {
            $this->updated_at = time();
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_user')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }

    public static function listConversation() {
        $items = array();
        $models = self::find()->where(['not in', 'id', [Yii::$app->user->identity->id]])->all();
        if ($models !== null) {
            foreach ($models as $model){
                $items[] =[ 'id'=>$model->id, 'username'=>$model->fullname]; 
            }
        }
        return $items;
    }

    public static function listData() {
        $items = array();
        $models = self::find()->where(['status'=> self::STATUS_ACTIVE])->orderBy('username')->all();
        if ($models !== null) {
            foreach ($models as $model){
                $items[] =[ 'id'=>$model->id, 'username'=>$model->fullname]; 
            }
        } 
        return $items;
    }
    
    public static function listDataDefault($id) {
        $items = array();
        $current = self::findOne($id);
        $items[] =[ 'id'=>$current->id, 'username'=>$current->fullname]; 
        
        // set loop
        $models = self::find()
                ->andWhere(['and','status',self::STATUS_ACTIVE])
                ->andWhere(['not in','id',$id])->orderBy('username')->all();
        if ($models !== null) {
            foreach ($models as $model){
                $items[] =[ 'id'=>$model->id, 'username'=>$model->fullname]; 
            }
        } 
        return $items;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getRole() {
        $result = "";
        $roleModel = Yii::$app->db
                ->createCommand("select * from auth_assignment where user_id='" . $this->id . "'")
                ->queryOne();
        $result = $roleModel['item_name'];
        return $result;
    }
    
    public function getListRole() {
        $result = "";
        $roleModel = Yii::$app->db
                ->createCommand("select * from auth_assignment where user_id='" . $this->id . "'")
                ->queryAll();
        foreach($roleModel as $row){
            $result .= $row['item_name'].", ";
        }
        return $result;
    }
    
    public function getFullname(){
        return $this->mstUserProfile == null ? $this->username : $this->mstUserProfile->full_name;
    }
    
    public function getImage(){
        return $this->mstUserProfile == null ? "/profile/user2-160x160.jpg" : 
                ($this->mstUserProfile->image_path==null ? "/profile/user2-160x160.jpg" : $this->mstUserProfile->image_path);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

}
