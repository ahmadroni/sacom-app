<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_workflow".
 *
 * @property int $id
 * @property string $wf_group
 * @property string $wf_type
 * @property string $wf_name
 * @property string $role_from
 * @property string $role_to
 * @property string $status
 * @property string $status_label
 * @property int $position
 */
class MstWorkflow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_workflow';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wf_group','wf_type', 'wf_name', 'role_from', 'status','status_label', 'position'], 'required'],
            [['position'], 'integer'],
            [['wf_type', 'role_from', 'role_to'], 'string', 'max' => 64],
            [['wf_name','status_label'], 'string', 'max' => 45],
            [['status'], 'string', 'max' => 20],
        ];
    }
    
    public static function labelProses($type,$role){
        $model = self::find()->where(['wf_type' => $type, 'role_from' => $role])->one();
        $label = '';
        if($model !== null){
            $label = $model->status_label;
        }
        return $label;
    }
    
    public static function invoiceWf($type,$name,$role){
        $model = self::find()->where(['wf_type' => $type,'wf_name' => $name, 'role_from' => $role])->one();
        if($model !== null){
            return $model;
        }else {
            return null;
        }
    }
    
    public static function invoiceWfGroup($group,$type,$name,$role){
        $model = self::find()->where(['wf_group'=>$group,'wf_type' => $type,'wf_name' => $name, 'role_from' => $role])->one();
        if($model !== null){
            return $model;
        }else {
            return null;
        }
    }
    public static function invoiceWfPosition($group,$type,$name,$role, $position){
        $model = self::find()->where(['wf_group'=>$group,'wf_type' => $type,'wf_name' => $name, 'role_from' => $role,'position'=>$position])->one();
        if($model !== null){
            return $model;
        }else {
            return null;
        }
    }
    
    public static function invoiceByUserId($type,$name,$userId){
        $query = (new \yii\db\Query)
                ->select(['t1.id'])
                ->from('mst_workflow t1')
                ->join('INNER JOIN','auth_assignment t2','t1.role_from=t2.item_name')
                ->where(['t1.wf_type' => $type,'t1.wf_name' => $name, 't2.user_id' => $userId])
                ->scalar();
        if($query!=null || $query!=0){
            //$model = self::find()->where(['wf_type' => $type,'wf_name' => $name, 'role_from' => $role])->one();
            $model = self::findOne($query);
            if($model !== null){
                return $model;
            }else {
                return null;
            }
        }else{
            return null;
        }        
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wf_group' => 'Workflow Group',
            'wf_type' => 'Workflow Type',
            'wf_name' => 'Workflow Name',
            'role_from' => 'Role From',
            'role_to' => 'Role To',
            'status' => 'Status',
            'status_label' => 'Label',
            'position' => 'Position',
        ];
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if(empty($this->role_to)){
            $this->role_to = 'END';
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
        } 
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_workflow')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
