<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_user_profile".
 *
 * @property int $user_id
 * @property string $full_name
 * @property string $informed_only
 * @property string $email
 * @property string $email_start
 * @property string $email_end
 * @property string $no_hp
 * @property string $no_telp
 * @property string $no_ext
 * @property string $computer_name
 * @property string $computer_ip
 * @property string $manager_id
 * @property int $department_id
 * @property int $location_id
 * @property string $cs_group
 * @property string $im_team
 * @property string $position
 * @property string $manager_id
 * @property string $safety_admin
 * @property string $image_path
 * @property int $is_active
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property MstDepartment $department
 * @property MstLocation $location
 * @property MstUser $user
 */
class MstUserProfile extends \yii\db\ActiveRecord
{
    public $image;
    public $crop_info;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'informed_only', 'department_id', 'location_id', 'cs_group', 'im_team', 'position', 'manager_id', 'safety_admin'], 'required'],
            ['image_path','required','on'=>'update'],
            [['user_id', 'department_id', 'location_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name', 'computer_name'], 'string', 'max' => 100],
            [['informed_only', 'no_hp', 'no_telp'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 64],
            [['email_start', 'email_end'], 'string', 'max' => 5],
            [['no_ext', 'cs_group', 'im_team', 'position', 'manager_id', 'safety_admin'], 'string', 'max' => 10],
            [['computer_ip'], 'string', 'max' => 45],
            [['image_path'], 'string', 'max' => 125],
            [['user_id'], 'unique'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstDepartment::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            //['image','required','on','profile'],
            ['image', 'image', 'extensions' => ['jpg', 'jpeg', 'png', 'gif'], 'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif']],
            [['crop_info','user_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'full_name' => 'Full Name',
            'informed_only' => 'Informed Only',
            'email' => 'Email',
            'email_start' => 'Email Start',
            'email_end' => 'Email End',
            'no_hp' => 'No Hp',
            'no_telp' => 'No Telp',
            'no_ext' => 'No Ext',
            'computer_name' => 'Computer Name',
            'computer_ip' => 'Computer Ip',
            'department_id' => 'Department ID',
            'location_id' => 'Location ID',
            'cs_group' => 'Cs Group',
            'im_team' => 'Im Team',
            'position' => 'Position',
            'manager_id' => 'Coordinator',
            'safety_admin' => 'Safety Admin',
            'image_path' => 'Image Path',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(MstDepartment::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(MstLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'user_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {            
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = Yii::$app->user->isGuest ? 1 : Yii::$app->user->identity->id;
        }
        return true;
    }
}
