<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_manual".
 *
 * @property int $id
 * @property string $number
 * @property int $location_id
 * @property string $reason
 * @property string $category
 * @property string $gold_category
 * @property string $description
 * @property int $quantity
 * @property int $unit
 * @property double $amount
 * @property string $jde_no
 * @property string $sold_to
 * @property string $deliver_to
 * @property string $transporter
 * @property string $vehicle_no
 * @property string $driver_name
 * @property string $notes
 * @property string $sender
 * @property string $status
 * @property int $is_active
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_by
 * @property int $updated_at
 */
class TrxManual extends \yii\db\ActiveRecord
{
    const OPEN = "OPEN";
    const IN_PROGRESS = "IN_PROGRESS";
    const CLOSE = "CLOSE";
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_manual';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'location_id', 'reason', 'category', 'gold_category', 'sender', 'transporter'], 'required'],
            [['location_id', 'quantity', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['amount'], 'number'],
            [['number'], 'string', 'max' => 255],
            [['reason','category', 'gold_category','unit'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 225],
            [['jde_no'], 'string', 'max' => 64],
            [['sold_to', 'driver_name', 'notes'], 'string', 'max' => 150],
            [['deliver_to', 'transporter', 'vehicle_no', 'sender'], 'string', 'max' => 45],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['status','created_at'],'safe'],
        ];
    }
    
    public function getLocation()
    {
        return $this->hasOne(MstLocation::className(), ['id' => 'location_id']);
    }
    
    public function getManualReason() {
        return $this->hasOne(MstLookup::className(), ['code' => 'reason'])
                        ->where(['type' => 'ManualReason']);
    }
    
    public function getManualCategory() {
        return $this->hasOne(MstLookup::className(), ['code' => 'category']);
    }
    
    public function getManualGoldCategory() {
        return $this->hasOne(MstLookup::className(), ['code' => 'gold_category']);
    }
    
    public function getSenderBy()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'sender']);
    }

    public function getTransportBy()
    {
        return $this->hasOne(MstCompany::className(), ['id' => 'transporter']);
    }
    
    public function getPostUser()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'created_by']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManualWf()
    {
        return $this->hasMany(TrxManualWf::className(), ['manual_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'location_id' => 'Location',
            'reason' => 'Reason',
            'category' => 'Category',
            'gold_category' => 'Gold Category',
            'description' => 'Description',
            'quantity' => 'Quantity',
            'unit' => 'Unit',
            'amount' => 'Amount',
            'jde_no' => 'Jde No',
            'sold_to' => 'Sold To',
            'deliver_to' => 'Deliver To',
            'transporter' => 'Transporter',
            'vehicle_no' => 'Vehicle No',
            'driver_name' => 'Driver Name',
            'notes' => 'Remarks',
            'sender' => 'Sender',
            'is_active' => 'Is Active',
            'created_by' => 'Post By',
            'created_at' => 'Post At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
            $this->status = self::OPEN;
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_manual')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    // workflow
    public static function updateWorkflow($id, $role){
        $result = false;
        // current
        $model = TrxManualWf::find()->where(['manual_id'=>$id, 'role_from'=>$role])->one();
        if($model !== null){
            $model->submited = 1;
            $model->update();
            // find manual
            $manual = TrxManual::findOne($id);
            
            //next
            $next = TrxManualWf::find()->where(['manual_id'=>$id, 'role_from'=>$model->role_to])->one();
            // jika masih ada role berikutnya, maka update next workflow
            if($next !== null){
                // update next workflow
                $next->ready = 1;
                $next->update();
                
                // update manual
                $manual->status = self::IN_PROGRESS;
                $manual->update();
                
            // jika sudah tidak ada maka close DD Manual
            }else{                
                $manual->status = self::CLOSE;
                $manual->update();
            }
            $result = true;
        }        
        return $result;
    }
    
    public static function rejectWorkflow($id, $role){
        $result = false;
        // current
        $model = TrxManualWf::find()->where(['manual_id'=>$id, 'role_from'=>$role])->one();
        if($model !== null){
            $model->ready = 0;
            $model->update();
            
            //before
            $next = TrxManualWf::find()->where(['manual_id'=>$id, 'position'=>($model->position - 1)])->one();
            if($next !== null){
                $next->submited = 0;
                $next->update();
            }
            $result = true;
        }        
        return $result;
    }

    public static function addWorkflow($id, $role) {
        $check = TrxManualWf::find()->where(['manual_id' => $id])->count();
        if ($check == 0) {
            $models = MstWorkflow::find()->where(['wf_type' => 'DD-MANUAL'])->orderBy('position')->all();
            foreach ($models as $model) {
                $item = new TrxManualWf();
                $item->manual_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = ($model->role_from == $role ? 1 : 0);
                $item->save();
            }
        }   
    }
    
    public static function statusList(){
        return [
            ['code' => self::OPEN, 'name'=>'Open'],
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
            ['code' => self::CLOSE, 'name'=>'Close'],
        ];
    }
    
    public static function responseStatus(){
        return [
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
        ];
    }
}
