<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_supplier".
 *
 * @property int $id
 * @property string $supplier_name
 * @property string $address
 * @property int $propinsi_id
 * @property int $kota_id
 * @property string $email
 * @property string $no_hp
 * @property string $no_telp
 * @property int $supplier_desc
 * @property string $category_id
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property MstKota $kota
 * @property MstPropinsi $propinsi
 * @property MstUser $supplier_descUser
 * @property TrxInvoice[] $trxInvoices
 */
class MstSupplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_supplier';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_name', 'address', 'propinsi_id', 'kota_id', 'category_id'], 'required'],
            [['propinsi_id', 'kota_id', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['supplier_name', 'address','supplier_desc'], 'string', 'max' => 225],
            [['email'], 'string', 'max' => 128],
            [['email'], 'email'],
            [['no_hp', 'no_telp', 'category_id'], 'string', 'max' => 20],
            [['kota_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstKota::className(), 'targetAttribute' => ['kota_id' => 'id']],
            [['propinsi_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstPropinsi::className(), 'targetAttribute' => ['propinsi_id' => 'id']],
            [['created_at','updated_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_name' => 'Supplier Name',
            'supplier_desc' => 'Details',
            'address' => 'Alamat',
            'propinsi_id' => 'Propinsi',
            'kota_id' => 'Kota',
            'email' => 'Email',
            'no_hp' => 'No Hp',
            'no_telp' => 'No Telp',            
            'category_id' => 'Kategori',
            'is_active' => 'Aktif',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKota()
    {
        return $this->hasOne(MstKota::className(), ['id' => 'kota_id']);
    }
    
    public function getCategory() {
        return $this->hasOne(MstLookup::className(), ['code' => 'category_id'])
                        ->where(['type' => 'SupplierCategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropinsi()
    {
        return $this->hasOne(MstPropinsi::className(), ['id' => 'propinsi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxInvoices()
    {
        return $this->hasMany(TrxInvoice::className(), ['supplier_id' => 'id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_supplier')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function listData(){
        $models = self::find()->select(['id','supplier_name'])->where(['is_active'=>'1'])->orderBy('supplier_name')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
}
