<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_desa".
 *
 * @property integer $id
 * @property string $nama_desa
 * @property integer $kecamatan_id
 *
 * @property MstKecamatan $kecamatan
 */
class MstDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_desa', 'kecamatan_id'], 'required'],
            [['id', 'kecamatan_id'], 'integer'],
            [['nama_desa'], 'string', 'max' => 150],
            [['kecamatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstKecamatan::className(), 'targetAttribute' => ['kecamatan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_desa' => 'Nama Desa',
            'kecamatan_id' => 'Kecamatan ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(MstKecamatan::className(), ['id' => 'kecamatan_id']);
    }
}
