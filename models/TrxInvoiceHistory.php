<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_invoice_history".
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $user_id
 * @property string $status
 * @property int $created_at
 *
 * @property TrxInvoice $invoice
 * @property MstUser $user
 */
class TrxInvoiceHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_invoice_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'user_id', 'status'], 'required'],
            [['invoice_id', 'user_id'], 'integer'],
            [['status'], 'string', 'max' => 64],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(TrxInvoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MstUserProfile::className(), ['user_id' => 'user_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = date('Y-m-d H:i:s');
        } 
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_invoice_history')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
