<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_invoice_detail".
 *
 * @property int $id
 * @property int $invoice_id
 * @property string $invoice_no
 * @property string $currency
 * @property double $amount
 * @property string $tax_no
 *
 * @property TrxInvoice $invoice
 */
class TrxInvoiceDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_invoice_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_no', 'currency', 'amount'], 'required'],
            [['invoice_id'], 'integer'],
            [['amount'], 'number'],
            [['invoice_no'], 'string', 'max' => 128],
            [['currency'], 'string', 'max' => 10],
            [['tax_no'], 'string', 'max' => 20],
            [['tax_no'],'safe'],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'invoice_no' => 'Invoice No',
            'currency' => 'Currency',
            'amount' => 'Amount',
            'tax_no' => 'Tax No',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(TrxInvoice::className(), ['id' => 'invoice_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = $this->updated_at = time();
            // $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            //$this->updated_at = time();
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_invoice_detail')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
