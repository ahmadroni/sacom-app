<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_callcenter".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $company_id
 * @property string $jenis_panggilan
 * @property string $jenis_media
 * @property string $isi_pertanyaan
 * @property string $isi_jawaban
 * @property int $sales_pic
 * @property int $is_active
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_by
 * @property int $updated_at
 * @property string $status
 *
 * @property MstCompany $company
 * @property MstCustomer $customer
 * @property MstUser $salesPic
 */
class TrxCallCenter extends \yii\db\ActiveRecord
{
    const OPEN = "OPEN";
    const IN_PROGRESS = "IN_PROGRESS";
    const CLOSE = "CLOSE";
    
    public $company_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_callcenter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'company_id', 'jenis_panggilan', 'jenis_media', 'isi_pertanyaan', 'isi_jawaban', 'sales_pic','created_at'], 'required'],
            [['customer_id', 'company_id', 'sales_pic', 'is_active', 'created_by',  'updated_by'], 'integer'],
            [['jenis_panggilan', 'jenis_media'], 'string', 'max' => 20],
            [['isi_pertanyaan', 'isi_jawaban'], 'string', 'max' => 225],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstCompany::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstCustomer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['sales_pic'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['sales_pic' => 'id']],
            [['created_at','updated_at'],'safe'],            
            // company name
            [['company_name','status'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'company_id' => 'Perusahaan',
            'company_name' => 'Nama Perusahaan',
            'jenis_panggilan' => 'Jenis Panggilan',
            'jenis_media' => 'Jenis Media',
            'isi_pertanyaan' => 'Isi Pertanyaan',
            'isi_jawaban' => 'Isi Jawaban',
            'sales_pic' => 'Sales Pic',
            'is_active' => 'Is Active',
            'created_by' => 'Post By',
            'created_at' => 'Tgl Input',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(MstCompany::className(), ['id' => 'company_id']);
    }
    
    public function getCallType() {
        return $this->hasOne(MstLookup::className(), ['code' => 'jenis_panggilan'])
                        ->where(['type' => 'CCCallType']);
    }
    
    public function getMediaType() {
        return $this->hasOne(MstLookup::className(), ['code' => 'jenis_media'])
                        ->where(['type' => 'CCMediaType']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(MstCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesPic()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'sales_pic']);
    }
    
    public function getCallCenterWf()
    {
        return $this->hasMany(TrxCallCenterWf::className(), ['callcenter_id' => 'id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            //$this->created_at = 
            $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
            $this->status = self::OPEN;
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_callcenter')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function updateWorkflow($id, $role){
        $result = false;
        // current
        $model = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'role_from'=>$role])->one();
        if($model !== null){
            $model->submited = 1;
            $model->update();
            
            // find cc
            $cc = TrxCallCenter::findOne($id);
            //next
            $next = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'role_from'=>$model->role_to])->one();
            // jika masih terdapat workflow maka update next workflow
            if($next !== null){
                $next->ready = 1;
                $next->update();
                
                // update cc
                $cc->status = self::IN_PROGRESS;
                $cc->update();
            // jika sudah tidak ada maka update model -> close
            }else{                
                $cc->status = self::CLOSE;
                $cc->update();
            }
            $result = true;
        }        
        return $result;
    }
    
    public static function rejectWorkflow($id, $role){
        $result = false;
        // current
        $model = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'role_from'=>$role])->one();
        if($model !== null){
            $model->ready = 0;
            $model->update();
            
            //before
            $next = TrxCallCenterWf::find()->where(['callcenter_id'=>$id, 'position'=>($model->position - 1)])->one();
            if($next !== null){
                $next->submited = 0;
                $next->update();
            }
            $result = true;
        }        
        return $result;
    }

    public static function addWorkflow($id, $role) {
        $check = TrxCallCenterWf::find()->where(['callcenter_id' => $id])->count();
        if ($check == 0) {
            $models = MstWorkflow::find()->where(['wf_type' => 'CALL-CENTER'])->orderBy('position')->all();
            foreach ($models as $model) {
                $item = new TrxCallCenterWf();
                $item->callcenter_id = $id;
                $item->role_from = $model->role_from;
                $item->role_to = $model->role_to;
                $item->position = $model->position;
                $item->submited = 0;
                $item->ready = ($model->role_from == $role ? 1 : 0);
                $item->save();
            }
        }   
    }
    
    public static function responseStatus(){
        return [
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
        ];
    }
    
    public static function statusList(){
        return [
            ['code' => self::OPEN, 'name'=>'Open'],
            ['code' => self::IN_PROGRESS, 'name'=>'In Progress'],
            ['code' => self::CLOSE, 'name'=>'Close'],
        ];
    }
}
