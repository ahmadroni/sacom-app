<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_role".
 *
 * @property int $id
 * @property string $role_name
 * @property string $role_desc
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 */
class MstRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_name', 'role_desc'], 'required'],
            [['role_name'],'unique','on'=>'insert'],
            [['is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['role_name'], 'string', 'max' => 128],
            [['role_desc'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_name' => 'Role Name',
            'role_desc' => 'Role Desc',
            'is_active' => 'Aktif',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = time();
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_role')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
