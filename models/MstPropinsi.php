<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_propinsi".
 *
 * @property integer $id
 * @property string $nama_propinsi
 *
 * @property MstKota[] $mstKotas
 */
class MstPropinsi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_propinsi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_propinsi'], 'required'],
            [['id'], 'integer'],
            [['nama_propinsi'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_propinsi' => 'Nama Propinsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstKotas()
    {
        return $this->hasMany(MstKota::className(), ['propinsi_id' => 'id']);
    }
    
    public static function listData(){
        $models = self::find()->select(['id','nama_propinsi'])->orderBy('id')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
}
