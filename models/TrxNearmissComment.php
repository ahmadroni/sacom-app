<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_nearmiss_comment".
 *
 * @property int $id
 * @property int $nearmiss_id
 * @property string $content
 * @property string $status
 * @property int $agreement
 * @property int $created_at
 * @property int $created_by
 *
 * @property MstUser $createdBy
 * @property TrxNearmiss $nearmiss
 */
class TrxNearmissComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_nearmiss_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nearmiss_id', 'content', 'status'], 'required'],
            [['nearmiss_id', 'agreement', 'created_by'], 'integer'],
            [['content'], 'string', 'max' => 512],
            [['status'], 'string', 'max' => 20],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['nearmiss_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxNearmiss::className(), 'targetAttribute' => ['nearmiss_id' => 'id']],
            [['created_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nearmiss_id' => 'Nearmiss ID',
            'content' => 'Follow Up',
            'status' => 'Status',
            'agreement' => 'Agreement',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostBy()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNearmiss()
    {
        return $this->hasOne(TrxNearmiss::className(), ['id' => 'nearmiss_id']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = date('Y-m-d H:i:s');
            $this->created_by =  Yii::$app->user->identity->id;
        } else {
            //$this->updated_at = date('Y-m-d H:i:s');
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_nearmiss_comment')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
