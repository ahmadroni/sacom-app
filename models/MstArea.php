<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_area".
 *
 * @property int $id
 * @property string $area_name
 * @property string $area_desc
 * @property int $is_active
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 */
class MstArea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area_name', 'area_desc'], 'required'],
            [['area_name'], 'unique', 'on'=>'insert'],
            [['is_active',  'created_by', 'updated_by'], 'integer'],
            [['area_name'], 'string', 'max' => 128],
            [['area_desc'], 'string', 'max' => 225],
            [['created_at','updated_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_name' => 'Area Name',
            'area_desc' => 'Area Desc',
            'is_active' => 'Aktif',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_area')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
    
    public static function listData(){
        $models = self::find()->select(['id','area_name'])->where(['is_active'=>'1'])->orderBy('area_name')->all();
        if($models !== null){
            return $models;
        }else {
            return ['empty'=>'Empty'];
        }
    }
}
