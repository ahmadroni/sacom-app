<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_hazard_comment".
 *
 * @property int $id
 * @property int $hazard_id
 * @property string $content
 * @property string $status
 * @property int $agreement
 * @property int $created_at
 * @property int $created_by
 *
 * @property TrxHazard $hazard
 * @property MstUser $createdBy
 */
class TrxHazardComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_hazard_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hazard_id', 'content', 'status'], 'required'],
            [['hazard_id', 'agreement', 'created_by'], 'integer'],
            [['content'], 'string', 'max' => 512],
            [['status'], 'string', 'max' => 20],
            [['hazard_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrxHazard::className(), 'targetAttribute' => ['hazard_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => MstUser::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['created_at'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hazard_id' => 'Hazard ID',
            'content' => 'Follow Up',
            'status' => 'Status',
            'agreement' => 'Agreement',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHazard()
    {
        return $this->hasOne(TrxHazard::className(), ['id' => 'hazard_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostBy()
    {
        return $this->hasOne(MstUser::className(), ['id' => 'created_by']);
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = date('Y-m-d H:i:s');
            $this->created_by = Yii::$app->user->identity->id;
        } 
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('trx_hazard_comment')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
