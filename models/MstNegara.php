<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_negara".
 *
 * @property int $id
 * @property string $kode_negara
 * @property string $nama_negara
 * @property int $is_active
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class MstNegara extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_negara';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_negara', 'nama_negara'], 'required'],
            [['is_active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_negara'], 'string', 'max' => 4],
            [['nama_negara'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_negara' => 'Kode Negara',
            'nama_negara' => 'Nama Negara',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    public static function listData() {
        $items;
        // set loop
        $models = self::find()
                ->where(['is_active'=>self::STATUS_ACTIVE])
                ->orderBy('nama_negara')
                ->all();
        if ($models !== null) {
            foreach ($models as $model){
                $items[]=['id'=>$model->id , 'nama'=>$model->kode_negara ." - ".$model->nama_negara]; 
            }
        } 
        return $items;
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            $this->updated_by = Yii::$app->user->identity->id;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_negara')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
