<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trx_inovation_file".
 *
 * @property int $id
 * @property int $inovation_id
 * @property string $nama_file
 * @property string $type_file
 * @property int $size_file
 */
class TrxInovationFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_inovation_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inovation_id', 'nama_file', 'type_file', 'size_file'], 'required'],
            [['inovation_id', 'size_file'], 'integer'],
            [['nama_file'], 'string', 'max' => 128],
            [['type_file'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inovation_id' => 'Inovation ID',
            'nama_file' => 'Nama File',
            'type_file' => 'Type File',
            'size_file' => 'Size File',
        ];
    }
}
