<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\TrxNearmiss;
use app\models\TrxNearmissDetail;

class TrxNearmissForm extends Model {

    private $_trxNearmiss;
    private $_trxNearmissDetail;

    public function rules() {
        return [
            [['TrxNearmiss'], 'required'],
            [['TrxNearmiss', 'TrxNearmissDetails'], 'safe'],
        ];
    }

    public function afterValidate() {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->trxNearmiss->save()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->saveTrxNearmissDetails()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public function saveTrxNearmissDetails() {
        $keep = [];
        foreach ($this->trxNearmissDetails as $detail) {
            $detail->nearmiss_id = $this->trxNearmiss->id;
            if (!$detail->save(false)) {
                return false;
            }
            $keep[] = $detail->id;
        }
        $query = TrxNearmissDetail::find()->andWhere(['nearmiss_id' => $this->trxNearmiss->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $detail) {
            $detail->delete();
        }
        return true;
    }

    public function getTrxNearmiss() {
        return $this->_trxNearmiss;
    }

    public function setTrxNearmiss($trxNearmiss) {
        if ($trxNearmiss instanceof TrxNearmiss) {
            $this->_trxNearmiss = $trxNearmiss;
        } else if (is_array($trxNearmiss)) {
            $this->_trxNearmiss->setAttributes($trxNearmiss);
        }
    }

    public function getTrxNearmissDetails() {
        if ($this->_trxNearmissDetail === null) {
            $this->_trxNearmissDetail = $this->trxNearmiss->isNewRecord ? [] : $this->trxNearmiss->trxNearmissDetails;
        }
        return $this->_trxNearmissDetail;
    }

    private function getTrxNearmissDetail($key) {
        $item = $key && strpos($key, 'new') === false ? TrxNearmissDetail::findOne($key) : false;
        if (!$item) {
            $item = new TrxNearmissDetail();
            $item->loadDefaultValues();
        }
        return $item;
    }

    public function setTrxNearmissDetails($trxNearmissDetails) {
        unset($trxNearmissDetails['__id__']); // remove the hidden "new Parcel" row
        $this->_trxNearmissDetail = [];
        foreach ($trxNearmissDetails as $key => $trxNearmissDetail) {
            if (is_array($trxNearmissDetail)) {
                $this->_trxNearmissDetail[$key] = $this->getTrxNearmissDetail($key);
                $this->_trxNearmissDetail[$key]->setAttributes($trxNearmissDetail);
            } elseif ($trxNearmissDetail instanceof TrxNearmissDetail) {
                $this->_trxNearmissDetail[$trxNearmissDetail->id] = $trxNearmissDetail;
            }
        }
    }

    public function errorSummary($form) {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels() {
        $models = [
            'TrxNearmiss' => $this->trxNearmiss,
        ];

        foreach ($this->trxNearmissDetails as $id => $detail) {
            $models['TrxNearmissDetail.' . $id] = $this->trxNearmissDetails[$id];
        }
        return $models;
    }

}
