<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class TrxInvoiceApproval extends Model
{
    public $invoice_id;
    public $role_from;
    public $role_to;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['invoice_id', 'role_from','role_to'], 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice Id',
            'role_from' => 'Role From',
            'role_to' => 'Role To',
        ];
    }
}