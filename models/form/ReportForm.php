<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ReportForm extends Model
{
    public $tgl_awal;
    public $tgl_akhir;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['tgl_awal', 'tgl_akhir'], 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'tgl_awal' => 'Periode Awal',
            'tgl_akhir' => 'Periode Akhir',
        ];
    }
}
