<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ReportSafetyForm extends Model
{
    public $category;
    public $type;
    public $location;
    public $tgl_awal;
    public $tgl_akhir;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['type','location','tgl_awal', 'tgl_akhir'], 'required'],
            [['category','type'],'safe']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'type' => 'Tipe',
            'category' => 'Kategori',
            'location' => 'Lokasi',
            'tgl_awal' => 'Periode Awal',
            'tgl_akhir' => 'Periode Akhir',
        ];
    }
}
