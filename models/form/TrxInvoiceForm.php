<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\TrxInvoice;
use app\models\TrxInvoiceDetail;

class TrxInvoiceForm extends Model {

    private $_trxInvoice;
    private $_trxInvoiceDetail;

    public function rules() {
        return [
            [['TrxInvoice'], 'required'],
            [['TrxInvoice', 'TrxInvoiceDetails'], 'safe'],
        ];
    }

    public function afterValidate() {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->trxInvoice->save()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->saveTrxInvoiceDetails()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public function saveTrxInvoiceDetails() {
        $keep = [];
        foreach ($this->trxInvoiceDetails as $detail) {
            $detail->invoice_id = $this->trxInvoice->id;
            if (!$detail->save(false)) {
                return false;
            }
            $keep[] = $detail->id;
        }
        $query = TrxInvoiceDetail::find()->andWhere(['invoice_id' => $this->trxInvoice->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $detail) {
            $detail->delete();
        }
        return true;
    }

    public function getTrxInvoice() {
        return $this->_trxInvoice;
    }

    public function setTrxInvoice($trxInvoice) {
        if ($trxInvoice instanceof TrxInvoice) {
            $this->_trxInvoice = $trxInvoice;
        } else if (is_array($trxInvoice)) {
            $this->_trxInvoice->setAttributes($trxInvoice);
        }
    }

    public function getTrxInvoiceDetails() {
        if ($this->_trxInvoiceDetail === null) {
            $this->_trxInvoiceDetail = $this->trxInvoice->isNewRecord ? [] : $this->trxInvoice->trxInvoiceDetails;
        }
        return $this->_trxInvoiceDetail;
    }

    private function getTrxInvoiceDetail($key) {
        $item = $key && strpos($key, 'new') === false ? TrxInvoiceDetail::findOne($key) : false;
        if (!$item) {
            $item = new TrxInvoiceDetail();
            $item->loadDefaultValues();
        }
        return $item;
    }

    public function setTrxInvoiceDetails($trxInvoiceDetails) {
        unset($trxInvoiceDetails['__id__']); // remove the hidden "new Parcel" row
        $this->_trxInvoiceDetail = [];
        foreach ($trxInvoiceDetails as $key => $trxInvoiceDetail) {
            if (is_array($trxInvoiceDetail)) {
                $this->_trxInvoiceDetail[$key] = $this->getTrxInvoiceDetail($key);
                $this->_trxInvoiceDetail[$key]->setAttributes($trxInvoiceDetail);
            } elseif ($trxInvoiceDetail instanceof TrxInvoiceDetail) {
                $this->_trxInvoiceDetail[$trxInvoiceDetail->id] = $trxInvoiceDetail;
            }
        }
    }

    public function errorSummary($form) {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels() {
        $models = [
            'TrxInvoice' => $this->trxInvoice,
        ];

        foreach ($this->trxInvoiceDetails as $id => $detail) {
            $models['TrxInvoiceDetail.' . $id] = $this->trxInvoiceDetails[$id];
        }
        return $models;
    }

}
