<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\form;

/**
 * Description of UserForm
 *
 * @author ahmad
 */
use Yii;
use yii\base\Model;
use yii\widgets\ActiveForm;
use app\models\TrxCallCenter;
use app\models\MstCustomer;

class TrxCallCenterForm extends Model {

    private $_trxCallCenter;
    private $_mstCustomer;

    public function rules() {
        return [
            [['TrxCallCenter'], 'required'],
            [['MstCustomer'], 'safe'],
        ];
    }
    
    public function afterValidate() {
        $error = false;
        if (!$this->trxCallCenter->validate()) {
            $error = true;
        }
        if (!$this->mstCustomer->validate()) {
            $error = true;
        }
        if ($error) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->trxCallCenter->save()) {
            $transaction->rollBack();
            return false;
        }
        /**
        $this->mstCustomer->user_id = $this->trxCallCenter->id;
        if (!$this->mstCustomer->save(false)) {
            $transaction->rollBack();
            return false;
        }
        ***/
        $transaction->commit();
        return true;
    }

    public function getTrxCallCenter() {
        return $this->_trxCallCenter;
    }

    public function setTrxCallCenter($item) {
        if ($item instanceof TrxCallCenter) {
            $this->_trxCallCenter = $item;
        } else if (is_array($item)) {
            $this->_trxCallCenter->setAttributes($item);
        }
    }

    public function getMstCustomer() {
        if ($this->_mstCustomer === null) {
            if ($this->trxCallCenter->isNewRecord) {
                $this->_mstCustomer = new MstCustomer();
                $this->_mstCustomer->loadDefaultValues();
            } else {
                $this->_mstCustomer = $this->trxCallCenter->customer;
            }
        }
        return $this->_mstCustomer;
    }

    public function setMstCustomer($item) {
        if (is_array($item)) {
            $this->mstCustomer->setAttributes($item);
        } elseif ($item instanceof MstCustomer) {
            $this->_mstCustomer = $item;
        }
    }

    public function errorSummary($form) {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels() {
        $models = [
            'TrxCallCenter' => $this->trxCallCenter,
            'MstCustomer' => $this->mstCustomer,
        ];
        return $models;
    }

}
