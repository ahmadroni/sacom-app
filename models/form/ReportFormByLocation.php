<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ReportFormByLocation extends Model
{
    public $location_id;
    public $tgl_awal;
    public $tgl_akhir;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['location_id','tgl_awal', 'tgl_akhir'], 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'location_id' => 'Periode Awal',
            'tgl_awal' => 'Periode Awal',
            'tgl_akhir' => 'Periode Akhir',
        ];
    }
}
