<?php
namespace app\models\form;

use app\components\UserStatus;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequest extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $class = Yii::$app->getUser()->identityClass ? : 'app\models\MstUser';
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            //['email', 'email'],
            ['email', 'exist',
                'targetClass' => $class,
                'filter' => ['status' => UserStatus::ACTIVE],
                'targetAttribute' =>['email'=>'username'],
                'message' => 'Username tidak terdaftar.'
            ],
        ];
    }
    
     /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Username',
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $class = Yii::$app->getUser()->identityClass ? : 'app\models\MstUser';
        /*
        $user = $class::find()
                ->andWhere(['status' => UserStatus::ACTIVE])
                ->andWhere(['or',['username' => $this->email],['email' => $this->email]])
                ->one();
         */
        $user = $class::findOne([
            'status' => UserStatus::ACTIVE,
            'username' => $this->email,
        ]);

        if ($user) {
            if (!ResetPassword::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
            }

            if ($user->save()) {
                return Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    //->setTo($this->email)
                    ->setTo($user->email)
                    ->setSubject('Password reset for ' . Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
