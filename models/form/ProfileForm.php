<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ProfileForm extends Model
{
    public $image;
    public $crop_info;
    public $user_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['image', 'image', 'extensions' => ['jpg', 'jpeg', 'png', 'gif'], 'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif']],
            [['crop_info','user_id'], 'safe'],
        ];
    }
    
}
