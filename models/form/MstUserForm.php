<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\form;

/**
 * Description of UserForm
 *
 * @author ahmad
 */
use Yii;
use yii\base\Model;
use yii\widgets\ActiveForm;
use app\models\MstUser;
use app\models\MstUserProfile;

class MstUserForm extends Model {

    private $_mstUser;
    private $_mstUserProfile;

    public function rules() {
        return [
            [['MstUser'], 'required'],
            [['MstUserProfile'], 'safe'],
        ];
    }

    public function afterValidate() {
        $error = false;
        if (!$this->mstUser->validate()) {
            $error = true;
        }
        if (!$this->mstUserProfile->validate()) {
            $error = true;
        }
        if ($error) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        // set password user
        $this->mstUser->setPassword($this->mstUser->password);
        // set AuthKey
        $this->mstUser->generateAuthKey();
        if (!$this->mstUser->save()) {
            $transaction->rollBack();
            return false;
        }
        
        $this->mstUserProfile->user_id = $this->mstUser->id;
        if (!$this->mstUserProfile->save()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    public function getMstUser() {
        return $this->_mstUser;
    }

    public function setMstUser($product) {
        if ($product instanceof MstUser) {
            $this->_mstUser = $product;
        } else if (is_array($product)) {
            $this->_mstUser->setAttributes($product);
        }
    }

    public function getMstUserProfile() {
        if ($this->_mstUserProfile === null) {
            if ($this->mstUser->isNewRecord) {
                $this->_mstUserProfile = new MstUserProfile();
                $this->_mstUserProfile->loadDefaultValues();
            } else {
                $this->_mstUserProfile = $this->mstUser->mstUserProfile;
            }
        }
        return $this->_mstUserProfile;
    }

    public function setMstUserProfile($item) {
        if (is_array($item)) {
            $this->mstUserProfile->setAttributes($item);
        } elseif ($item instanceof MstUserProfile) {
            $this->_mstUserProfile = $item;
        }
    }

    public function errorSummary($form) {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            //'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors :</p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels() {
        return [
            'MstUser' => $this->mstUser,
            'MstUserProfile' => $this->mstUserProfile,
        ];
    }

}
