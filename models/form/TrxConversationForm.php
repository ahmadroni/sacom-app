<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\TrxConversation;
use app\models\TrxConversationDetail;

class TrxConversationForm extends Model {

    private $_trxConversation;
    private $_trxConversationDetail;

    public function rules() {
        return [
            [['TrxConversation'], 'required'],
            [['TrxConversation', 'TrxConversationDetails'], 'safe'],
        ];
    }

    public function afterValidate() {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->trxConversation->save()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->saveTrxConversationDetails()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public function saveTrxConversationDetails() {
        $keep = [];
        foreach ($this->trxConversationDetails as $detail) {
            $detail->conversation_id = $this->trxConversation->id;
            if (!$detail->save(false)) {
                return false;
            }
            $keep[] = $detail->id;
        }
        $query = TrxConversationDetail::find()->andWhere(['conversation_id' => $this->trxConversation->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $detail) {
            $detail->delete();
        }
        return true;
    }

    public function getTrxConversation() {
        return $this->_trxConversation;
    }

    public function setTrxConversation($trxConversation) {
        if ($trxConversation instanceof TrxConversation) {
            $this->_trxConversation = $trxConversation;
        } else if (is_array($trxConversation)) {
            $this->_trxConversation->setAttributes($trxConversation);
        }
    }

    public function getTrxConversationDetails() {
        if ($this->_trxConversationDetail === null) {
            $this->_trxConversationDetail = $this->trxConversation->isNewRecord ? [] : $this->trxConversation->trxConversationDetails;
        }
        return $this->_trxConversationDetail;
    }

    private function getTrxConversationDetail($key) {
        $item = $key && strpos($key, 'new') === false ? TrxConversationDetail::findOne($key) : false;
        if (!$item) {
            $item = new TrxConversationDetail();
            $item->loadDefaultValues();
        }
        return $item;
    }

    public function setTrxConversationDetails($trxConversationDetails) {
        unset($trxConversationDetails['__id__']); // remove the hidden "new Parcel" row
        $this->_trxConversationDetail = [];
        foreach ($trxConversationDetails as $key => $trxConversationDetail) {
            if (is_array($trxConversationDetail)) {
                $this->_trxConversationDetail[$key] = $this->getTrxConversationDetail($key);
                $this->_trxConversationDetail[$key]->setAttributes($trxConversationDetail);
            } elseif ($trxConversationDetail instanceof TrxConversationDetail) {
                $this->_trxConversationDetail[$trxConversationDetail->id] = $trxConversationDetail;
            }
        }
    }

    public function errorSummary($form) {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            //'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors :</p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels() {
        $models = [
            'TrxConversation' => $this->trxConversation,
        ];

        foreach ($this->trxConversationDetails as $id => $detail) {
            $models['TrxConversationDetail.' . $id] = $this->trxConversationDetails[$id];
        }
        return $models;
    }

}
