<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\TrxHazard;
use app\models\TrxHazardDetail;

class TrxHazardForm extends Model {

    private $_trxHazard;
    private $_trxHazardDetail;

    public function rules() {
        return [
            [['TrxHazard'], 'required'],
            [['TrxHazard', 'TrxHazardDetails'], 'safe'],
        ];
    }

    public function afterValidate() {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();

        if (!$this->trxHazard->save()) {
            $transaction->rollBack();
            return false;
        }

        if (!$this->saveTrxHazardDetails()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();
        return true;
    }

    public function saveTrxHazardDetails() {
        $keep = [];
        foreach ($this->trxHazardDetails as $detail) {
            $detail->hazard_id = $this->trxHazard->id;
            if (!$detail->save(false)) {
                return false;
            }
            $keep[] = $detail->id;
        }
        $query = TrxHazardDetail::find()->andWhere(['hazard_id' => $this->trxHazard->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $detail) {
            $detail->delete();
        }
        return true;
    }

    public function getTrxHazard() {
        return $this->_trxHazard;
    }

    public function setTrxHazard($trxHazard) {
        if ($trxHazard instanceof TrxHazard) {
            $this->_trxHazard = $trxHazard;
        } else if (is_array($trxHazard)) {
            $this->_trxHazard->setAttributes($trxHazard);
        }
    }

    public function getTrxHazardDetails() {
        if ($this->_trxHazardDetail === null) {
            $this->_trxHazardDetail = $this->trxHazard->isNewRecord ? [] : $this->trxHazard->trxHazardDetails;
        }
        return $this->_trxHazardDetail;
    }

    private function getTrxHazardDetail($key) {
        $item = $key && strpos($key, 'new') === false ? TrxHazardDetail::findOne($key) : false;
        if (!$item) {
            $item = new TrxHazardDetail();
            $item->loadDefaultValues();
        }
        return $item;
    }

    public function setTrxHazardDetails($trxHazardDetails) {
        unset($trxHazardDetails['__id__']); // remove the hidden "new Parcel" row
        $this->_trxHazardDetail = [];
        foreach ($trxHazardDetails as $key => $trxHazardDetail) {
            if (is_array($trxHazardDetail)) {
                $this->_trxHazardDetail[$key] = $this->getTrxHazardDetail($key);
                $this->_trxHazardDetail[$key]->setAttributes($trxHazardDetail);
            } elseif ($trxHazardDetail instanceof TrxHazardDetail) {
                $this->_trxHazardDetail[$trxHazardDetail->id] = $trxHazardDetail;
            }
        }
    }

    public function errorSummary($form) {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels() {
        $models = [
            'TrxHazard' => $this->trxHazard,
        ];

        foreach ($this->trxHazardDetails as $id => $detail) {
            $models['TrxHazardDetail.' . $id] = $this->trxHazardDetails[$id];
        }
        return $models;
    }

}
