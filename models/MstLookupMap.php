<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_lookup_map".
 *
 * @property int $id
 * @property string $code_from
 * @property string $code_to
 * @property string $code_value
 * @property string $colors
 */
class MstLookupMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_lookup_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_from', 'code_to', 'code_value', 'colors'], 'required'],
            [['code_from', 'code_to', 'code_value'], 'string', 'max' => 20],
            [['colors'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_from' => 'Code From',
            'code_to' => 'Code To',
            'code_value' => 'Code Value',
            'colors' => 'Colors',
        ];
    }
    
    public function beforeSave($insert) {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->id = self::newId();
            $this->created_at = $this->updated_at = time();
            // $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
        } else {
            $this->updated_at = time();
            // $this->updated_by = Yii::$app->user->identity->username;
        }
        return true;
    }

    public static function newId() {
        $row = (new \yii\db\Query)
                ->select(['id'])
                ->from('mst_lookup_map')
                ->max('id');
        return $row == null ? 1 : ($row + 1);
    }
}
