<?php
return [
    'class' => 'mdm\admin\components\AccessControl',
    'allowActions' => [
        'site/login',
        'site/signup',
        'site/logout',
        //'admin/*',
        'user/request-password-reset',
        'user/reset-password',
        'user/change-password',
        'ajax/*',
        'gii/*',
        'debug/*',
    // The actions listed here will be allowed to everyone including guests.
    // So, 'admin/*' should not appear here in the production, of course.
    // But in the earlier stages of your development, you may probably want to
    // add a lot of actions here until you finally completed setting up rbac,
    // otherwise you may not even take a first step.
    ]
];
