<?php

return [
    'adminEmail' => 'yusuf.hangga@usgboral.com',
    'supportEmail' => 'noreply-jayasacom@usgboral.com',
    'supportName' => 'JayaSACOM',
    'mdm.admin.configs' => [
        //'db' => 'sacom_app',
        'menuTable' => '{{mst_menu}}',
        'userTable' => '{{mst_user}}',
        /*'cache' => [
                'class' => 'yii\caching\DbCache',
                'db' => ['dsn' => 'mysql:host=localhost;dbname=sacom_app'],
        ],*/
    ]
];
