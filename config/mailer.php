<?php
return [
    'class' => 'yii\swiftmailer\Mailer',
    'useFileTransport' => false,
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => '172.16.18.40',
        'username' => '',
        'password' => '',
        'port' => '25',
        'encryption' => '',
        /*
        'host' => 'smtp.gmail.com', // diganti
        'username' => 'username', //diganti
        'password' => 'password', //diganti
        'port' => '587',
        'encryption' => 'tls',
         * *
         */
    ],
];
