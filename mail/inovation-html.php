<?php
use yii\helpers\Html;
use yii\helpers\Url;

$resetLink = Url::to(['inovation/view','id'=>$inovationId], true);
?>
<div class="password-reset">
    <p>Hi <?= Html::encode($receiver) ?>,</p>

    <p>There are new innovation post by <b><?= Html::encode($userInput) ?></b></p>
    <p>to review please use this link : </p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <br>
    <br>
    <p>Sent by <b>JayaSACOM</b></p>
</div>
