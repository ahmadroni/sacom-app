<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nearmiss'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label'=> Yii::t('app','Detail')];
$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
$currentController = Yii::$app->controller->id;
?>
<div class="box box-info trx-conversation-index">
    <div class="box-header with-border">
        <div class="user-block">
            <img class="img-circle img-bordered-sm" src="<?= $imageUrl; ?>" alt="user image">
            <span class="username">
                <a href="#"><?= $model->postBy->fullname ?></a>
            </span>
            <span class="description">
                <i>Lokasi :</i>
                <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                , <i>Area :</i>
                <strong><?= $model->area->area_name; ?> </strong>
                , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
            </span>
        </div>      
        <div class="box-tools">
            <?php if ($model->allowComment) : ?>
                <a href="<?= Url::to(['trx-nearmiss/response', 'id' => $model->id]) ?>" class="btn btn-sm btn-success"><i class="fa fa-reply-all margin-r-5"></i> Response</a>
            <?php endif; ?>
            <?php if ($model->isAuthor && !$model->isClose) : ?>
                <a href="<?= Url::to(['trx-nearmiss/closing', 'id' => $model->id]) ?>" class="btn btn-sm btn-warning"><i class="fa fa-close margin-r-5"></i> Closing</a>
            <?php endif; ?>
        </div>

    </div>
    <div class="box-body">
        <!-- Post -->
        <strong> 
            <?= $model->getAttributeLabel('risk') . ' : <i>' . $model->riskLookup->name . '</i>' ?> | Kemungkinan : <i><?= $model->posibilityLookup->name ?> </i> |
            Tingkatan Resiko : <i><?= $model->risk_level ?></i>
        </strong> <br/>
        <strong><i class="fa fa-comment-o margin-r-5"></i> Pembicaraan : </strong>
        <p> 
            <?= HtmlPurifier::process($model->description) ?> 
        </p>
        <strong><i class="fa fa-comments-o margin-r-5"></i>Tindakan Yang Disepakati :</strong>
        <p>
            <?= HtmlPurifier::process($model->problems) ?> 
        </p>
        <?= $model->getAttributeLabel('potency') ?><strong><?= ' : <i>' . $model->potencyLookup->name . '</i>' ?> </strong> 
        <?= $model->getAttributeLabel('involved') ?> <strong><?= ' : <i>' . $model->involvedLookup->name . '</i>' ?></strong>
        <br/>
        <strong><i class="fa fa-users margin-r-5"></i> Recomended Solution :</strong>
    </div>
    <div class="box-footer box-comments">
        <div class="box-comments">
            <?php foreach ($model->trxNearmissDetails as $item): ?>
                <div class="box-comment">
                    <img class="img-circle img-sm" src="<?= \Yii::$app->request->BaseUrl . '/img/user3-128x128.jpg' ?>" alt="User Image">
                    <div class="comment-text">
                        <span class="username"><?= $item->forUser->username; ?> [<?= $item->status ?>]
                            <span class="text-muted pull-right">Dead Line : <?= date('d-m-Y H::s', strtotime($item->do_date)) ?></span>
                        </span>
                        <?= HtmlPurifier::process($item->notes); ?>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
        <span class="text-muted">Follow Up (<?= $model->commentCount ?>)</span>
        <?php foreach ($model->trxNearmissComments as $item): ?>
            <div class="box-comment">
                <img class="img-circle img-sm" src="<?= \Yii::$app->request->BaseUrl . '/img/user5-128x128.jpg' ?>" alt="User Image">
                <div class="comment-text">
                    <span class="username">
                    <?= $item->postBy->username; ?> [<?= $item->status ?>]
                        <span class="text-muted pull-right">Posting At : <?= date('d-m-Y H:i:s', strtotime($item->created_at)); ?></span>
                    </span>
                    <?= HtmlPurifier::process($item->content) ?>
                </div>
            </div>

        <?php endforeach; ?>
    </div>


    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>