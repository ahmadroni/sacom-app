<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrxNearmiss */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nearmiss'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label'=> Yii::t('app','Detail')];

$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="trx-nearmiss-view">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
             <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'title',
                    'location.location_name',
                    'area',
                    'area_detail',
                    'risk',
                    'posibility',
                    'types',
                ],
            ]) ?>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [                    
                    'description',
                    'problems',
                    'potency',
                    'involved_name',
                    'involved_checked',
                    'category',
                ],
            ]) ?>
        </div>
    </div>
   
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">List Detail</h3>
        </div>
        <div class="box-body no-padding">
            <table class="table table">
                <?php
                $detail = new app\models\TrxNearmissDetail();
                $detail->loadDefaultValues();
                ?>
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th><?= $detail->getAttributeLabel('notes') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('for_user') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('status') ?></th>
                        <th style="width:160px;"><?= $detail->getAttributeLabel('do_date') ?></th>
                    </tr>
                </thead>
                <tbody id="list-detail">
                    <?php foreach ($model->trxNearmissDetails as $key => $item) : ?>
                        <tr>
                            <td><?= ($key + 1) ?></td>
                            <td><?= $item->notes ?></td>
                            <td><?= $item->forUser->username ?></td>
                            <td><?= $item->status ?></td>
                            <td><?= $item->do_date ?></td>
                        </tr>    
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
