<?php
use app\models\TrxNearmiss;
use app\models\MstUser;
use yii\helpers\ArrayHelper;
?>
<td class="nomor">
</td>
<td>
    <?=
    $form->field($detail, 'notes', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->textInput([
        'id' => "TrxNearmissDetails_{$key}_notes",
        'name' => "TrxNearmissDetails[$key][notes]",
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'for_user', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->dropDownList(ArrayHelper::map(MstUser::listConversation(), 'id', 'username'), [
        'prompt' => '= Select Status =',
        'id' => "TrxNearmissDetails_{$key}_for_user",
        'name' => "TrxNearmissDetails[$key][for_user]",
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'status', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->dropDownList(ArrayHelper::map(TrxNearmiss::statusList(), 'code', 'name'),[
        'id' => "TrxNearmissDetails_{$key}_status",
        'name' => "TrxNearmissDetails[$key][status]",
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'do_date', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->textInput([
        'id' => "TrxNearmissDetails_{$key}_do_date",
        'name' => "TrxNearmissDetails[$key][do_date]",
        'class'=>'form-control tanggal'
    ])->label(false)
    ?>
</td>
<td>
    <button type="button" class="btn btn-danger btn-remove-detail btn-xs"><i class="fa fa-trash"></i></button>
</td>