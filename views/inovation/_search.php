<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\TrxInovationSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="trx-inovation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'location_id') ?>

    <?= $form->field($model, 'is_manufacturing_process') ?>

    <?= $form->field($model, 'is_product_feature') ?>

    <?php // echo $form->field($model, 'is_product_raw_material') ?>

    <?php // echo $form->field($model, 'is_design') ?>

    <?php // echo $form->field($model, 'is_currently_use') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'third_party') ?>

    <?php // echo $form->field($model, 'current_stage') ?>

    <?php // echo $form->field($model, 'invention_keyword') ?>

    <?php // echo $form->field($model, 'inventors') ?>

    <?php // echo $form->field($model, 'detail_description') ?>

    <?php // echo $form->field($model, 'invention_reason') ?>

    <?php // echo $form->field($model, 'any_addtional') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
