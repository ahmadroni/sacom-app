<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstLocation;

/* @var $this yii\web\View */
/* @var $model app\models\TrxInovation */
/* @var $form yii\bootstrap\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'id' =>'form-trx-inovation',
    'enableClientValidation' => false,
]); ?>
<div class="box box-info mst-config-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($model->title) ?></h3>    
    </div>
    <div class="box-body">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true,'placeholder'=>'Masukan inputan disini']) ?>
        <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(MstLocation::listInovation(), 'id', 'location_name'), ['prompt' => '= Pilih Region =']) ?>
       <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <?= $form->field($model, 'is_manufacturing_process')->checkbox([
                    'template' => '{beginLabel}{labelTitle}{endLabel} <br/> {input}{error}{hint}',
                    'class' => 'checkbox',
                    'data-toggle'=>'toggle', 
                    'data-size'=>"small", 
                    'data-on'=>"Ya",
                    'data-off'=>"Tidak"
                ]) ?>
                
                <?= $form->field($model, 'is_product_feature')->checkbox([
                    'template' => '{beginLabel}{labelTitle}{endLabel} <br/> {input}{error}{hint}',
                    'class' => 'checkbox',
                    'data-toggle'=>'toggle', 
                    'data-size'=>"small", 
                    'data-on'=>"Ya",
                    'data-off'=>"Tidak"
                ]) ?>
                
                <?= $form->field($model, 'is_product_raw_material')->checkbox([
                    'template' => '{beginLabel}{labelTitle}{endLabel} <br/> {input}{error}{hint}',
                    'class' => 'checkbox',
                    'data-toggle'=>'toggle', 
                    'data-size'=>"small", 
                    'data-on'=>"Ya",
                    'data-off'=>"Tidak"
                ]) ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <?= $form->field($model, 'is_design')->checkbox([
                    'template' => '{beginLabel}{labelTitle}{endLabel} <br/> {input}{error}{hint}',
                    'class' => 'checkbox',
                    'data-toggle'=>'toggle', 
                    'data-size'=>"small", 
                    'data-on'=>"Ya",
                    'data-off'=>"Tidak"
                ]) ?>
                
                <?= $form->field($model, 'is_currently_use')->checkbox([
                    'template' => '{beginLabel}{labelTitle}{endLabel} <br/> {input}{error}{hint}',
                    'class' => 'checkbox',
                    'data-toggle'=>'toggle', 
                    'data-size'=>"small", 
                    'data-on'=>"Ya",
                    'data-off'=>"Tidak"
                ]) ?>                
            </div>
        </div>        

        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'third_party')->textInput(['maxlength' => true, 'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'current_stage')->textInput(['maxlength' => true, 'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'invention_keyword')->textInput(['maxlength' => true, 'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'inventors')->textInput(['maxlength' => true, 'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'detail_description')->textarea(['rows' => 6, 'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'invention_reason')->textInput(['maxlength' => true,'placeholder'=>'Masukan inputan disini']) ?>

        <?= $form->field($model, 'any_addtional')->textInput(['maxlength' => true,'placeholder'=>'Masukan inputan disini']) ?>
        
        <?= $form->field($model, 'is_active')->checkbox([
            'template' => '{beginLabel}{labelTitle}{endLabel} <br/> {input}{error}{hint}',
            'class' => 'checkbox',
            'data-toggle'=>'toggle', 
            'checked'=>'checked', 
            'data-size'=>"small", 
            'data-on'=>"Ya",
            'data-off'=>"Tidak"
        ]) ?>  
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php ob_start(); ?>
<script>
    $("#trxinovation-country_id").select2();
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 
