<?php
use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'template' => '<tr><th>{label}</th><td style="width:70%;">{value}</td></tr>',
    'attributes' => [
        'id',
        'title',
        [
            'attribute'=>'created_at',
            'value'=>date('d-m-Y h:i:s', strtotime($model->created_at)),
        ],
        [
            'attribute'=>'location_id',
            'value'=>$model->location->nama_location
        ],
        [
            'attribute'=>'is_manufacturing_process',
            'value'=>$model->is_manufacturing_process == 1 ? "Yes" : "No",
        ],
        [
            'attribute'=>'is_product_feature',
            'value'=>$model->is_product_feature == 1 ? "Yes" : "No",
        ],
        [
            'attribute'=>'is_product_raw_material',
            'value'=>$model->is_product_raw_material == 1 ? "Yes" : "No",
        ],
        [
            'attribute'=>'is_design',
            'value'=>$model->is_design == 1 ? "Yes" : "No",
        ],
        [
            'attribute'=>'is_currently_use',
            'value'=>$model->is_currently_use == 1 ? "Yes" : "No",
        ],
        'description',
        'third_party',
        'current_stage',
        'invention_keyword',
        'inventors',
        'detail_description:ntext',
        'invention_reason',
        'any_addtional',
        [
            'attribute'=>'is_active',
            'value'=>$model->is_active == 1 ? "Yes" : "No",
        ],
        [
            'attribute'=>'updated_at',
            'value'=>date('d-m-Y h:i:s', strtotime($model->updated_at)),
        ],
    ],
]) ?>

