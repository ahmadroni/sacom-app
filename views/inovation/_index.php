<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

$imageUrl = Yii::getAlias('@web').'/img/user7-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="post">
    <div class="user-block">
        <img class="img-circle img-bordered-sm" height="32" width="32" src="<?= $imageUrl; ?>"/>
        <span class="username"><?= Html::a($model->title,['view','id'=>$model->id]);?></span>
        <span class="description"><?= "By ".$model->postUser->full_name .", at ". date('d M Y h:i:s'); ?></span>
    </div>
    <p>
       <?= $model->description; ?>
    </p>
    <hr>
</div>

