<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrxInovation */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Trx Inovations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box box-info mst-config-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>    
    </div>
    <div class="box-body">
        <?= $this->render('_item', [
            'model' => $model,
        ]) ?>
    </div>
</div>
