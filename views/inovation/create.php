<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrxInovation */

$this->title = 'New Item';
$this->params['breadcrumbs'][] = ['label' => 'Inovations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
    'title' => $this->title
]) ?>
