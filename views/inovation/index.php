<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxInovationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inovation';
$this->params['breadcrumbs'][] = $this->title;
?>  
<div class="box box-info mst-config-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3> 
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i> Add', ['create'], ['class' => 'btn btn-sm btn-info pull-right']); ?>
        </div>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary'=>'',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'created_at',
                'title',
                'description',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',
                    'options' => ['style' => 'width:100px;'],
                    'buttons' => [
                        'view' => function($url, $model, $key) {
                            return Html::a('<span class="fa fa-eye"></span>',['view','id'=>$model['id']], [
                                        'class' => 'btn btn-xs btn-success btn-view',
                                        'title' => Yii::t('yii', 'View Data'),
                                        'value' => $model['id'],
                            ]);
                        },
                        'update' => function($url, $model, $key) {
                            return Html::a('<span class="fa fa-pencil"></span>',['update','id'=>$model['id']], [
                                        'class' => 'btn btn-xs btn-warning btn-update',
                                        'title' => Yii::t('yii', 'Update Data'),
                                        'value' => $model['id'],
                            ]);
                        },
                    ],
                ],
            ],
        ]);
        ?>
        <!--?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_index',
            'summary' => '',
        ]);
        ?-->
    </div>
</div>
