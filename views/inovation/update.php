<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrxInovation */

$this->title = 'Update Trx Inovation: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Trx Inovations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
