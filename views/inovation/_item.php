<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

$imageUrl = Yii::getAlias('@web').'/img/user7-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="post">
    <div class="user-block">
        <img class="img-circle img-bordered-sm" height="32" width="32" src="<?= $imageUrl; ?>"/>
        <span class="username"><?= Html::a($model->title,['view','id'=>$model->id]);?></span>
        <span class="description"><?= "By ".$model->postUser->full_name .", at ". date('d M Y h:i:s'); ?></span>
    </div>
    <strong><i class="fa fa-book margin-r-5"></i> <?= $model->getAttributeLabel('title'); ?></strong>
    <p class="text-muted">
       <?= $model->title; ?>
    </p>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= DetailView::widget([
                'model' => $model,
                'template' => '<tr><th>{label}</th><td style="width:15%;">{value}</td></tr>',
                'options'=>['class'=>'table table-responsive'],
                'attributes' => [
                    [
                        'attribute'=>'is_manufacturing_process',
                        'value'=>$model->is_manufacturing_process == 1 ? "Yes" : "No",
                    ],
                    [
                        'attribute'=>'is_product_feature',
                        'value'=>$model->is_product_feature == 1 ? "Yes" : "No",
                    ],
                    [
                        'attribute'=>'is_product_raw_material',
                        'value'=>$model->is_product_raw_material == 1 ? "Yes" : "No",
                    ],
                    [
                        'attribute'=>'is_design',
                        'value'=>$model->is_design == 1 ? "Yes" : "No",
                    ],
                    [
                        'attribute'=>'is_currently_use',
                        'value'=>$model->is_currently_use == 1 ? "Yes" : "No",
                    ],
                ],
            ]) ?>
            
            <strong><i class="fa fa-info margin-r-5"></i> <?= $model->getAttributeLabel('description'); ?></strong>
            <p class="text-muted">
               <?= $model->description; ?>
            </p>
            <hr>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           

            <strong><i class="fa fa-group margin-r-5"></i> <?= $model->getAttributeLabel('third_party'); ?></strong>
            <p class="text-muted">
               <?= $model->third_party; ?>
            </p>
            <hr>
            <strong><i class="fa fa-lock margin-r-5"></i> <?= $model->getAttributeLabel('current_stage'); ?></strong>
            <p class="text-muted">
               <?= $model->current_stage; ?>
            </p>
            <hr>
            <strong><i class="fa fa-key margin-r-5"></i> <?= $model->getAttributeLabel('invention_keyword'); ?></strong>
            <p class="text-muted">
               <?= $model->invention_keyword; ?>
            </p>
            <hr>
            <strong><i class="fa fa-users margin-r-5"></i> <?= $model->getAttributeLabel('inventors'); ?></strong>
            <p class="text-muted">
               <?= $model->inventors; ?>
            </p>
        </div>
    </div>
    
    
    
    <hr>
    <strong><i class="fa fa-inbox margin-r-5"></i> <?= $model->getAttributeLabel('detail_description'); ?></strong>
    <p class="text-muted">
       <?= $model->detail_description; ?>
    </p>
    <hr>
    <strong><i class="fa fa-adjust margin-r-5"></i> <?= $model->getAttributeLabel('invention_reason'); ?></strong>
    <p class="text-muted">
       <?= $model->invention_reason; ?>
    </p>
    <hr>
    <strong><i class="fa fa-plus-circle margin-r-5"></i> <?= $model->getAttributeLabel('any_addtional'); ?></strong>
    <p class="text-muted">
       <?= $model->any_addtional; ?>
    </p>
</div>

