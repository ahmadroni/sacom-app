<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MstSupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = 'Invoice Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info mst-supplier-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title); ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('//report/_report', ['model' => $model]); ?>
        <?php if(count($dataProvider) > 0) : ?>
        <table class="table table-bordered table-striped table-responsive">
            <thead>
                <tr>
                    <?php foreach($dataProvider[0] as $key => $value){
                        if($key != 'id'){
                            echo "<th>". $key."</th>";
                        }
                    } ?>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $totalSum[0]=0;
                foreach($dataProvider as $models) {
                    echo "<tr>";
                    $cols = 0;
                    $totalTd = 0;
                    foreach($models as $key => $value){
                        if($key != 'id'):
                            if($cols==0){
                               echo "<td>". $value ."</td>";
                            }else{
                                echo "<td class='text-right'>". number_format($value)."</td>";
                                $totalTd+=$value;
                                if(!array_key_exists($cols, $totalSum)){
                                    array_push($totalSum, $value);
                                }else {
                                   $totalSum[$cols]+=$value;  
                                }

                            }
                            $cols++;
                         endif;
                    }
                    if(!array_key_exists($cols, $totalSum)){
                        array_push($totalSum, $totalTd);
                    }else {
                       $totalSum[$cols]+=$totalTd;  
                    }
                    echo "<td class='text-right'>". number_format($totalTd)."</td>";
                    echo "</tr>";
                }
                ?>
                <tr>
                    <?php foreach($totalSum as $key => $total){ 
                        if($key == 0) {
                            echo "<th>Total</th>";
                        }
                        else{
                            echo "<th class='text-right'>".number_format($total)."</th>";
                        }
                    }?>
                </tr>
            </tbody>
        </table>
        <?php else : ?>
        <div class="callout callout-warning" style="padding: 5px 10px 20px 10px;">
            <h5><i class="fa fa-warning"></i> Info : </h5>
            Data not found on your search criteria
        </div> 
        <?php endif; ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    $(document).ready(function () {
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            setDate: new Date(),
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 