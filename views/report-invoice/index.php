<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxManualSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Report Invoice');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-manual-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3> 
    </div>
    <div class="box-body">
        <?= $this->render('//report/_report', ['model' => $model]); ?>
        <?= $this->render('_table', ['dataProvider' => $dataProvider]); ?>
        <!--?php echo '<pre>'. print_r($dataProvider) .'</pre>';?-->
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    $(document).ready(function () {
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            setDate: new Date(),
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 