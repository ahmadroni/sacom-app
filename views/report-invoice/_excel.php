<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=report-excel.xls");
//echo '<pre>'. print_r($dataProvider->models) .'</pre>';
?>
<table border="1">
    <thead>
        <tr>
            <th>No.</th>
             <?php foreach($dataProvider[0] as $key => $value){
                echo "<th>". $key."</th>";
            } ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no=1;
        $total=0;
        foreach($dataProvider as $models) {
            echo "<tr>";
            echo "<td>".$no."</td>";
            foreach($models as $key => $value){
                if($key=='Tgl Invoice Masuk'){
                    echo "<td>". date('d-m-y',strtotime($value))."</td>";                    
                }else if($key=='Total Amount'){
                    echo "<td class='text-right'>". number_format($value,2,',','.')."</td>";
                    $total+=$value;
                }else {
                    echo "<td>". $value."</td>";
                }
            }
            echo "</tr>";
            $no++; 
        }?>
        <tr>
            <td colspan="<?= count($dataProvider[0]);?>"> Total</td>
            <td><?= number_format($total,2,',','.')?></td>
        </tr>
    </tbody>
</table>