
<table class="table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <td>No.</td>
            <?php foreach($dataProvider[0] as $key => $value){
                echo "<th>". $key."</th>";
            } ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no=1;
        $total = 0;
        foreach($dataProvider as $models) {
            echo "<tr>";
            echo "<td>".$no."</td>";
            foreach($models as $key => $value){
                if($key=='Tgl Invoice Masuk'){
                    echo "<td>". date('d-m-Y',strtotime($value))."</td>";                    
                }else if($key=='Total Amount'){
                    echo "<td class='text-right'>". number_format($value,2,',','.')."</td>";
                    $total+=$value;
                }else {
                    echo "<td>". $value."</td>";
                }
            }
            echo "</tr>";
            $no++;
        }
        ?>
        <tr>
            <th colspan="<?= count($dataProvider[0]);?>"> Total</th>
            <th><?= number_format($total,2,',','.')?></th>
        </tr>
    </tbody>
</table>