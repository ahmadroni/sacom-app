<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- /.login-logo -->
<div class="login-box-body">

    <p class="login-box-msg">Sign in to start your session</p>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <?=
    $form->field($model, 'username', [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>',
    ])->textInput(['autofocus' => true, 'placeholder' => 'Username'])->label(false)
    ?>
    <?=
    $form->field($model, 'password', [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
    ])->passwordInput(['autofocus' => true, 'placeholder' => 'Password'])->label(false)
    ?>
    <div class="row">
        <div class="col-xs-8">
            <?= $form->field($model, 'rememberMe',[
                'options' => ['class' => 'checkbox icheck'],                  
            ])->checkbox([
                'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}',
            ]) ?>           
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div>
        <!-- /.col -->
    </div>
    
    <!--div style="color:#999;margin:1em 0">
        If you forgot your password you can <?= Html::a('reset it', ['user/request-password-reset']) ?>.
        For new user you can <?= Html::a('signup', ['user/signup']) ?>.
    </div-->    
<?php ActiveForm::end(); ?>
<?= Html::a('I forgot my password', ['/user/request-password-reset']) ?>
</div>

