<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use bupy7\cropbox\CropboxWidget;
?>
<div class="box box-info site-signup">
    <div class="box-header with-border">
        <h3 class="box-title">Update Profile</h3>
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'id' => 'form-update',
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'options' => ['enctype' => 'multipart/form-data'],
        ]);
        ?>
        <?= $form->errorSummary($model,['header'=>''])?>
        <?=
        $form->field($model, 'image')->widget(CropboxWidget::className(), [
            'croppedDataAttribute' => 'crop_info',
            'croppedImagesUrl' => (empty($model->images_path) ? '' : Yii::getAlias('@web') . $model->images_path),
        ]);
        ?>

        <?php /* $form->field($model, 'full_name')->textInput(['maxlength' => true])*/ ?>

        <?php /*$form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'password')->passwordInput() */?>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-8">
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>