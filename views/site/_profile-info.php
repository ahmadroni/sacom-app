<div class="box box-primary">
    <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="<?= Yii::getAlias('@web') . $model->images_path; ?>" alt="User profile picture">

        <h3 class="profile-username text-center"><?= $model->role ?></h3>

        <p class="text-muted text-center">&nbsp;</p>

        <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
                <b>Nama Lengkap </b> <a class="pull-right"><?= $model->full_name ?></a>
            </li>
            <li class="list-group-item">
                <b>User Aplikasi</b> <a class="pull-right"><?= $model->username; ?></a>
            </li>
            <li class="list-group-item">
                &nbsp;
            </li>
            <li class="list-group-item">
                &nbsp;
            </li>
        </ul>

        <button type="button" class="btn btn-primary btn-block" id="btn-update"><b>Update Profile</b></button>
    </div>
    <!-- /.box-body -->
</div>