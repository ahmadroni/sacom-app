<?php
$this->title = 'Profile User';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Profile Image -->
<?= $this->render('_profile-update', ['model' => $model]); ?>
<?php ob_start(); ?>
<script>
    $('#btn-update').click(function(){
        $('#profile-update').show();
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
