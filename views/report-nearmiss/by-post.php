<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MstSupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = 'Report';
$this->params['breadcrumbs'][] = 'Near Miss';
?>
<div class="box box-info mst-supplier-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?> </h3>
    </div>
    <div class="box-body">        
        <?= $this->render('//report/_report-no-export', ['model' => $model]); ?>
        <?php if(count($dataProvider) > 0 ): ?>
        <table class="table table-bordered table-striped table-responsive">
            <thead>
                <tr>
                    <th>No.</th>
                    <?php foreach($dataProvider[0] as $key => $value){
                        if($key != 'id'){
                            echo "<th>". $key."</th>";
                        }
                    } ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $rows=0;
                foreach($dataProvider as $models) {
                    echo "<tr><td>".$rows."</td>";
                    $cols = 0;
                    foreach($models as $key => $value){
                        if($key != 'id'):
                            if($cols==0){
                                echo "<td>". Html::a($value,['/trx-nearmiss/detail','id'=>$models['id']])."</a></td>";
                            }else{
                                echo "<td>".$value."</td>";
                            }
                            $cols++;
                         endif;
                    }
                    echo "</tr>";
                    $rows++;
                }
                ?>
            </tbody>
        </table>
        <?php else : ?>
        <div class="callout callout-warning" style="padding: 5px 10px 20px 10px;">
            <h5><i class="fa fa-warning"></i> Info : </h5>
            Data not found on your search criteria
        </div> 
        <?php endif; ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>
<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    $(document).ready(function () {
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            setDate: new Date(),
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 