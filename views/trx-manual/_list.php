<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
Pjax::begin(['id' => $currentController . '-pjax']);
?>
<?=

GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //'id',
        'number',
        [
            'attribute' => 'location_id',
            'value' => 'location.location_name',
            'filter' => Html::activeDropDownList($searchModel, 'location_id', ArrayHelper::map(app\models\MstLocation::listData(), 'id', 'location_name'), ['class' => 'form-control', 'prompt' => '= Pilih Location=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'reason',
            'value' => 'manualReason.name',
            'filter' => Html::activeDropDownList($searchModel, 'reason', ArrayHelper::map(app\models\MstLookup::listData('ManualReason'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Reason=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        /*
          [
          'attribute' => 'category',
          'value' => 'manualCategory.name',
          'filter' => Html::activeDropDownList($searchModel, 'category', ArrayHelper::map(app\models\MstLookup::listData('ManualCategory'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Category=']),
          'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
          ], */
        'quantity',
        [
            'attribute' => 'sender',
            'value' => 'senderBy.username',
            'filter' => Html::activeDropDownList($searchModel, 'sender', ArrayHelper::map(app\models\MstUser::listData(), 'id', 'username'), ['class' => 'form-control', 'prompt' => '= Pilih Sender=']),
        ],
        //'gold_category',
        //'description',
        //'unit',
        //'amount',
        //'jde_no',
        //'sold_to',
        //'deliver_to',
        //'transporter',
        //'vehicle_no',
        //'driver_name',
        //'notes',
        //'sender',
        //'is_active',
        //'created_by',
        //'created_at',
        //'updated_by',
        //'updated_at',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {proses} {update} {delete}',
            'options' => ['style' => 'width:120px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $key,
                    ]);
                },
                'proses' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-cog"></span>', [
                                'class' => 'btn btn-xs btn-info btn-proses',
                                'title' => Yii::t('yii', 'Proses Data'),
                                'value' => $model['id'],
                    ]);
                },
                'update' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $key], [
                                'class' => 'btn btn-xs btn-warning btn-update',
                                'title' => Yii::t('yii', 'Update Data'),
                                'value' => $key,
                    ]);
                },
                'delete' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                    ]);
                }
            ],
        ],
    ],
]);
?>
<?php Pjax::end(); ?>