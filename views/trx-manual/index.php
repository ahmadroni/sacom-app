<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;

$currentController = Yii::$app->controller->id;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxManualSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'DD Manual');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-manual-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>        
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i>  Add', ['create'], ['class' => 'btn btn-info btn-sm']) ?>       
        </div>
    </div>
    <div class="box-body">
        <?php echo $this->render('_index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later  ?>
<script>
    $('.select2').select2();
    
    $(".tanggal").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        setDate: new Date(),
    });
    // add button
    $('.btn-add').on('click', function () {
        $.ajax({
            url: '<?= Url::to([$currentController . '/create']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New <?= Html::encode($this->title) ?>');                
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // update
    $(document).on('click', '.btn-proses', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/proses']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Proses <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });
    // view
    $(document).on('click', '.btn-view', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/view']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('View <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    $('#modal-input').on('submit', 'form', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (data) {
                if (data.result == 'success') {
                    $.pjax.reload({container: '#<?= $currentController ?>-pjax'});
                    $('#modal-input').modal('hide');
                    $(this).trigger('reset');
                } else {
                    $('#modal-data').html(data);
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
        return false;
    });
    
    $('#modal-input').on('shown.bs.modal', function(){
        // set tanggal
        $('#modal-input').find(".tanggal").datepicker({
            'autoclose': true,
            'format': 'yyyy-mm-dd',
            'setDate': new Date(),
        });
    });
    
    $('#modal-input').on('keypress','.unit', function(e){
        if(e.which == 13){
            var qty = $('#modal-input').find('.quantity').val();
            var unit = $(this).val();
            $('#modal-input').find('.amount').val(parseInt(qty) * parseInt(unit) );
            return false;
        }
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 