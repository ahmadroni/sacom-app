<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrxForm */

$this->title = Yii::t('app', 'Deatil DD Manual');
$this->params['breadcrumbs'][] = ['label' => 'DD Manual', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $model->number;
?>
<div class="invoice" style="margin:0px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3 class="page-header">
                <i class="fa fa-globe"></i>
                <?= $model->description;?>
                <small class="pull-right">Date : <?= date('d-m-Y h:m', strtotime($model->created_at));?></small>
            </h3>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 invoice-col">
            From <br>
            <?php echo "<b>". $model->transportBy->company_name ."</b><br>"; ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 invoice-col">
            Deliver To :<br>
            <?php echo "<b>". $model->deliver_to."</b><br>"; ?>
            <br>
            Sold To : <br>
            <?php echo "<b>". (empty($model->sold_to) ? "-" : $model->sold_to)."</b><br>"; ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 invoice-col">
            <?php 
                echo "<b>Number :#". $model->number ."</b><br>".
                "Reason :<br><b>". $model->manualReason->name ."</b><br>".
                "Category:<br><b>". $model->manualCategory->name ."</b><br>"; 
            ?>
        </div>
    </div>
    <div class="row">
        <div calss="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th style="width:10%;">Quantity</th>
                        <th style="width:10%;">Unit</th>
                        <th style="width:10%;">JDE No.</th>
                        <th>Description</th>
                        <th style="width:10%;">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $model->quantity; ?></td>
                        <td><?= $model->unit; ?></td>
                        <td><?= $model->jde_no; ?></td>
                        <td><?= $model->description; ?></td>
                        <td class="text-right"><?= $model->amount; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <p class="lead">Remark :</p>
            <?php if(!empty($model->notes)) : ?>
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?= $model->notes; ?>
            </p>
            <?php endif;?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <p class="lead">Amount :</p>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th style="width:50%;">Amount </th>
                            <th><?= $model->amount; ?></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
