<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrxForm */

$this->title = Yii::t('app', 'New DD Manual');
$this->params['breadcrumbs'][] = ['label' => 'DD Manual', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-callcenter-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?> Form</h3>  
    </div>
    <div class="box-body">
        <?= $this->render('_form', ['model' => $model]) ?>
    </div>
</div>
