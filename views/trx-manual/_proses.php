<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="trx-invoice-view">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'number',
                    'location.location_name',
                    'manualReason.name',
                    'manualCategory.name',
                    'manualGoldCategory.name',
                    'description',
                    'quantity',
                    'unit',
                    'amount',
                ],
            ])
            ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [                    
                    'jde_no',
                    'sold_to',
                    'deliver_to',
                    'transporter',
                    'vehicle_no',
                    'driver_name',
                    'notes',
                    'sender',
                ],
            ])
            ?>
        </div>
    </div>   

    <div class="box-footer">   
    	<?= Html::a($proses, ['proses', 'id' => $model->id], [
			'class' => 'btn btn-success pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to proses this item?',
				'method' => 'post',
			],
		]) ?>
	<?= Html::a('Reject', ['reject', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Are you sure you want to reject this item?',
				'method' => 'post',
			],
		]) ?>
    </div>

</div>
