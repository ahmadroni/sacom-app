<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
//Pjax::begin(['id' => $currentController . '-pjax']);
?>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'created_at',
            'value' => function($data){
                return date('d-m-Y', strtotime($data->created_at));
            },
            'options' => ['class' => 'col-sm-1 col-md-1 col-lg-1 text-center'],
            'filter'=>Html::activeInput('text', $searchModel, 'created_at', ['class'=>'form-control tanggal'])
        ],
        'number',
        [
            'attribute' => 'location_id',
            'value' => 'location.location_name',
            'filter' => Html::activeDropDownList($searchModel, 'location_id', ArrayHelper::map(app\models\MstLocation::listData(), 'id', 'location_name'), ['class' => 'form-control select2', 'prompt' => '= Pilih Location=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        'description',
        [
            'attribute' => 'reason',
            'value' => 'manualReason.name',
            'filter' => Html::activeDropDownList($searchModel, 'reason', ArrayHelper::map(app\models\MstLookup::listData('ManualReason'), 'code', 'name'), ['class' => 'form-control select2', 'prompt' => '= Pilih Reason=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        /*
          [
          'attribute' => 'category',
          'value' => 'manualCategory.name',
          'filter' => Html::activeDropDownList($searchModel, 'category', ArrayHelper::map(app\models\MstLookup::listData('ManualCategory'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Category=']),
          'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
          ], 
        'quantity',
        
        [
            'attribute' => 'sender',
            'value' => 'senderBy.username',
            'filter' => Html::activeDropDownList($searchModel, 'sender', ArrayHelper::map(app\models\MstUser::listData(), 'id', 'username'), ['class' => 'form-control select2', 'prompt' => '= Pilih Sender=']),
        ],
        */
        //'gold_category',
        //'description',
        //'unit',
        //'amount',
        //'jde_no',
        //'sold_to',
        //'deliver_to',
        //'transporter',
        //'vehicle_no',
        //'driver_name',
        //'notes',
        //'sender',
        //'is_active',
        //'created_by',
        //'created_at',
        //'updated_by',
        //'updated_at',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'options' => ['style' => 'width:50px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $key,
                    ]);
                },
            ],
        ],
    ],
]);
?>
<?php //Pjax::end(); ?>