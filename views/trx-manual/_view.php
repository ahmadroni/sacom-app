<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrxManual */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trx Manuals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-manual-view">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'number',
                    'location.location_name',
                    [
                        'label'=>'Reason',
                        'value' =>$model->manualReason->name,
                    ],
                    [
                        'label'=>'Category',
                        'value' =>$model->manualCategory->name,
                    ],
                    [
                        'label'=>'Gold Category',
                        'value' =>$model->manualGoldCategory->name,
                    ],
                    'description',
                    'quantity',
                    'unit',
                    'amount',
                ],
            ])
            ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [                    
                    'jde_no',
                    'sold_to',
                    'deliver_to',
                    'transporter',
                    'vehicle_no',
                    'driver_name',
                    'notes',
                    'sender',
                ],
            ])
            ?>
        </div>
    </div>
    <div class="box-footer">   
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
<?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
