<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstLocation;
use app\models\MstLookup;
use app\models\MstUser;
use app\models\MstCompany;

/* @var $this yii\web\View */
/* @var $model app\models\TrxManual */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-manual-form">

    <?php
    $form = ActiveForm::begin([
                //'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-2 col-md-2 col-lg-2',
                        'offset' => '',
                        'wrapper' => 'col-sm-10 col-md-10 col-lg-10',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>
    <?php if ($model->hasErrors()) : ?>
        <div class="callout callout-warning">
            <?= $form->errorSummary($model); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(MstLocation::listData2(), 'id', 'location_name'), ['class' => 'form-control select2', 'prompt' => '= Select Lokasi ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'created_at', [
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4 col-md-4 col-lg-4',
                    'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                ],
            ])->textInput(['maxlength' => true, 'class' => 'form-control tanggal'])
            ?>
        </div>        
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'reason')
                ->dropDownList(
                    ArrayHelper::map(MstLookup::listData("ManualReason"), 'code', 'name'), 
                    [
                        'prompt' => '= Select Reason =', 
                        'class' => 'form-control reason select2'
                    ])?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'category', [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
                    'wrapper' => 'col-sm-8',
                ],
            ])->dropDownList(
                    empty($model->category) ? ArrayHelper::map(['empty' => 'empty'], 'id', 'value') : ArrayHelper::map(MstLookup::listData($model->reason), 'code', 'name'), 
                    [
                        'prompt' => '= Select Category =', 
                        'class' => 'form-control category select2'
                    ]);?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'gold_category')
                ->dropDownList(
                    empty($model->gold_category) ? ArrayHelper::map(['empty' => 'empty'], 'id', 'value') : ArrayHelper::map(MstLookup::listData($model->category), 'code', 'name'), 
                    [
                        'prompt' => '= Select Gold Category =', 
                        'class' => 'form-control gold-category select2'
                    ]);?>
        </div>
    </div>
    <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'rows' => 4]) ?>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'quantity')->textInput(['class' => 'form-control quantity'])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'unit')->textInput(['class' => 'form-control unit'])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">            
            <?=
            $form->field($model, 'amount')->textInput(['class' => 'form-control unit'])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'jde_no')->textInput(['maxlength' => true])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'sold_to')->textInput(['maxlength' => true])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"> 
            <?=
            $form->field($model, 'deliver_to')->textInput(['maxlength' => true])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
             <?=
            $form->field($model, 'transporter')
                ->dropDownList(
                    ArrayHelper::map(MstCompany::listDataByType(MstCompany::TRANSPORTER_TYPE), 'id', 'name'), 
                    [
                        'class' => 'form-control select2',
                        'prompt' => '= Select Transporter ='
                    ]); ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'vehicle_no')->textInput(['maxlength' => true])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'driver_name')->textInput(['maxlength' => true])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model, 'notes')->textarea(['maxlength' => true, 'rows' => 4]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model, 'sender', [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4 col-md-4 col-lg-4',
                    'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                ],
            ])->dropDownList(
                ArrayHelper::map(MstUser::listDataDefault(Yii::$app->user->identity->id), 'id', 'username'), 
                [
                    'class' => 'form-control select2',
                    'prompt' => '= Select Person ='
                ]);?>
        </div> 
    </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    $(document).ready(function () {
        $('.select2').select2();
        
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            setDate: new Date(),
        });
    });
    $(document).on('change', '.reason', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(['ajax/get-lookup']) ?>',
            data: {'type': id},
            type: 'get',
            dataType: 'json',
            success: function (result) {
                $(document).find('.category').empty();
                $(document).find('.category').append('<option value="">= Pilih Kategori =</option>');
                $.each(result.data, function (index, item) {
                    $(document).find('.category').append('<option value="' + item.code + '">' + item.name + '</option>');
                });
            },
        });
    });

    $(document).on('change', '.category', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(['ajax/get-lookup']) ?>',
            data: {'type': id},
            type: 'get',
            dataType: 'json',
            success: function (result) {
                $(document).find('.gold-category').empty();
                $(document).find('.gold-category').append('<option value="">= Pilih Gold Kategori =</option>');
                $.each(result.data, function (index, item) {
                    $(document).find('.gold-category').append('<option value="' + item.code + '">' + item.name + '</option>');
                });
            },
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 