<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ResetPassword */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box-body">
    <h3 class="box-title text-center"><?= Html::encode($this->title) ?></h3>

    <p class="text-center">Please choose your new password:</p>

   <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <?=
        $form->field($model, 'password', [
            'options' => ['class' => 'form-group has-feedback'],
            'inputTemplate' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
        ])->passwordInput(['autofocus' => true, 'placeholder' => 'Password'])->label(false)
        ?>

        <?=
        $form->field($model, 'retypePassword', [
            'options' => ['class' => 'form-group has-feedback'],
            'inputTemplate' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
        ])->passwordInput(['autofocus' => true, 'placeholder' => 'Retype Password'])->label(false)
        ?>
        <div class="row">
            <div class="col-xs-6">         
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <?= Html::submitButton('Reset Password', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
    <?php ActiveForm::end(); ?>
</div>
