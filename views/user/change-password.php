<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ChangePassword */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box-body">
    <h3 class="box-title text-center"><?= Html::encode($this->title) ?></h3>
    <p class="text-center">Please fill out the following fields to change password:</p>

    <?php $form = ActiveForm::begin(['id' => 'form-change']); ?>
         <?=
        $form->field($model, 'oldPassword', [
            'options' => ['class' => 'form-group has-feedback'],
            'inputTemplate' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
        ])->passwordInput(['autofocus' => true, 'placeholder' => 'Old Password'])->label(false)
        ?>

        <?=
        $form->field($model, 'newPassword', [
            'options' => ['class' => 'form-group has-feedback'],
            'inputTemplate' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
        ])->passwordInput(['autofocus' => true, 'placeholder' => 'New Password'])->label(false)
        ?>
    
        <?=
        $form->field($model, 'retypePassword', [
            'options' => ['class' => 'form-group has-feedback'],
            'inputTemplate' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>',
        ])->passwordInput(['autofocus' => true, 'placeholder' => 'Retype Password'])->label(false)
        ?>
        <div class="row">
            <div class="col-xs-6">         
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <?= Html::submitButton('Change Password', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
    <?php ActiveForm::end(); ?>
</div>
