<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\PasswordResetRequest */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box-body">
    <h3 class="box-title text-center"><?= Html::encode($this->title) ?></h3>
    <p class="text-center">Please fill out your <b>Username</b>. A link to reset password will be sent there.</p>

    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
    <?=
    $form->field($model, 'email', [
        'options' => ['class' => 'form-group has-feedback'],
        'inputTemplate' => '{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>',
    ])->textInput(['autofocus' => true, 'placeholder' => 'Username'])->label(false)
    ?>
    <div class="row">
        <div class="col-xs-6">         
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
            <?= Html::submitButton('Reset Password', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div>
        <!-- /.col -->
    </div>  
<?php ActiveForm::end(); ?>
</div>
