<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="mst-kota-view">
<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama_kota',
            'propinsi.nama_propinsi'
        ],
    ]) ?>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>
