<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstPropinsi;

/* @var $this yii\web\View */
/* @var $model app\models\MstPropinsi */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="mst-kota-form">

    <?php $form = ActiveForm::begin([
                'id' =>'form-mst-kota',
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4 col-md-4 col-lg-4',
                        'offset' => '',
                        'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]); ?>
    
    <?= $form->field($model, 'propinsi_id')->dropDownList(ArrayHelper::map(MstPropinsi::find()->asArray()->all(), 'id', 'nama_propinsi'), ['prompt' => '= Pilih Propinsi =']) ?>
    
    <?= $form->field($model, 'nama_kota')->textInput(['maxlength' => true]) ?>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
