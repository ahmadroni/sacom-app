<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MstSupplier */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mst Suppliers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mst-supplier-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'supplier_name',
            [
                'attribute' => 'address',
                'value'=>$model->address .', Kab.'. $model->kota->nama_kota.', Prop.'. $model->propinsi->nama_propinsi
            ],
            'email:email',
            'no_hp',
            'no_telp',          
            [
                'label'=>'Kategori',
                'value'=> $model->category == null ? "-" : $model->category->name,
            ],
        ],
    ]) ?>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
