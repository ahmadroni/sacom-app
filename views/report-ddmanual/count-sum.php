<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\grid\GridView;

$currentController = Yii::$app->controller->id;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxManualSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Report DD Manual');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-manual-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title)." - Count And Summary" ?></h3> 
    </div>
    <div class="box-body">
        <?= $this->render('//report/_report', ['model' => $model]); ?>
        <?php
        Pjax::begin(['id' => $currentController . '-pjax']);
        ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'location_name',
                'COUNT - Asset Sales',
                'SUM - Asset Sales',
                'COUNT - JDE System Down',
                'SUM - JDE System Down',
                'COUNT - Scrapt Sales',
                'SUM - Scrapt Sales',
                'COUNT - Waste Landfilled',
                'SUM - Waste Landfilled',
                'COUNT - Third Parties Process',
                'SUM - Third Parties Process',
                'COUNT - Others',
                'SUM - Others',
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    $(document).ready(function () {
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            setDate: new Date(),
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 