<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use mdm\admin\components\MenuHelper;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\TrxConversation;
use app\models\TrxHazard;
use app\models\TrxNearmiss;
use app\models\TrxInvoice;

AppAsset::register($this);

$safetyCount = Yii::$app->user->isGuest ? 0 : TrxConversation::countByUser();
$hazardCount = Yii::$app->user->isGuest ? 0 : TrxHazard::countByUser();
$nearmissCount = Yii::$app->user->isGuest ? 0 : TrxNearmiss::countByUser();
$trxCount = Yii::$app->user->isGuest ? 0 : TrxInvoice::trxAlertTotal();
// image user url
$imageUserUrl = Yii::getAlias('@web').'/img/user2-160x160.jpg';
$userName = "Guest";
$userRole = "Guest";
if(!Yii::$app->user->isGuest) {
    if(file_exists(Yii::getAlias('@web').'/'.Yii::$app->user->identity->image)){
        $imageUserUrl =Yii::getAlias('@web').'/'.Yii::$app->user->identity->image;
    }
    $userName = Yii::$app->user->identity->username;
    $userRole = Yii::$app->user->identity->listRole;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" type="image/png" href="<?= \Yii::$app->request->BaseUrl . '/img/logo.png'; ?>" />
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-green sidebar-mini">
        <?php $this->beginBody() ?>

        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                        <!--b>U</b>SG -->
                        <?= Html::img('@web/img/logo.png', ['height' => '40', 'alt' => 'Logo Image']); ?>
                    </span>
                    <!-- logo for regular state and mobile devices -->
                    <?= Html::img('@web/img/usg-boral.png', ['height' => '50', 'alt' => 'Logo Image']); ?>
                    <!--span class="logo-lg"><b>Admin</b>LTE</span-->
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Notifications: style can be found in dropdown.less -->
                            <?php if(!Yii::$app->user->isGuest) : ?>
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-exchange"></i>
                                    <span class="label label-warning"><?= $trxCount ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have <?= $trxCount ?> transaction</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <?php foreach (TrxInvoice::trxAlert() as $alert) : ?>
                                                <li>
                                                    <a href="<?= Url::to([$alert['link']]) ?>">
                                                        <i class="<?= $alert['icon'] ?>"></i> <?= $alert['jumlah'] .' '.$alert['link_name'] .' notification' ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <!-- Messages: style can be found in dropdown.less-->
                            <?php if (!Yii::$app->user->isGuest && $hazardCount > 0) : ?>
                                <li class="dropdown messages-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-life-ring"></i>
                                        <span class="label label-success"><?= $hazardCount ?></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <?= $hazardCount ?> hazard to response</li>
                                        <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu">
                                                <?php foreach (TrxHazard::listByUser() as $item): ?>
                                                    <li>
                                                        <?= Html::a('<i class="fa fa-warning text-yellow"></i> ' . $item->title, ['/trx-hazard/response', 'id' => $item->id]) ?>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <?php if (!Yii::$app->user->isGuest && $safetyCount > 0) : ?>
                                <li class="dropdown notifications-menu">
                                    <a href="<?= Url::to(['trx-safety/index']) ?>" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="label label-warning"><?= $safetyCount ?></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <?= $safetyCount ?> Safety to response</li>

                                        <li>
                                            <ul class="menu">
                                                <?php foreach (TrxConversation::listByUser() as $item): ?>
                                                    <li>
                                                        <?= Html::a('<i class="fa fa-users text-aqua"></i> ' . $item->title, ['/trx-safety/response', 'id' => $item->id]) ?>
                                                    </li>       
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <?php if (!Yii::$app->user->isGuest && $nearmissCount > 0): ?>
                                <li class="dropdown tasks-menu">
                                    <a href="<?= Url::to(['trx-nearmiss/index']) ?>" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-flag-o"></i>
                                        <span class="label label-danger"><?= $nearmissCount ?></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <?= $nearmissCount ?> Near Miss to response</li>
                                        <li>
                                            <ul class="menu">
                                                <?php foreach (TrxNearmiss::listByUser() as $item): ?>
                                                    <li>
                                                        <?= Html::a('<i class="fa fa-gavel text-yellow"></i> ' . $item->title, ['/trx-nearmiss/response', 'id' => $item->id]) ?>
                                                    </li> 
                                                <?php endforeach; ?>      
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= $imageUserUrl; ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?= $userName; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= $imageUserUrl;?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?= $userName; ?>
                                            <small><?= $userRole; ?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!--li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Followers</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <?= Html::a('Profile', ['//site/profile'],['class'=>'btn btn-default btn-flat']); ?>
                                        </div>
                                        <div class="pull-right">
                                            <?php
                                            echo Html::beginForm(['/site/logout'], 'post') .
                                            Html::submitButton(
                                                    'Logout', ['class' => 'btn btn-default btn-flat']
                                            ) .
                                            Html::endForm()
                                            ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#"><i class="fa fa-user-secret"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= $imageUserUrl; ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?= $userName; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?= $userRole; ?></a>
                        </div>
                    </div>
                    <!-- search form 
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <!--ul class="sidebar-menu" data-widget="tree">
                        <li class="header">User Menu</li>
                    </ul-->
                    <?php
                    $callback = function($menu) {
                        $data = eval($menu['data']);
                        //if have syntax error, unexpected 'fa' (T_STRING)  Errorexception,can use
                        //$data = $menu['data'];
                        return [
                            'label' => $menu['name'],
                            'url' => [$menu['route']],
                            'option' => $data,
                            'icon' => $menu['icons'],
                            'items' => $menu['children'],
                        ];
                    };
                    if(!Yii::$app->user->isGuest) :
                        $items = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback, true);
                        echo app\components\MenuHelper::widget([
                            'options' => ['class' => 'sidebar-menu'],
                            'items' => $items
                        ]);
                    endif;
                    ?>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- End Sidebar -->

            <!-- Content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?= $this->title; ?>
                        <small><?= isset($this->params['sub-title']) ? $this->params['sub-title'] : ""; ?></small>
                    </h1>
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                </section>


                <?= Alert::widget() ?>
                <!-- Main content -->
                <section class="content">
                    <?= $content ?>
                </section>
            </div>            
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; My Company <?= date('Y') ?> <a href="https://adminlte.io">SACOM</a>.</strong> All rights
            reserved.
        </footer>

        <!-- control-sidebar -->
        <!--?= $this->render("_control-sidebar.php") ?-->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->        
        <?php $this->endBody() ?>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
    </body>
</html>
<?php $this->endPage() ?>
