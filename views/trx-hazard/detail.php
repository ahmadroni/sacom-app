<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model app\models\TrxHazard */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trx Hazards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="box box-info trx-conversation-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Inflector::camel2words($model->title) ?></h3>        
        <div class="box-tools">
            <?php if ($model->allowComment) : ?>
                <a href="<?= Url::to(['trx-hazard/response', 'id' => $model->id]) ?>" class="btn btn-sm btn-success"><i class="fa fa-reply-all margin-r-5"></i> Response</a>
            <?php endif; ?>
            <?php if ($model->isAuthor && !$model->isClose) : ?>
                <a href="<?= Url::to(['trx-hazard/closing', 'id' => $model->id]) ?>" class="btn btn-sm btn-warning"><i class="fa fa-close margin-r-5"></i> Closing</a>
            <?php endif; ?>
        </div>

    </div>
    <div class="box-body">
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?= $imageUrl; ?>" alt="user image">
                <span class="username">
                    <a href="#"><?= $model->postBy->fullname ?></a>
                </span>
                <span class="description">
                    <i>Lokasi :</i>
                    <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                    , <i>Area :</i>
                    <strong><?= $model->area->area_name . ' ,' . $model->area_other ?> </strong>
                    , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
                </span>
            </div>
            <strong> 
                Resiko : <i><?= $model->riskLookup->name ?> </i> | Kemungkinan : <i><?= $model->posibilityLookup->name ?> </i> |
                Tingkatan Resiko : <i><?= $model->risk_level ?></i>
            </strong> <br/>
            <strong><i class="fa fa-comment-o margin-r-5"></i> Penjelasan Singkat : </strong>
            <p> 
                <!-- /.user-block -->
                <?= HtmlPurifier::process($model->description) ?> 
            </p>
            <strong><i class="fa fa-users margin-r-5"></i> Person Follow Up :</strong>
            <ul class="timeline timeline-inverse">
                <?php foreach ($model->trxHazardDetails as $key => $item) : ?>
                    <li>
                        <i class="fa fa-user-o bg-aqua"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> <?= date('d-m-Y', strtotime($item->do_date)); ?></span>
                            <h3 class="timeline-header"><a href="#"><?= $item->forUser->username ?></a> [<?= $item->status ?>]</h3>
                            <div class="timeline-body"><?= HtmlPurifier::process($item->notes) ?> </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            
            <!-- /.post -->  
            <strong><i class="fa fa-list margin-r-5"></i> List Follow Up :</strong>
            <ul class="timeline timeline-inverse">
                <?php foreach ($model->trxHazardComments as $comment): ?>
                    <li>
                        <i class="fa fa-comments bg-yellow"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> <?= date('d-m-Y H:i:s', strtotime($comment->created_at)); ?></span>
                            <h3 class="timeline-header"><a href="#"><?= $comment->postBy->username ?></a> [<?= $comment->status ?>]</h3>
                            <div class="timeline-body"><?= HtmlPurifier::process($comment->content) ?> </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="list-inline">
                <li class="pull-right">
                    <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Follow Up(<?= $model->commentCount ?>)</a>
                </li>
            </ul>
        </div>
    </div>
</div>