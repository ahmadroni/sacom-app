<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model app\models\TrxHazard */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trx Hazards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="trx-hazard-view">
    <div class="post">
        <div class="user-block">
            <img class="img-circle img-bordered-sm" src="<?= $imageUrl; ?>" alt="user image">
            <span class="username">
                <a href="#"><?= $model->postBy->fullname ?></a>
            </span>
            <span class="description">
                <i>Lokasi :</i>
                <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                , <i>Area :</i>
                <strong><?= $model->area->area_name .' ,'. $model->area_other ?> </strong>
                , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
            </span>
        </div>
        <strong> 
            Resiko : <i><?= $model->riskLookup->name ?> </i> | Kemungkinan : <i><?= $model->posibilityLookup->name ?> </i> |
            Tingkatan Resiko : <i><?= $model->risk_level ?></i>
        </strong> <br/>
        <strong><i class="fa fa-comment-o margin-r-5"></i> Penjelasan Singkat : </strong>
        <p> 
            <!-- /.user-block -->
            <?= HtmlPurifier::process($model->description) ?> 
        </p>
        <strong><i class="fa fa-users margin-r-5"></i> Person Follow Up :</strong>
        <table class="table table">
            <?php
            $detail = new app\models\TrxHazardDetail();
            $detail->loadDefaultValues();
            ?>
            <thead>
                <tr>
                    <th style="width: 20px;">No</th>
                    <th><?= $detail->getAttributeLabel('notes') ?></th>
                    <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('for_user') ?></th>
                    <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('status') ?></th>
                    <th style="width:160px;"><?= $detail->getAttributeLabel('do_date') ?></th>
                </tr>
            </thead>
            <tbody id="list-detail">
                <?php foreach ($model->trxHazardDetails as $key => $item) : ?>
                    <tr>
                        <td><?= ($key + 1) ?></td>
                        <td><?= $item->notes ?></td>
                        <td><?= $item->forUser->username ?></td>
                        <td><?= $item->status ?></td>
                        <td><?= date('d-m-Y', strtotime($item->do_date)) ?></td>
                    </tr>    
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">   
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
