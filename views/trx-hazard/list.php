<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Inflector;

$currentController = Yii::$app->controller->id;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxHazardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hazard');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-hazard-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Inflector::camel2words($this->title) ?></h3>        
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i>  Add', ['create'], ['class' => 'btn btn-info btn-sm']) ?>             
        </div>
    </div>
    <div class="box-body">
        <?php echo $this->render('_list', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later  ?>
<script>
    // add button
    $('.btn-add').on('click', function () {
        $.ajax({
            url: '<?= Url::to([$currentController . '/create']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('#modal-input').find('#btn-add-item').click();
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // update
    $(document).on('click', '.btn-update', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/update']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Update <?= Html::encode($this->title) ?>');                
                // membuat nomor
                nomorUrut();               
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // view
    $(document).on('click', '.btn-view', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/view']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('View <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    $('#modal-input').on('submit', 'form', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (data) {
                if (data.result == 'success') {
                    $.pjax.reload({container: '#<?= $currentController ?>-pjax'});
                    $('#modal-input').modal('hide');
                    $(this).trigger('reset');
                } else {
                    $('#modal-data').html(data);                    
                    // membuat nomor
                    nomorUrut();
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
        return false;
    });
    
    $('#modal-input').on('shown.bs.modal', function(){
        // set tanggal
        $('#modal-input').find(".tanggal").datepicker({
            'autoclose': true,
            'format': 'yyyy-mm-dd',
            'setDate': new Date(),
        });
    });
    // buat nomor urut
    function nomorUrut(){
        $.each($('#modal-input').find('#list-detail >tr'), function(index, item){
            $(this).find('.nomor').text(index);
        }); 
    }
    
    // add tembusan button
    var detail_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
    $('#modal-input').on('click','#btn-add-item', function () {
        var index = $("#list-item >tr").length;
        index -= 1;
        detail_k += 1;
        $('#modal-input').find('#list-detail').append('<tr>' + $('#new-detail-block').html().replace(/__id__/g, 'new' + detail_k) + '</tr>');
        //$('#modal-input').find('#list-detail').find("input[type='hidden'][name*='new" + detail_k + "']").val(index);        
        // nomor urut
        nomorUrut();        
        // datepicker
        $('#modal-input').find('#TrxHazardDetails_new'+ detail_k +'_do_date').datepicker({
            'autoclose': true,
            'format': 'yyyy-mm-dd',
            'setDate': new Date(),
        });
    });

    // remove tembusan button
    $('#modal-input').on('click', '.btn-remove-detail', function () {
        $(this).closest('tbody tr').remove();        
        nomorUrut();
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 