<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model app\models\TrxForm */

$this->title = Yii::t('app', 'Closing Safety');
$this->params['breadcrumbs'][] = ['label' => 'Safety Conversation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-callcenter-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Inflector::camel2words($this->title ."Form") ?> Form</h3>  
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    //'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2 col-md-2 col-lg-2',
                            'offset' => '',
                            'wrapper' => 'col-sm-10 col-md-10 col-lg-10',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
        ]);
        ?>
        <?php if ($model->hasErrors()) : ?>
            <div class="callout callout-warning">
                <?= $form->errorSummary($model); ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'content')->textArea(['placeholder' => 'Type Comment', 'rows' => 4])->label('Pernyataan Closing') ?>
        <div class="modal-footer">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'hazard_id')->hiddenInput()->label(false) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
