<?php

use yii\helpers\Html;
use yii\widgets\ListView;

$currentController = Yii::$app->controller->id;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxHazardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hazard');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="callout callout-info" style="padding: 5px 10px 20px 10px;">
    <h5><i class="fa fa-info"></i> Note : </h5>
    Click Add button to create new Nearmiss
    <?= Html::a('<i class="fa fa-plus"></i> Add', ['create'], ['class' => 'btn btn-sm btn-primary pull-right']); ?>
</div>   

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'summary' => '',
]);
?>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>

</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 