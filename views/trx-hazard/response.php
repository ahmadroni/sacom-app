<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Inflector;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Safety Hazard'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = 'Response';

$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}

?>
<div class="box box-info trx-conversation-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Inflector::camel2words($model->title) ?></h3>       
    </div>
    <div class="box-body">
        <!-- Post -->
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?= $imageUrl; ?>" alt="user image">
                <span class="username">
                    <a href="#"><?= $model->postBy->fullname ?></a>
                </span>
                <span class="description">
                    <i>Lokasi :</i>
                    <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                    , <i>Area :</i>
                    <strong><?= $model->area->area_name . ' ,' . $model->area_other ?> </strong>
                    , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
                </span>
            </div>
            <strong> 
                Resiko : <i><?= $model->riskLookup->name ?> </i> | Kemungkinan : <i><?= $model->posibilityLookup->name ?> </i> |
                Tingkatan Resiko : <i><?= $model->risk_level ?></i>
            </strong> <br/>
            <strong><i class="fa fa-comment-o margin-r-5"></i> Penjelasan Singkat : </strong>
            <p> 
                <!-- /.user-block -->
                <?= HtmlPurifier::process($model->description) ?> 
            </p>
            <strong><i class="fa fa-reply-all margin-r-5"></i> Need Response : </strong>
            <ul class="timeline timeline-inverse">
                <li>
                    <i class="fa fa-user bg-aqua"></i>
                    <div class="timeline-item">
                        <h3 class="timeline-header"><a href="#"><?= $detail->forUser->username; ?></a> <?= $detail->notes; ?> </h3>
                    </div>
                </li>
            </ul>
        </div> 
        <!-- /.post -->
        <?php
        if ($model->allowComment || $model->isAuthor) {
            $comment = new \app\models\TrxHazardComment;
            $comment->loadDefaultValues();
            $comment->hazard_id = $model->id;
            echo '<div class="box-footer">';
            echo $this->render('_comment', ['model' => $comment]);
            echo '</div>';
        }
        ?>    
    </div>  
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 