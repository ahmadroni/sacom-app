<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstLocation;
use app\models\MstArea;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\TrxHazard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-hazard-form">

    <?php
    $form = ActiveForm::begin([
                //'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-2',
                        'offset' => '',
                        'wrapper' => 'col-sm-10',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    if ($model->hasErrors()) :
        ?>
        <div class="callout callout-warning">
            <?= $model->errorSummary($form) ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model->trxHazard, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxHazard, 'created_at',[
                'inputTemplate' => '<div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div> {input}</div>',
            ])->textInput(['maxlength' => true,'placeholder'=>'Tanggal','class'=>'form-control tanggal']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxHazard, 'location_id')->dropDownList(ArrayHelper::map(MstLocation::listData2(), 'id', 'location_name'), ['prompt' => '= Select Lokasi ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxHazard, 'area_id')->dropDownList(ArrayHelper::map(MstArea::listData(), 'id', 'area_name'), ['prompt' => '= Select Area ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxHazard, 'area_other')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxHazard, 'risk')->dropDownList(ArrayHelper::map(MstLookup::listData("HazardRisk"), 'code', 'name'), ['prompt' => '= Select Konsekuensi =', 'class' => 'form-control risk'])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxHazard, 'posibility')->dropDownList(ArrayHelper::map(MstLookup::listData("HazardPosibility"), 'code', 'name'), ['prompt' => '= Select Kemungkinan =', 'class' => 'form-control posibility'])
            ?> 
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxHazard, 'risk_level')->textInput(['maxlength' => true, 'class' => 'form-control risk_level'])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model->trxHazard, 'description')->textArea(['maxlength' => true, 'rows' => 6]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"> </div>
    </div>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Please click button + to add Follow Up item</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-primary btn-sm" id="btn-add-item"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table">
                <?php
                $detail = new app\models\TrxHazardDetail();
                $detail->loadDefaultValues();
                ?>
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th><?= $detail->getAttributeLabel('notes') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('for_user') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('status') ?></th>
                        <th style="width:160px;"><?= $detail->getAttributeLabel('do_date') ?></th>
                        <th style="width:20px;">#</th>
                    </tr>
                </thead>
                <tbody id="list-detail">
                    <?php
                    echo '<tr id="new-detail-block" style="display: none;">';
                    echo $this->render('_form-detail', [
                        'key' => '__id__',
                        'form' => $form,
                        'detail' => $detail,
                    ]);
                    echo '</tr>';
                    foreach ($model->trxHazardDetails as $key => $_detail) {
                        echo '<tr>';
                        echo $this->render('_form-detail', [
                            'key' => $_detail->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_detail->id,
                            'form' => $form,
                            'detail' => $_detail,
                        ]);
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    // buat nomor urut
    function nomorUrut() {
        $.each($('#list-detail >tr'), function (index, item) {
            $(this).find('.nomor').text(index);
        });
    }
    // add tembusan button
    var detail_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
    $(document).on('click', '#btn-add-item', function () {
        var index = $("#list-item >tr").length;
        index -= 1;
        detail_k += 1;
        $('#list-detail').append('<tr>' + $('#new-detail-block').html().replace(/__id__/g, 'new' + detail_k) + '</tr>');
        //$('#modal-input').find('#list-detail').find("input[type='hidden'][name*='new" + detail_k + "']").val(index);
        nomorUrut();

        // datepicker
        $('#list-detail').find('#TrxHazardDetails_new' + detail_k + '_do_date').datepicker({
            'autoclose': true,
            'format': 'yyyy-mm-dd',
            'setDate': new Date(),
        });
        // select2
        $('#list-detail').find('#TrxHazardDetails_new' + detail_k + '_for_user').select2();
    });

    // remove tembusan button
    $(document).on('click', '.btn-remove-detail', function () {
        $(this).closest('tbody tr').remove();
        nomorUrut();
    });

    $(document).ready(function () {
        nomorUrut();

        $(".tanggal").datepicker({
            'autoclose': true,
            'format': 'yyyy-mm-dd',
            'setDate': new Date(),
        });
    });
    
    function loadRisk(codeFrom, codeTo) {
        $.ajax({
            url: '<?= Url::to(['ajax/get-lookup-map']) ?>',
            type: 'get',
            data: {'codeFrom': codeFrom, 'codeTo': codeTo},
            dataType: 'json',
            success: function (result) {
                $('.risk_level').val(result.data.code_value);
            },
        });
    }
    $('.posibility').change(function () {
        var codeFrom = $('.risk').val();
        var codeTo = $(this).val();
        if (codeFrom != '' && codeFrom != null && codeTo != '' && codeTo != null) {
            loadRisk(codeFrom, codeTo);
        }
    });

    $('.risk').change(function () {
        var codeFrom = $(this).val();
        var codeTo = $('.posibility').val();
        if (codeFrom != '' && codeFrom != null && codeTo != '' && codeTo != null) {
            loadRisk(codeFrom, codeTo);
        }
    });
<?php
// OPTIONAL: click add when the form first loads to display the first new row
if (!Yii::$app->request->isPost && $model->trxHazard->isNewRecord)
    echo "$('#btn-add-item').click();";
?>
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 