<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
Pjax::begin(['id' => $currentController . '-pjax']);
?>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //'id',
        [
            'attribute' => 'created_by',
            'value' => 'postBy.username',
            'filter' => Html::activeDropDownList($searchModel, 'created_by', ArrayHelper::map(app\models\MstUser::listData(), 'id', 'username'), ['class' => 'form-control', 'prompt' => '= Pilih Post By =']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        'title',
        [
            'attribute' => 'location_id',
            'value' => 'location.location_name',
            'filter' => Html::activeDropDownList($searchModel, 'location_id', ArrayHelper::map(app\models\MstLocation::listData(), 'id', 'location_name'), ['class' => 'form-control', 'prompt' => '= Pilih Location =']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'risk',
            'value' => 'riskLookup.name',
            'filter' => Html::activeDropDownList($searchModel, 'risk', ArrayHelper::map(app\models\MstLookup::listData("Consequences"), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Location =']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        //'posibility',
        //'category',
        //'description',
        //'is_active',
        //'created_by',
        //'created_at',
        //'updated_by',
        //'updated_at',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
            'options' => ['style' => 'width:100px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $key,
                    ]);
                },
                'update' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $key], [
                                'class' => 'btn btn-xs btn-warning',
                                'title' => Yii::t('yii', 'Update Data'),
                                'value' => $key,
                    ]);
                },
                'delete' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                    ]);
                }
            ],
        ],
    ],
]);
?>
<?php Pjax::end(); ?>
    