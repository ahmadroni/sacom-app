<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MstWorkflow */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-workflow-form">

    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3',
                        'offset' => '',
                        'wrapper' => 'col-sm-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>
    <?php if ($model->hasErrors()) : ?>
        <div class="callout callout-warning">
            <?= $form->errorSummary($model); ?>
        </div>
    <?php endif; ?>
    <?= $form->field($model, 'wf_type')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'wf_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role_from')->dropDownList(ArrayHelper::map($roleList, 'name', 'name'), ['prompt' => '= Select From Role =']) ?>

    <?= $form->field($model, 'role_to')->dropDownList(ArrayHelper::map($roleList, 'name', 'name'), ['prompt' => '= Select To Role =']) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'status_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
