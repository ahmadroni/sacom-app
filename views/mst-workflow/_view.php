<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MstWorkflow */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mst Workflows'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mst-workflow-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'wf_type',
            'role_from',
            'role_to',
            'status',
            'status_label',
            'position',
        ],
    ]) ?>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
