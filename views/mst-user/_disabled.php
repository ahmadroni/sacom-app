<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MstUserProfile */
?>
<div class="mst-user-profile-view">
    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
    ]);
     if($model->hasErrors()) :
    ?>
    <div class="callout callout-warning">
        <?= $form->errorSummary($model); ?>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'mstUserProfile.full_name',
                    'username',
                    'email:email',
                    'mstUserProfile.no_hp',
                    'mstUserProfile.no_telp',
                    'mstUserProfile.no_ext',
                    'mstUserProfile.computer_name',
                ],
            ]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [  
                    'mstUserProfile.computer_ip',
                    'mstUserProfile.department.department_name',
                    'mstUserProfile.location.location_name',
                    'mstUserProfile.cs_group',
                    'mstUserProfile.im_team',
                    'mstUserProfile.position',
                    'mstUserProfile.coordinator',
                ],
            ]) ?>
        </div>
    </div>
    <div class="box-footer">   
        <?= $form->field($model, 'id',['template' => "{input}",'options'=>['tag'=>false]])->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'username',['template' => "{input}",'options'=>['tag'=>false]])->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status',['template' => "{input}",'options'=>['tag'=>false]])->hiddenInput()->label(false) ?>
        <?= Html::submitButton(Yii::t('app', 'Disabled User'), ['class' => 'btn btn-danger pull-right']) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
