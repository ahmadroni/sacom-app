<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrxForm */

$this->title = Yii::t('app', 'New Master User');
$this->params['breadcrumbs'][] = ['label' => 'Master User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-callcenter-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?> Form</h3>  
    </div>
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4 col-md-4 col-lg-4',
                            'offset' => '',
                            'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
        ]);
        ?>
        <?php if ($model->hasErrors()) : ?>
            <div class="callout callout-warning">
                <?= $form->errorSummary() ?>
            </div>
        <?php endif; ?>


        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <?= $form->field($model, 'username')->textInput() ?>
                
                <?= $form->field($model, 'password')->passwordInput() ?>
                
                <?= $form->field($model, 'email')->textInput() ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                
            </div>
        </div>

        <div class="modal-footer">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
