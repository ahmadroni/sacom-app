<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MstUserProfile */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mst User Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mst-user-profile-view">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'mstUserProfile.full_name',
                    'username',
                    'email:email',
                    'mstUserProfile.no_hp',
                    'mstUserProfile.no_telp',
                    'mstUserProfile.no_ext',
                    'mstUserProfile.computer_name',
                ],
            ]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [  
                    'mstUserProfile.computer_ip',
                    'mstUserProfile.department.department_name',
                    'mstUserProfile.location.location_name',
                    'mstUserProfile.cs_group',
                    'mstUserProfile.im_team',
                    'mstUserProfile.position',
                    'mstUserProfile.manager_id',
                ],
            ]) ?>
        </div>
    </div>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
