<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MstUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info mst-user-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>        
        <div class="box-tools">
            <?= Html::a('<i class="fa fa-plus"></i>  Add', ['create'], ['class' => 'btn btn-info btn-sm']) ?>           
        </div>
    </div>
    <div class="box-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
        <?php Pjax::begin(['id' => $currentController . '-pjax']); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'full_name',
                    'value'=>'mstUserProfile.full_name',
                    'filter'=> Html::activeTextInput($searchModel, 'full_name',['class'=>'form-control']),
                    'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
                ],
                'username',
                [
                    'attribute'=>'department_id',
                    'value'=>'mstUserProfile.department.department_name',
                    'filter'=> Html::activeDropDownList($searchModel, 'department_id', ArrayHelper::map(app\models\MstDepartment::listData(), 'id', 'department_name'), ['class' => 'form-control', 'prompt' => '= Pilih Department =']),
                    'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
                ],
                [
                    'attribute'=>'location_id',
                    'value'=>'mstUserProfile.location.location_name',
                    'filter'=> Html::activeDropDownList($searchModel, 'location_id', ArrayHelper::map(app\models\MstLocation::listData(), 'id', 'location_name'), ['class' => 'form-control', 'prompt' => '= Pilih Location =']),
                    'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
                ],
                //'auth_key',
                //'password_hash',
                //'password_reset_token',
                //'email:email',
                [
                    'attribute' =>'status',
                    'header'=>'Status',
                    'filter'=>['10'=>'ACTIVE','0'=>'IN ACTIVE'],
                    'value'=>function($model, $key, $index){
                        if($model->status==10){
                            return "ACTIVE";
                        }else {
                            return "IN ACTIVE";
                        }
                    },
                ],         
                //'created_at',
                //'updated_at',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'options' => ['style' => 'width:100px;'],
                    'buttons' => [
                        'view' => function($url, $model, $key) {
                            return Html::button('<span class="fa fa-eye"></span>', [
                                        'class' => 'btn btn-xs btn-success btn-view',
                                        'title' => Yii::t('yii', 'View'),
                                        'value' => $key,
                            ]);
                        },
                        'update' => function($url, $model, $key) {
                            return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-warning btn-update',
                                        'title' => Yii::t('yii', 'View'),
                                        'value' => $key,
                            ]);
                        },
                        'delete' => function($url, $model, $key) {
                            return Html::button('<span class="fa fa-trash-o"></span>',[
                                        'class' => 'btn btn-xs btn-danger btn-delete',
                                        'title' => Yii::t('yii', 'Delete'),
                                        'value' => $key,
                                        /*
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],*/
                                
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later  ?>
<script>
    // add button
    $('.btn-add').on('click', function () {
        $.ajax({
            url: '<?= Url::to([$currentController . '/create']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
            },
        });
    });

    // update
    $(document).on('click', '.btn-update', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/update']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Update <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
            },
        });
    });

    // view
    $(document).on('click', '.btn-view', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/view']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('View <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
            },
        });
    });
    
    //delete
    $(document).on('click', '.btn-delete', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/disabled']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Disabled <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
            },
        });
    });

    $('#modal-input').on('submit', 'form', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (data) {
                if (data.result == 'success') {
                    $.pjax.reload({container: '#<?= $currentController ?>-pjax'});
                    $('#modal-input').modal('hide');
                    $(this).trigger('reset');
                } else {
                    $('#modal-data').html(data);
                    $('#modal-input').modal('show');
                }
            },
        });
        return false;
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 