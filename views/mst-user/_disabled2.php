<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstLocation;
use app\models\MstDepartment;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\MstUserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-user-profile-form">

    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4 col-md-4 col-lg-4',
                        'offset' => '',
                        'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    if($model->hasErrors()) :
    ?>
    <div class="callout callout-warning">
        <?= $model->errorSummary($form) ?>
    </div>
    <?php endif; ?>
    
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUser, 'username')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUser, 'password')->passwordInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'full_name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'informed_only')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUser, 'email')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model->mstUserProfile, 'email_start',[
                'horizontalCssClasses' => [
                    'label' => 'col-sm-8',
                    'wrapper' => 'col-sm-4',
                ],
            ])->textInput() ?>
        </div>        
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <?= $form->field($model->mstUserProfile, 'email_end',[
                'horizontalCssClasses' => [
                    'label' => 'col-sm-6',
                    'wrapper' => 'col-sm-6',
                ],
            ])->textInput() ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'no_hp')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'no_telp')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'no_ext')->textInput() ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'computer_name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'computer_ip')->textInput() ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'department_id')->dropDownList(ArrayHelper::map(MstDepartment::listData(), 'id', 'department_name'), ['prompt' => '= Select Lokasi =']) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'location_id')->dropDownList(ArrayHelper::map(MstLocation::listData(), 'id', 'location_name'), ['prompt' => '= Select Lokasi =']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'cs_group')->dropDownList(ArrayHelper::map(MstLookup::listData('UserCSGroup'), 'code', 'name'), ['prompt' => '= Pilih CS Group =', 'class' => 'form-control']) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'im_team')->dropDownList(ArrayHelper::map(MstLookup::listData('UserIMTeam'), 'code', 'name'), ['prompt' => '= Pilih IM Team =', 'class' => 'form-control']) ?> 
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'position')->dropDownList(ArrayHelper::map(MstLookup::listData('UserPosition'), 'code', 'name'), ['prompt' => '= Pilih IM Team =', 'class' => 'form-control']) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'coordinator')->dropDownList(ArrayHelper::map(MstLookup::listData('UserCoordinator'), 'code', 'name'), ['prompt' => '= Pilih Koordinator =', 'class' => 'form-control'])  ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->mstUserProfile, 'safety_admin')->dropDownList(ArrayHelper::map(MstLookup::listData('UserSafetyAdmin'), 'code', 'name'), ['prompt' => '= Pilih Safety Admin =', 'class' => 'form-control'])  ?>
        </div>
    </div>

    <div class="modal-footer">
    <?= Html::submitButton(Yii::t('app', 'Disabled User'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
