<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="mst-config-view">
<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'config_name',
            'config_value',
        ],
    ]) ?>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>
