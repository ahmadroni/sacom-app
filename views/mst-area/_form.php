<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MstArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-area-form">

    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3 col-md-3 col-lg-3',
                        'offset' => '',
                        'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>

    <?= $form->field($model, 'area_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area_desc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?=
        $form->field($model, 'is_active', [
            'options' => ['class' => 'col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-sm-8 col-md-8 col-lg-8']
        ])->checkbox([
            'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}'
        ])
        ?>
    </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
