<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
?>
<?= $this->render('_customer-search', ['model' => $searchModel]); ?>
<?php Pjax::begin(['id' => $currentController . '-pjax']); ?>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'customer_name',
        [
            'attribute' => 'propinsi_id',
            'value' => 'propinsi.nama_propinsi',
        ],
        [
            'attribute' => 'kota_id',
            'value' => 'kota.nama_kota',
        ],
        'category.name',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{ok}',
            'options' => ['style' => 'width:50px;'],
            'buttons' => [
                'ok' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-check"></span>', [
                                'class' => 'btn btn-xs btn-success btn-ok-customer',
                                'title' => Yii::t('yii', 'View Data'),
                                'value'=>$model->id,
                                'data' => '{"id":"'. $model->id .'",'.
                                        '"customer_name":"'. $model->customer_name .'",'.
                                        '"nama_propinsi":"'. $model->propinsi->nama_propinsi .'",'.
                                        '"nama_kota":"'. $model->kota->nama_kota .'",'.
                                        '"email":"'. $model->email .'",'.
                                        '"nomor_hp":"'. $model->nomor_hp .'",'.
                                        '"nomor_telp":"'. $model->nomor_telp .'",'.
                                        '"address":"'. $model->address .'",'.
                                        '"category_id":"'. $model->category_id .'",'.
                                        '"sales_pic":"'. $model->sales_pic.'",'.
                                        '"propinsi_id":"'. $model->propinsi_id .'",'.
                                        '"kota_id":"'. $model->kota_id .'"}',
                    ]);
                },
            ],
        ],
    ],
]);
?>
<?php Pjax::end(); ?>