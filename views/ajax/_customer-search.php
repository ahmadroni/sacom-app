<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MstPropinsi;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\search\MstTersangkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
        'id' =>'form-customer-search',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'offset' => 'col-sm-offset-0',
                'wrapper' => 'col-sm-12 col-md-12',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="row">
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'customer_name')->textInput(['placeholder'=>'Customer Name'])->label(false) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'propinsi_id')->dropDownList(ArrayHelper::map(MstPropinsi::find()->all(), 'id', 'nama_propinsi'), ['prompt' => '= Pilih Propinsi =', 'class' => 'form-control propinsi'])->label(false) ?>   
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'kota_id')->dropDownList(((int)$model->kota_id == 0 ? ArrayHelper::map(['empty'=>'empty'], 'id', 'value') : ArrayHelper::map(MstKota::listData($model->propinsi_id), 'id', 'nama_kota')),['prompt'=>'= Pilih Kota =','class'=>'form-control kota'])->label(false) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(MstLookup::listData('CustomerCategory'), 'code', 'name'), ['prompt' => '= Jenis Customer ='])->label(false) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
