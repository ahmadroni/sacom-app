<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MstLookup */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'id'=>'form-lookup',
    'enableClientValidation' => false,
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 id="modal-title" class="modal-title"><?= $title; ?> </h4>
</div>
<div class="modal-body">
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
    <div style="display:none;">
         <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'position')->textInput() ?>
    </div>
</div>
<div class="modal-footer">
    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
</div>

<?php ActiveForm::end(); ?>
