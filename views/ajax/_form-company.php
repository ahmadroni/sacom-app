<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstPropinsi;
use app\models\MstKota;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\MstCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-company-form">

    <?php
    $form = ActiveForm::begin([
        'id' =>'form-company',
        'enableClientValidation' => false,
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
            
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'company_type')->dropDownList(ArrayHelper::map(MstLookup::listData("CompanyType"), 'code', 'name'), ['prompt' => '= Pilih Company Type =', 'class' => 'form-control']) ?> 
            
            
        </div>
    </div>
    <?= $form->field($model, 'company_desc')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'propinsi_id')->dropDownList(ArrayHelper::map(MstPropinsi::listData(), 'id', 'nama_propinsi'), ['prompt' => '= Pilih Propinsi =', 'class' => 'form-control propinsi select2']) ?> 
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'kota_id')->dropDownList(((int)$model->kota_id == 0 ? ArrayHelper::map(['empty'=>'empty'], 'id', 'value') : ArrayHelper::map(MstKota::listData($model->propinsi_id), 'id', 'nama_kota')),['prompt'=>'= Pilih Kota =','class'=>'form-control kota select2']) ?>    
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"> 
            <?=
                $form->field($model, 'is_active')->checkbox([
                    'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}'
                ])
                ?> 
        </div>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
