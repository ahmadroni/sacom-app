<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;
?>
<?= $this->render('_search', ['model' => $searchModel]);      ?>
<?php Pjax::begin(['id' => $currentController . '-pjax']); ?>

<?=

GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'companyType.name',
        'company_name',
        'company_desc',
        [
            'attribute' => 'propinsi_id',
            'value' => 'propinsi.nama_propinsi',
        ],
        [
            'attribute' => 'kota_id',
            'value' => 'kota.nama_kota',
        ],
        'address',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{ok}',
            'options' => ['style' => 'width:50px;'],
            'buttons' => [
                'ok' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-check"></span>', [
                                'class' => 'btn btn-xs btn-success btn-ok-company',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $key,
                                'data' => '{"id":"' . $model->id . '","company_name":"' . $model->company_name . '"}',
                    ]);
                },
            ],
        ],
    ],
]);
?>
<?php Pjax::end(); ?>