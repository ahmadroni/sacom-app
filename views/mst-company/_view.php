<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MstCompany */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mst Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mst-company-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'company_name',
            'company_desc',
            [
                'attribute'=>'address',
                'value'=>$model->address .', Kab.'. $model->kota->nama_kota .', Prop.'. $model->propinsi->nama_propinsi,
            ],
            'email:email',
            'no_telp',
            [
                'attribute'=>'is_active',
                'value'=> app\models\MstLookup::itemData('IsActive', $model->is_active)
            ],
        ],
    ]) ?>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
