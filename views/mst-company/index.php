<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\MstLookup;
use app\models\MstPropinsi;
use app\models\MstKota;
use yii\helpers\Url;
use yii\grid\GridView;

$currentController = Yii::$app->controller->id;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MstCompanySeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Master Company');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info mst-company-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>        
        <div class="box-tools">
            <button type="button" class="btn btn-info btn-sm btn-add"><i class="fa fa-plus"></i> Add</button>            
        </div>
    </div>
    <div class="box-body">
        <?php Pjax::begin(['id' => $currentController . '-pjax']); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'company_type',
                    'value' => 'companyType.name',
                    'filter' => Html::activeDropDownList($searchModel, 'company_type', ArrayHelper::map(MstLookup::listData('CompanyType'), 'code', 'name'), ['class' => 'form-control','prompt' => '= Pilih Jenis Perusahaan =']),
                    'options' => ['class' =>'col-sm-2 col-md-2 col-lg-2'],
                ],
                'company_name',
                'company_desc',
                [
                    'attribute' => 'propinsi_id',
                    'value' => 'propinsi.nama_propinsi',
                    'filter' => Html::activeDropDownList($searchModel, 'propinsi_id', ArrayHelper::map(MstPropinsi::listData(), 'id', 'nama_propinsi'), ['class' => 'form-control propinsi', 'prompt' => '= Pilih Propinsi =']),
                    'options' => ['class' =>'col-sm-2 col-md-2 col-lg-2'],
                ],
                [
                    'attribute' => 'kota_id',
                    'value' => 'kota.nama_kota',
                    'filter' => Html::activeDropDownList($searchModel, 'kota_id', ArrayHelper::map(((int) $searchModel->propinsi_id == 0 ? MstKota::listDataAll() : MstKota::listData($searchModel->propinsi_id)), 'id', 'nama_kota'), ['class' => 'form-control kota', 'prompt' => '= Pilih Kota =']),
                    'options' => ['class' =>'col-sm-2 col-md-2 col-lg-2'],
                ],
                [
                    'attribute' => 'is_active',
                    'format' => 'raw',
                    'options' => ['class' =>'col-sm-1 col-md-1 col-lg-1'],
                    'value' => function($model){ 
                        return Html::button($model->is_active == 0 ? '<span class="fa fa-close"></span>' : '<span class="fa fa-check"></span>', [
                            'class' => 'btn btn-xs '. ($model->is_active == 0 ? 'btn-danger' : 'btn-success'),
                        ]);    
                    },
                    'filter'=>Html::activeDropDownList($searchModel, 'is_active', ArrayHelper::map(MstLookup::listData('IsActive'), 'code', 'name'),['prompt'=>'= Pilih Data =','class'=>'form-control'])
                ],
                //'alamat',
                //'email:email',
                //'no_telp',
                //'is_active',
                //'created_at',
                //'created_by',
                //'updated_at',
                //'updated_by',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'options' => ['style' => 'width:100px;'],
                    'buttons' => [
                        'view' => function($url, $model, $key) {
                            return Html::button('<span class="fa fa-eye"></span>', [
                                        'class' => 'btn btn-xs btn-success btn-view',
                                        'title' => Yii::t('yii', 'View Data'),
                                        'value' => $key,
                            ]);
                        },
                        'update' => function($url, $model, $key) {
                            return Html::button('<span class="fa fa-pencil"></span>', [
                                        'class' => 'btn btn-xs btn-warning btn-update',
                                        'title' => Yii::t('yii', 'Update Data'),
                                        'value' => $key,
                            ]);
                        },
                        'delete' => function($url, $model, $key) {
                            return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-danger',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later  ?>
<script>
    // add button
    $('.btn-add').on('click', function () {
        $.ajax({
            url: '<?= Url::to([$currentController . '/create']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New <?= Html::encode($this->title) ?>');                
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // update
    $(document).on('click', '.btn-update', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/update']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Update <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // view
    $(document).on('click', '.btn-view', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/view']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('View <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    $('#modal-input').on('submit', 'form', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (data) {
                if (data.result == 'success') {
                    $.pjax.reload({container: '#<?= $currentController ?>-pjax'});
                    $('#modal-input').modal('hide');
                    $(this).trigger('reset');
                } else {
                    $('#modal-data').html(data);
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
                $("#modal-input").find(".btn-save").prop('disabled',false);
            },
            beforeSend:function(){
                $("#modal-input").find(".btn-save").prop('disabled',true);
                $('.overlay').show();
            },
        });
        return false;
    });
    
    $('#modal-input').on('shown.bs.modal', function(){
        // set tanggal
        
    });
    
    // change propinsi
    $(document).on("change",".propinsi", function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(["ajax/get-kota"]); ?>',
            type: 'get',
            data: {'id': id},
            dataType: 'json',
            success: function (result) {
                $(document).find(".kota").empty();
                $(document).find(".kota").append('<option value="">= Pilih Kota =</option>');
                
                $.each(result.data, function (index, item) {
                    $(document).find(".kota").append('<option value="' + item.id + '">' + item.nama_kota + '</option>');
                });
            },
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 