<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstPropinsi;
use app\models\MstKota;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\MstCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-company-form">

    <?php
    $form = ActiveForm::begin([
                'id' =>'form-company',
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4 col-md-4 col-lg-4',
                        'offset' => '',
                        'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'company_type')->dropDownList(ArrayHelper::map(MstLookup::listData("CompanyType"), 'code', 'name'), ['prompt' => '= Pilih Company Type =', 'class' => 'form-control']) ?> 
            
            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'company_desc')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'propinsi_id')->dropDownList(ArrayHelper::map(MstPropinsi::listData(), 'id', 'nama_propinsi'), ['prompt' => '= Pilih Propinsi =', 'class' => 'form-control propinsi']) ?> 

            <?= $form->field($model, 'kota_id')->dropDownList(((int)$model->kota_id == 0 ? ArrayHelper::map(['empty'=>'empty'], 'id', 'value') : ArrayHelper::map(MstKota::listData($model->propinsi_id), 'id', 'nama_kota')),['prompt'=>'= Pilih Kota =','class'=>'form-control kota']) ?>    
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?=
                $form->field($model, 'is_active')->checkbox([
                    'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}'
                ])
                ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
