<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MstLocation;

/* @var $this yii\web\View */
/* @var $model app\models\MstLocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-location-form">

    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3',
                        'offset' => '',
                        'wrapper' => 'col-sm-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>

    <?= $form->field($model, 'location_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inovation_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'safety_email')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(MstLocation::find()->orderBy(['location_name'=> SORT_ASC])->asArray()->all(), 'id', 'location_name'), ['prompt' => '= Pilih Location =']) ?>

    <div class="form-group">
        <?=
        $form->field($model, 'enable_inovation', [
            'options' => ['class' => 'col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-sm-8 col-8 col-lg-8']
        ])->checkbox([
            'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}'
        ])
        ?>
    </div>

    <div class="form-group">
        <?=
        $form->field($model, 'is_active', [
            'options' => ['class' => 'col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-sm-8 col-8 col-lg-8']
        ])->checkbox([
            'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}'
        ])
        ?>
    </div>


    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>