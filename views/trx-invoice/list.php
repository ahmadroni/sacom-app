<?php

use yii\helpers\Html;
use yii\helpers\Url;
use mdm\admin\components\Helper;

$currentController = Yii::$app->controller->id;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoice');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-invoice-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>        
        <div class="box-tools">
            <?php if(Helper::checkRoute('create')){ echo Html::a('<i class="fa fa-plus"></i>  Add', ['create'], ['class' => 'btn btn-info btn-sm']); } ?>            
        </div>
    </div>
    <div class="box-body">
        <?= $this->render('_search', ['model' => $model]); ?>
        <form id="submit-form">
        <?= $this->render('_list', ['dataProvider' => $dataProvider]); ?>
        <div class="box-footer">
            <button class="btn btn-primary" id="btn-submit-all">Proses All</button>
        </div>
        </form>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<div id="modal-input2" class="modal modal-solid modal-danger fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later  ?>
<script>
    $('.select2').select2();

    $(".tanggal").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        setDate: new Date(),
    });

    // submit all
    $("#btn-submit-all").click(function(){
        var dataForm = $('#submit-form').serialize();
        $.ajax({
            url: '<?= Url::to([$currentController . '/submit-all']) ?>',
            type: 'post',
            dataType: 'json',
            data:dataForm,
            success: function (data) {
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
        //return false;
    });
    // buat nomor urut
    function nomorUrut() {
        $.each($('#modal-input').find('#list-detail >tr'), function (index, item) {
            $(this).find('.nomor').text(index);
        });
    }
    // add button
    $('.btn-add').on('click', function () {
        $.ajax({
            url: '<?= Url::to([$currentController . '/create']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('#modal-input').find('#btn-add-item').click();
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });

    // reject modal
    $('#modal-input').on('click','.btn-reject', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/problem']) ?>',
            type: 'get',
            data:{'id':id},
            dataType: 'html',
            success: function (data) {
                //$('#modal-data').html(data);
                //$('#modal-title').html('New <?= Html::encode($this->title) ?>');
                $('#modal-input2').find('.modal-content').html(data);
                $('#modal-input2').modal('show');
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });

    $('#modal-input2').on('shown.bs.modal',function(){
        //$('.select2').select2();
    });

    // update
    $(document).on('click', '.btn-update', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/update']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Update <?= Html::encode($this->title) ?>');
                // membuat nomor
                nomorUrut();
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });

    // view
    $(document).on('click', '.btn-view', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/view']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('View <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });

    $(document).on('click', '.btn-proses', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to([$currentController . '/proses']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Proses <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });

    $('#modal-input').on('submit', 'form', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (data) {
                if (data.result == 'success') {
                    $.pjax.reload({container: '#<?= $currentController ?>-pjax'});
                    $('#modal-input').modal('hide');
                    $(this).trigger('reset');
                } else {
                    $('#modal-data').html(data);
                    // membuat nomor
                    nomorUrut();
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
        return false;
    });

    // add tembusan button
    var detail_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
    $('#modal-input').on('click', '#btn-add-item', function () {
        var index = $("#list-item >tr").length;
        index -= 1;
        detail_k += 1;
        $('#modal-input').find('#list-detail').append('<tr>' + $('#new-detail-block').html().replace(/__id__/g, 'new' + detail_k) + '</tr>');
        //$('#modal-input').find('#list-detail').find("input[type='hidden'][name*='new" + detail_k + "']").val(index);
        nomorUrut();
    });

    // remove tembusan button
    $('#modal-input').on('click', '.btn-remove-detail', function () {
        $(this).closest('tbody tr').remove();
        nomorUrut();
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 