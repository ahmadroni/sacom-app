<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\search\MstTersangkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
        'id' =>'form-search',
    ]); ?>
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'created_at',[
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
            ])->textInput(['placeholder'=>'dd-mm-yy','class'=>'form-control tanggal created_at']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'supplier_id')->dropDownList(
                ArrayHelper::map(app\models\MstSupplier::listData(), 'id', 'supplier_name'), 
                ['class'=>'form-control select2', 'prompt' => '= Pilih Supplier =']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'recieve_from')->dropDownList(
                ArrayHelper::map(app\models\MstLookup::listData('InvoiceReciept'), 'code', 'name'), 
                ['class'=>'form-control select2', 'prompt' => '= Select Kurir =']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'status')->dropDownList(
                ArrayHelper::map(app\models\MstLookup::listData('InvoiceStatus'), 'code', 'name'), 
                ['class'=>'form-control select2', 'prompt' => '= Select Status =']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <br/>
            <?= Html::submitButton('<i class="fa fa-search"></i> Search', ['name'=>'submit','value'=>'search', 'class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
