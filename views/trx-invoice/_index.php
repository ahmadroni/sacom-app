<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;

Pjax::begin(['id' => $currentController . '-pjax']); 
?>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'created_at',
            'value'=>'created_at',
            'format'=>['date','php: d-m-Y'],
            'options' => ['class' => 'col-sm-1 col-md-1 col-lg-1 text-center'],
        ],
        'invoice_desc',
        [
            'attribute' => 'supplier_id',
            'value' => 'supplier.supplier_name',
            'filter' => Html::activeDropDownList($searchModel, 'supplier_id', ArrayHelper::map(app\models\MstSupplier::listData(), 'id', 'supplier_name'), ['class' => 'form-control', 'prompt' => '= Pilih Supplier =']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'recieve_from',
            'value' => 'recieveFrom.name',
            'filter' => Html::activeDropDownList($searchModel, 'recieve_from', ArrayHelper::map(app\models\MstLookup::listData('InvoiceReciept'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Recieve From =']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2 text-center'],
        ],
        [
            'header' => 'Jumlah Invoice',
            'value' => 'detailCount',
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2 text-center'],
        ],
        [
            'attribute' => 'status',
            'value' => 'invoiceStatus.name',
            'filter' => Html::activeDropDownList($searchModel, 'status', ArrayHelper::map(app\models\MstLookup::listData('InvoiceStatus'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Recieve From =']),
            'options' => ['class' => 'col-sm-1 col-md-1 col-lg-1 text-center'],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'options' => ['style' => 'width:50px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $model['id'],
                    ]);
                },
            ],
        ],
    ],
]);
?>
<?php Pjax::end(); ?>