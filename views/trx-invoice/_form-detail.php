<?php
use app\models\MstLookup;
use yii\helpers\ArrayHelper;
?>
<td class="nomor">
</td>
<td>
    <?=
    $form->field($detail, 'invoice_no', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->textInput([
        'id' => "TrxInvoiceDetails_{$key}_invoice_no",
        'name' => "TrxInvoiceDetails[$key][invoice_no]",
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'currency', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->dropDownList(ArrayHelper::map(MstLookup::listData('Currency'), 'code', 'name'), [
        'prompt' => '= Select Status =',
        'id' => "TrxInvoiceDetails_{$key}_currency",
        'name' => "TrxInvoiceDetails[$key][currency]",
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'amount', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->textInput([
        'id' => "TrxInvoiceDetails_{$key}_amount",
        'name' => "TrxInvoiceDetails[$key][amount]",
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'tax_no', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->textInput([
        'id' => "TrxInvoiceDetails_{$key}_tax_no",
        'name' => "TrxInvoiceDetails[$key][tax_no]",
    ])->label(false)
    ?>
</td>
<td>
    <button type="button" class="btn btn-danger btn-remove-detail btn-xs"><i class="fa fa-trash"></i></button>
</td>