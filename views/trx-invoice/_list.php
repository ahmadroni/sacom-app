<?php

use yii\helpers\Html;
use mdm\admin\components\Helper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$currentController = Yii::$app->controller->id;

Pjax::begin(['id' => $currentController . '-pjax']); 
?>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'header' => 'Tanggal',
            'attribute'=>'created_at',
            'value'=>'created_at',
            'format'=>['date','php: d-m-Y'],
            'options' => ['class' => 'col-sm-1 col-md-1 col-lg-1 text-center'],
        ],
        'invoice_desc',
        [
            'attribute' => 'supplier_name',
            'value' => 'supplier_name',
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'recieve_from',
            'value' => 'recieve_from',
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2 text-center'],
        ],
        [
            'header' => 'Jumlah Invoice',
            'value' => 'detail_count',
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2 text-center'],
        ],
        [
            'attribute' => 'status',
            'value' => 'status',
            'options' => ['class' => 'col-sm-1 col-md-1 col-lg-1 text-center'],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => Helper::filterActionColumn('{view} {proses} {update} {delete}'),
            'options' => ['style' => 'width:95px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $model['id'],
                    ]);
                },
                'proses' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-cog"></span>', [
                                'class' => 'btn btn-xs btn-info btn-proses',
                                'title' => Yii::t('yii', 'Proses Data'),
                                'value' => $model['id'],
                    ]);
                },
                'update' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $model['id']], [
                                'class' => 'btn btn-xs btn-warning',
                                'title' => Yii::t('yii', 'Update Data'),
                                'value' => $model['id'],
                    ]);
                },
                'delete' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model['id']], [
                                'class' => 'btn btn-xs btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                    ]);
                }
            ],
        ],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions'=> function($model){
                return ['value'=>$model['id']];
            }
        ],
    ],
]);
?>
<?php Pjax::end(); ?>