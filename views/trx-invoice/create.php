<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrxForm */

$this->title = Yii::t('app', 'New Invoice');
$this->params['breadcrumbs'][] = ['label' => 'Invoice', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info trx-callcenter-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?> Form</h3>  
    </div>
    <div class="box-body">
        <?= $this->render('_form', ['model' => $model]) ?>
    </div>
</div>
