<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrxInvoice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trx Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-invoice-view">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'invoice_desc',
                    [
                        'attribute'=>'supplier_id',
                        'value' => $model->supplier->supplier_name,
                    ],
                    [
                        'attribute'=>'recieve_from',
                        'value' => $model->recieveFrom->name,
                    ],
                ],
            ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">            
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'invoice_desc',
                    [
                        'attribute'=>'status',
                        'value' => $model->status,
                    ],
                    [
                        'attribute'=>'category',
                        'value' => $model->invoiceCategory->name,
                    ],
                ],
            ])
            ?>
        </div>
    </div>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">List Invoice</h3>
        </div>
        <div class="box-body no-padding">
            <table class="table table">
                <?php
                $detail = new app\models\TrxInvoiceDetail();
                $detail->loadDefaultValues();
                ?>
                <thead>
                    <tr>
                        <th class="col-sm-1 col-md-1 col-lg-1">No</th>
                        <th><?= $detail->getAttributeLabel('invoice_no') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('currency') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('amount') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('tax_no') ?></th>
                    </tr>
                </thead>
                <tbody id="list-detail">
                    <?php foreach ($model->trxInvoiceDetails as $key => $item) : ?>
                        <tr>
                            <td><?= ($key + 1) ?></td>
                            <td><?= $item->invoice_no ?></td>
                            <td><?= $item->currency ?></td>
                            <td class="pull-right"><?= number_format($item->amount,2, ',', '.') ?></td>
                            <td><?= $item->tax_no ?></td>
                        </tr>    
                    <?php endforeach; ?>
                </tbody>
            </table>

            <h4 class="box-title">Invoice Log:</h4>
            <table class="table table-responsive">
                <?php foreach($model->invoiceHistory as $key=>$item): ?>
                <tr>
                    <td width="20%"><?= date('d-m-Y h:i:s', strtotime($item->created_at)); ?></td>
                    <td width="20%"><?= empty($item->user->full_name)?"_": $item->user->full_name; ?></td>
                    <td><?= $item->status; ?></td>
                </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>

    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
