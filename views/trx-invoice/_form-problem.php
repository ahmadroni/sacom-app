<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TrxInvoiceProblem;

/* @var $this yii\web\View */
/* @var $model app\models\MstLookup */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin([
    'id'=>'form-problem',
    'enableClientValidation' => false,
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 id="modal-title" class="modal-title">Invoice Problem </h4>
</div>
<div class="modal-body">
    <?= $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            
        </div>

        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            
        </div>
    </div>
    <?= $form->field($model, 'category')->dropDownList(
                ArrayHelper::map(TrxInvoiceProblem::listCategory(), 'id', 'name'), 
                ['prompt' => '= Select Reciever =','class'=>'form-control select2']) ?>
    <?= $form->field($model, 'notes')->textArea(['rows' => 4,'maxlength'=>true]);?>
    <div style="display:none;">
         <?= $form->field($model, 'invoice_id')->textInput() ?>
         <?= $form->field($model, 'user_id')->textInput() ?>
    </div>
</div>
<div class="modal-footer">
    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
</div>

<?php ActiveForm::end(); ?>
