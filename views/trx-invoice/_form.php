<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MstSupplier;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\TrxInvoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-invoice-form">

    <?php
    $form = ActiveForm::begin([
                //'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-2 col-md-2 col-lg-2',
                        'offset' => '',
                        'wrapper' => 'col-sm-10 col-md-10 col-lg-10',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    if($model->hasErrors()) :
    ?>
    <div class="callout callout-warning">
        <?= $model->errorSummary($form) ?>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxInvoice, 'supplier_id')->dropDownList(
                ArrayHelper::map(MstSupplier::listData(), 'id', 'supplier_name'), 
                ['prompt' => '= Select Supplier =','class'=>'form-control select2']) ?>
        </div>
        
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">   
            <?= $form->field($model->trxInvoice, 'recieve_from')->dropDownList(
                ArrayHelper::map(MstLookup::listData('InvoiceReciept'), 'code', 'name'), 
                ['prompt' => '= Select Reciever =','class'=>'form-control select2']) ?>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">  
            <?= $form->field($model->trxInvoice, 'category')->dropDownList(
                ArrayHelper::map(MstLookup::listData('InvoiceCategory'), 'code', 'name'), 
                ['prompt' => '= Select Category =','class'=>'form-control select2']) ?>
        </div>
    </div>
    <?= $form->field($model->trxInvoice, 'invoice_desc')->textInput(['maxlength' => true]) ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Please click button + to add item</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info btn-sm" id="btn-add-item"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table">
                <?php
                $detail = new app\models\TrxInvoiceDetail();
                $detail->loadDefaultValues();
                ?>
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th><?= $detail->getAttributeLabel('invoice_no') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('currency') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('amount') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('tax_no') ?></th>
                        <th style="width: 20px;">#</th>
                    </tr>
                </thead>
                <tbody id="list-detail">
                    <?php
                    echo '<tr id="new-detail-block" style="display: none;">';
                    echo $this->render('_form-detail', [
                        'key' => '__id__',
                        'form' => $form,
                        'detail' => $detail,
                    ]);
                    echo '</tr>';
                    foreach ($model->trxInvoiceDetails as $key => $_detail) {
                        echo '<tr>';
                        echo $this->render('_form-detail', [
                            'key' => $_detail->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_detail->id,
                            'form' => $form,
                            'detail' => $_detail,
                        ]);
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php ob_start(); // output buffer the javascript to register later  ?>
<script>
    $('.select2').select2();
    
    // buat nomor urut
    function nomorUrut(){
        $.each($('#list-detail >tr'), function(index, item){
            $(this).find('.nomor').text(index);
        }); 
    }    
    // add tembusan button
    var detail_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
    $(document).on('click','#btn-add-item', function () {
        var index = $("#list-item >tr").length;
        index -= 1;
        detail_k += 1;
        $('#list-detail').append('<tr>' + $('#new-detail-block').html().replace(/__id__/g, 'new' + detail_k) + '</tr>');
        //$('#modal-input').find('#list-detail').find("input[type='hidden'][name*='new" + detail_k + "']").val(index);
        nomorUrut();
    });

    // remove tembusan button
    $(document).on('click', '.btn-remove-detail', function () {
        $(this).closest('tbody tr').remove();        
        nomorUrut();
    });
    
    $(document).ready(function(){
        nomorUrut();
    });
<?php
  // OPTIONAL: click add when the form first loads to display the first new row
if (!Yii::$app->request->isPost && $model->trxInvoice->isNewRecord){
    echo "$('#btn-add-item').click();";
}
?>
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 
