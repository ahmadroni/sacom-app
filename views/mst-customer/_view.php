<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MstCustomer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mst Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mst-customer-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'customer_name',
            [
                'attribute'=>'address',
                'value'=>$model->address .', Kab.'. $model->kota->nama_kota .', Prop.'. $model->propinsi->nama_propinsi,
            ],
            'email:email',
            'nomor_hp',
            'nomor_telp',
            [
                'attribute'=>'sales_pic',
                'value'=>$model->salesPic->username,
            ],
            [
                'attribute'=>'category_id',
                'value'=> app\models\MstLookup::itemData('CustomerCategory', $model->category_id)
            ],
            [
                'attribute'=>'is_active',
                'value'=> app\models\MstLookup::itemData('IsActive', $model->is_active)
            ],
        ],
    ]) ?>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
