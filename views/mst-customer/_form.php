<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstPropinsi;
use app\models\MstKota;
use app\models\MstUser;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\MstCustomer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mst-customer-form">

    <?php
    $form = ActiveForm::begin([
                'id' => 'form-customer',
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4 col-md-4 col-lg-4',
                        'offset' => '',
                        'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>
    <?php if ($model->hasErrors()) : ?>
        <div class="callout callout-warning">
            <?= $form->errorSummary($model); ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs 12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address')->textarea(['maxlength' => true, 'rows' => 6]) ?>

            <?= $form->field($model, 'propinsi_id')->dropDownList(ArrayHelper::map(MstPropinsi::find()->all(), 'id', 'nama_propinsi'), ['prompt' => '= Pilih Propinsi =', 'class' => 'form-control propinsi select2']) ?>   

            <?= $form->field($model, 'kota_id')->dropDownList(((int) $model->kota_id == 0 ? ArrayHelper::map(['empty' => 'empty'], 'id', 'value') : ArrayHelper::map(MstKota::listData($model->propinsi_id), 'id', 'nama_kota')), ['prompt' => '= Pilih Kota =', 'class' => 'form-control kota select2']) ?>       
        </div>

        <div class="col-xs 12 col-sm-6 col-md-6 col-lg-6"> 
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'nomor_hp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'nomor_telp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'sales_pic')->dropDownList(ArrayHelper::map(MstUser::listDataDefault(Yii::$app->user->identity->id), 'id', 'username'), ['prompt' => '= Sales PIC =','class' => 'form-control select2']) ?>

            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(MstLookup::listData('CustomerCategory'), 'code', 'name'), ['prompt' => '= Jenis Customer =','class' => 'form-control select2']) ?>

            <div class="form-group">
                <?=
                $form->field($model, 'is_active', [
                    'options' => ['class' => 'col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-8 col-lg-8']
                ])->checkbox([
                    'template' => '{beginLabel}{input} {labelTitle}{endLabel}{error}{hint}'
                ])
                ?>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
