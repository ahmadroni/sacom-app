<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstLookup;
use app\models\MstUser;
use app\models\MstCustomer;
use app\models\MstCompany;

$currentController = Yii::$app->controller->id;
/* @var $this yii\web\View */
/* @var $model app\models\TrxCallcenter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-callcenter-form">

    <?php
    $form = ActiveForm::begin([
                //'layout' => 'horizontal',
                'enableClientValidation' => false,
                
    ]);
    ?>
    <?php if ($model->hasErrors()) : ?>
        <div class="callout callout-warning">
            <?= $model->errorSummary($form) ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?=
            $form->field($model->mstCustomer, 'id', [
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-btn">' .
                //'<button type="button" class="btn btn-default" id="btn-search-customer"><i class="fa fa-search"></i></button>' .
                '<button type="button" class="btn btn-success" id="btn-clone-customer"><i class="fa fa-clone"></i></button>' .
                '<button type="button" class="btn btn-info" id="btn-add-customer"><i class="fa fa-plus"></i></button>' .
                '</div></div>',
            ])->dropDownList(ArrayHelper::map(MstCustomer::listData2(), 'id', 'name'), 
                    ['prompt' => '=Pilih Customer=','class' => 'form-control select2'])
            ->label('Customer');
            ?>

            <?= $form->field($model->mstCustomer, 'nomor_hp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model->mstCustomer, 'nomor_telp')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model->mstCustomer, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            
            <?= $form->field($model->mstCustomer, 'address')->textInput(['maxlength' => true]) ?>
            
            <?= $form->field($model->mstCustomer, 'category_id')->textInput(['maxlength' => true]) ?>           

            <?= $form->field($model->trxCallCenter, 'sales_pic')->dropDownList(
                    ArrayHelper::map(MstUser::listDataDefault(Yii::$app->user->identity->id), 'id', 'username'),
                    ['class' => 'form-control select2']) ?>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model->trxCallCenter, 'company_id', [
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-btn">' .
                //'<button type="button" class="btn btn-default" id="btn-search-company"><i class="fa fa-search"></i></button>' .
                '<button type="button" class="btn btn-info" id="btn-add-company"><i class="fa fa-plus"></i></button>' .
                '</div></div>',
            ])->dropDownList(ArrayHelper::map(MstCompany::listDataByType(MstCompany::CALL_CENTER_TYPE), 'id', 'name'), 
                    ['prompt' => '=Pilih Perusahaan=','class' => 'form-control select2'])
            ->label('Perusahaan')
            ?>
            
            <div class="form-group">
                <label class="control-label">No Telp</label>
                <input type="text" id="txt-telp-perusahaan" class="form-control" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
                <label class="control-label">Alamat</label>
                <input type="text" id="txt-alamat-perusahaan" class="form-control" />
            </div>
            
            <div class="form-group">
                <label class="control-label">Email</label>
                <input type="text" id="txt-email-perusahaan" class="form-control" />
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxCallCenter, 'jenis_panggilan',[
                    'inputTemplate' => '<div class="input-group">{input}<div class="input-group-btn">' .
                    '<button type="button" class="btn btn-info" id="btn-add-call"><i class="fa fa-plus"></i></button>' .
                    '</div></div>'
                ])->dropDownList( 
                    ArrayHelper::map(MstLookup::listData("CCCallType"), 'code', 'name'), 
                    ['prompt' => '=Jenis Panggilan=','class' => 'form-control select2']); ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxCallCenter, 'jenis_media',[
                    'inputTemplate' => '<div class="input-group">{input}<div class="input-group-btn">' .
                    '<button type="button" class="btn btn-info" id="btn-add-media"><i class="fa fa-plus"></i></button>' .
                    '</div></div>'
                ])->dropDownList(
                    ArrayHelper::map(MstLookup::listData("CCMediaType"), 'code', 'name'), 
            [       'prompt' => '=Jenis Media=','class' => 'form-control select2']); ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxCallCenter, 'created_at',[
                'inputTemplate' => '<div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div> {input}</div>',
            ])->textInput(['maxlength' => true,'placeholder'=>'Tanggal','class'=>'form-control tanggal']) ?>
        </div>
    </div>

    <?= $form->field($model->trxCallCenter, 'isi_pertanyaan')->textarea(['maxlength' => true, 'rows' => 6]) ?>

    <?= $form->field($model->trxCallCenter, 'isi_jawaban')->textarea(['maxlength' => true, 'rows' => 6]) ?>
    
    
    <div id="hidden-input" style="display: none">
        <?= $form->field($model->trxCallCenter, 'customer_id')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div id="modal-input" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>

<div id="modal-input2" class="modal modal-solid modal-info fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            
        </div>
    </div>
</div>
<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    var jenisCustomer;
    var alamat;
    var propinsiId;
    var kotaId;
    var email;
    var noTelp;

     $(document).ready(function () {
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            setDate: new Date(),
        });

        <?php
            if (!Yii::$app->request->isPost && !$model->trxCallCenter->isNewRecord){
                echo "$('#mstcustomer-id').trigger('change');";
                echo "$('#trxcallcenter-company_id').trigger('change');";
            }
        ?>
    });
    $(document).on('change', '.propinsi', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(['ajax/get-kota']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'json',
            success: function (result) {
                $(document).find('.kota').empty();
                $(document).find('.kota').append('<option value="">= Pilih Kota =</option>');
                $.each(result.data, function (index, item) {
                    $(document).find('.kota').append('<option value="' + item.id + '">' + item.nama_kota + '</option>');
                });
            },
        });
    });
    
    $('.select2').select2();
    // set select2 
    $('#modal-input').on('shown.bs.modal', function (e){
         $('.select2').select2();
    });
    
    // lookup
    //add lookup call
    $('#btn-add-call').click(function(){
        $.ajax({
            url: '<?= Url::to(['/ajax/add-lookup']) ?>',
            data: {'type': 'CCCallType'},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $("#modal-input2").find('.modal-content').html(data);
                $("#modal-input2").modal("show");
            },
        });
    });
    // add lookup media
    $('#btn-add-media').click(function(){
        $.ajax({
            url: '<?= Url::to(['/ajax/add-lookup']) ?>',
            data: {'type': 'CCMediaType'},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $("#modal-input2").find('.modal-content').html(data);
                $("#modal-input2").modal("show");
            },
        });
    });
    // save lookup
     $('#modal-input2').on('submit', '#form-lookup', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (result) {
                if (result.status == 'success') {                    
                    $('#modal-input2').modal('hide');
                    
                    if(result.data.type=='CCCallType'){
                        $('#trxcallcenter-jenis_panggilan').select2('val','');
                        $('#trxcallcenter-jenis_panggilan').html('');

                        $.each(result.items, function(key, item) {
                            $('#trxcallcenter-jenis_panggilan').append($('<option>', {
                                value: item.code,
                                text: item.name,
                            }));
                        });
                    }else{
                        $('#trxcallcenter-jenis_media').select2('val','');
                        $('#trxcallcenter-jenis_media').html('');

                        $.each(result.items, function(key, item) {
                            $('#trxcallcenter-jenis_media').append($('<option>', {
                                value: item.code,
                                text: item.name,
                            }));
                        });
                    }
                    console.log(result.items);
                } else {
                    $('#modal-input2').find('.modal-content').html(data);
                    $('#modal-input2').modal('show');
                }
            },
        });
        return false;
    });
    // add customer
    $('#btn-add-customer').click(function () {
        $.ajax({
            url: '<?= Url::to(['/ajax/add-customer']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New Customer');
                $('#modal-input').modal('show');
            },
        });
    });

    $('#modal-input').on('submit', '#form-customer', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (result) {
                if (result.status == 'success') {
                    var data = result.data;
                    $('#mstcustomer-id').select2('val','');
                    $('#mstcustomer-id').html('');

                    $.each(result.items, function(key, item) {
                        $('#mstcustomer-id').append($('<option>', {
                            value: item.id,
                            text: item.name,
                        }));
                    });
                    
                    $('#mstcustomer-id').val(data.id).trigger('change');
                    
                    $(this).trigger('reset');
                    $('#modal-input').modal('hide');
                } else {
                    $('#modal-data').html(data);
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
                $("#modal-input").find(".btn-save").prop('disabled', false);
            },
        });
        return false;
    });
    
    // customer
    $('#btn-clone-customer').on('click',function(){
        var id = $('#mstcustomer-id').val();
        if(id !="" || id != 0){
            if (confirm('Anda ingin mencopy data customer ini?')) {
                $.ajax({
                    url: '<?= Url::to(['ajax/customer-to-company']) ?>',
                    data: {'id': id},
                    type: 'get',
                    dataType: 'json',
                    success: function (result) {
                        $('#trxcallcenter-company_id').select2('val','');
                        $('#trxcallcenter-company_id').html('');
                        $.each(result.items, function(key, item) {
                            $('#trxcallcenter-company_id').append($('<option>', {
                                value: item.id,
                                text: item.name,
                            }));
                        });
                        var data = result.data;
                        $('#trxcallcenter-company_id').val(data.id).trigger('change');
                        console.log(data);
                    },
                });
            }
        }else {
            alert('Pilih Customer Terlbih dahulu');
        }
    });
    
    // change customer
    $('#mstcustomer-id').change(function(){
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(['ajax/get-customer-by-id']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'json',
            success: function (result) {
                var data = result.data;
                $('#mstcustomer-customer_name').val(data.customer_name);
                $('#mstcustomer-nomor_hp').val(data.nomor_hp);
                $('#mstcustomer-nomor_telp').val(data.nomor_telp);
                $('#mstcustomer-address').val(data.address+", "+data.nama_kota +", "+data.nama_propinsi);
                $('#mstcustomer-category_id').val(data.category_id);
                $('#txt-jsn-customer').val(data.jns_customer);
                $('#mstcustomer-email').val(data.email);
                $('#mstcustomer-sales_pic').val(data.sales_pic);

                $('#trxcallcenter-customer_id').val(data.id);
                $('#trxcallcenter-sales_pic').val(data.sales_pic);
                console.log(data);
            },
        });
    });
    
    
    $("#modal-input").on('click','.pagination >li >a', function(){
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
            },
        });
        return false;
    });

    // search customer
    $('#btn-search-customer').click(function () {
        $.ajax({
            url: '<?= Url::to(['mst-customer/search']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Search Customer');
                $('#modal-input').modal('show');
            },
        });
    });

    // 
    $("#modal-input").on("submit", "#form-customer-search", function () {
        $.ajax({
            url: '<?= Url::to(['mst-customer/search']); ?>',
            type: 'post',
            data: $(this).serialize(),
            success: function (result) {
                $("#modal-data").html(result);
            }
        });
        return false;
    });
    
    // update
    $(document).on('click', '.btn-update', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(['mst-customer/update']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Update <?= Html::encode($this->title) ?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend: function () {
                $('.overlay').show();
            },
        });
    });

    // btn-ok-customer click
    $('#modal-input').on('click', '.btn-ok-customer', function () {
        var data = JSON.parse($(this).attr('data'));
        $('#mstcustomer-customer_name').val(data.customer_name);
        $('#mstcustomer-nomor_hp').val(data.nomor_hp);
        $('#mstcustomer-nomor_telp').val(data.nomor_telp);
        $('#mstcustomer-propinsi_id').val(data.propinsi_id);
        $('#mstcustomer-nama_propinsi').val(data.nama_propinsi);
        $('#mstcustomer-address').val(data.address);
        $('#mstcustomer-category_id').val(data.category_id);
        $('#mstcustomer-kota_id').val(data.kota_id);
        $('#mstcustomer-nama_kota').val(data.nama_kota);
        $('#mstcustomer-email').val(data.email);
        $('#mstcustomer-sales_pic').val(data.sales_pic);

        $('#trxcallcenter-customer_id').val(data.id);
        $('#trxcallcenter-sales_pic').val(data.sales_pic);
        $('#modal-input').modal('hide');
    });   
    //company change
    $("#trxcallcenter-company_id").change(function(){
        var id = $(this).val();
        $.ajax({
            url: '<?= Url::to(['ajax/get-company-by-id']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'json',
            success: function (result) {
                var data = result.data;
                $('#txt-telp-perusahaan').val(data.no_telp);
                $('#txt-email-perusahaan').val(data.email);
                $('#txt-alamat-perusahaan').val(data.address+", "+data.nama_kota +", "+data.nama_propinsi);
                console.log(data);
            },
        });
    });
     // search company
    $('#btn-search-company').click(function () {
        $.ajax({
            url: '<?= Url::to(['mst-company/search']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Search Company');
                $('#modal-input').modal('show');
            },
        });
    });

    // btn-ok-company click
    $('#modal-input').on('click', '.btn-ok-company', function () {
        var data = JSON.parse($(this).attr('data'));
        $('#trxcallcenter-company_id').val(data.id);
        $('#trxcallcenter-company_name').val(data.company_name);
        $('#modal-input').modal('hide');
    });
    
    // form tersangka search
    $("#modal-input").on("submit", "#form-company-search", function () {
        $.ajax({
            url: '<?= Url::to(['mst-company/search']); ?>',
            type: 'post',
            data: $(this).serialize(),
            success: function (result) {
                $("#modal-data").html(result);
            }
        });
        return false;
    });
    
    $('#btn-add-company').click(function () {
        $.ajax({
            url: '<?= Url::to(['/ajax/add-company']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New Company');
                $('#modal-input').modal('show');
            },
        });
    });

    $('#modal-input').on('submit', '#form-company', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (result) {
                if (result.status == 'success') {
                    var data = result.data;
                    $('#trxcallcenter-company_id').select2('val','');
                    $('#trxcallcenter-company_id').html('');

                    $.each(result.items, function(key, item) {
                        $('#trxcallcenter-company_id').append($('<option>', {
                            value: item.id,
                            text: item.name,
                        }));
                    });
                    
                    $('#trxcallcenter-company_id').val(data.id).trigger('change');
                    
                    $(this).trigger('reset');
                    $('#modal-input').modal('hide');
                } else {
                    $('#modal-data').html(data);
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
                $("#modal-input").find(".btn-save").prop('disabled', false);
            },
        });
        return false;
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 
