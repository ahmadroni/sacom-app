<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrxCallcenter */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Center'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Detail';
?>
<div class="trx-callcenter-view">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'customer.customer_name',
                    'customer.nomor_hp',
                    'customer.nomor_telp',                    
                    'customer.address',
                    'customer.kota.nama_kota',
                    'customer.propinsi.nama_propinsi',
                    [
                        'label'=>'Jenis Customer',
                        'value'=>($model->customer->category==null ?"-":$model->customer->category->name),
                    ],
                    'customer.email',                    
                    [
                        'label'=>'Sales PIC',
                        'value'=>$model->salesPic->mstUserProfile->full_name,
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                      'attribute'=>'company_id',
                      'value' => $model->company->company_name,  
                    ],
                    'company.address',
                    'company.kota.nama_kota',
                    'company.propinsi.nama_propinsi',
                    [
                      'attribute'=>'jenis_panggilan',
                      'value' => $model->callType->name,  
                    ],
                    [
                      'attribute'=>'jenis_media',
                      'value' => $model->mediaType->name,  
                    ],
                    'isi_pertanyaan',
                    'isi_jawaban',
                    [
                      'attribute'=>'sales_pic',
                      'value' => $model->salesPic->username,  
                    ],
                ],
            ]) ?>
        </div>
    </div>
    <div class="box-footer">   
    	<?= Html::a('Delete', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger pull-right',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		]) ?>
	<?=  Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
