<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MstSupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Call Center');
$this->params['breadcrumbs'][] = 'Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info mst-supplier-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <?= $this->render('_report', ['model' => $model]); ?>
        <?= $this->render('_table', ['dataProvider'=>$dataProvider]); ?>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    $(document).ready(function () {
        $(".tanggal").datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            setDate: new Date(),
        });
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 