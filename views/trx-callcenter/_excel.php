<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=report-excel.xls");
//echo '<pre>'. print_r($dataProvider->models) .'</pre>';
?>
<table border="1">
    <thead>
        <tr>
            <th>No.</th>
             <?php foreach($dataProvider[0] as $key => $value){
                    echo "<th>". $key."</th>";
            } ?>
        </tr>
    </thead>
    <tbody>
    <?php 
    $no=1;
    foreach($dataProvider as $models) {
        echo "<tr>";
        echo "<td>".$no."</td>";
        foreach($models as $key => $value){
           echo "<td>". $value."</td>";
        }
        echo "</tr>";
    $no++; 
    }?>
    </tbody>
</table>