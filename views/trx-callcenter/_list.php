<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;

$currentController = Yii::$app->controller->id;

use yii\widgets\Pjax;
?>
<?php Pjax::begin(['id' => $currentController . '-pjax']); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

<?=

GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'customer_id',
            'value' => 'customer.customer_name',
            'filter' => Html::activeDropDownList($searchModel, 'customer_id', ArrayHelper::map(app\models\MstCustomer::listData(), 'id', 'customer_name'), ['class' => 'form-control', 'prompt' => '= Pilih Customer=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'jenis_panggilan',
            'value' => 'callType.name',
            'filter' => Html::activeDropDownList($searchModel, 'jenis_panggilan', ArrayHelper::map(app\models\MstLookup::listData('CCCallType'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Jenis Panggilan=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'jenis_media',
            'value' => 'mediaType.name',
            'filter' => Html::activeDropDownList($searchModel, 'jenis_media', ArrayHelper::map(app\models\MstLookup::listData('CCMediaType'), 'code', 'name'), ['class' => 'form-control', 'prompt' => '= Pilih Jenis Media=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        'isi_pertanyaan',
        //'isi_jawaban',
        //'sales_pic',
        //'is_active',
        //'created_by',
        //'created_at',
        //'updated_by',
        //'updated_at',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
            'options' => ['style' => 'width:100px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $key,
                    ]);
                },
                'update' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-pencil"></span>', ['update', 'id' => $key], [
                                'class' => 'btn btn-xs btn-warning btn-update',
                                'title' => Yii::t('yii', 'Update Data'),
                                'value' => $key,
                    ]);
                },
                'delete' => function($url, $model, $key) {
                    return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                    ]);
                }
            ],
        ],
    ],
]);
?>
<?php Pjax::end(); ?>