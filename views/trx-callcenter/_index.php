<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;

$currentController = Yii::$app->controller->id;

use yii\widgets\Pjax;
?>
<?php //Pjax::begin(['id' => $currentController . '-pjax']); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute'=>'created_at',
            //'format'=>['date','php: Y-m-d h:i:s'],
            'value'=>function($model){
                return date('Y-m-d', strtotime($model->created_at));
            },
            'options' => ['class' => 'col-sm-1 col-md-1 col-lg-1 text-center'],
            'filter'=>Html::activeInput('text', $searchModel, 'created_at', ['class'=>'form-control tanggal'])
        ],
        [
            'attribute' => 'customer_id',
            'value' => 'customer.customer_name',
            'filter' => Html::activeDropDownList($searchModel, 'customer_id', ArrayHelper::map(app\models\MstCustomer::listData(), 'id', 'customer_name'), ['class' => 'form-control select2', 'prompt' => '= Pilih Customer=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'jenis_panggilan',
            'value' => 'callType.name',
            'filter' => Html::activeDropDownList($searchModel, 'jenis_panggilan', ArrayHelper::map(app\models\MstLookup::listData('CCCallType'), 'code', 'name'), ['class' => 'form-control select2', 'prompt' => '= Pilih Jenis Panggilan=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        [
            'attribute' => 'jenis_media',
            'value' => 'mediaType.name',
            'filter' => Html::activeDropDownList($searchModel, 'jenis_media', ArrayHelper::map(app\models\MstLookup::listData('CCMediaType'), 'code', 'name'), ['class' => 'form-control select2', 'prompt' => '= Pilih Jenis Media=']),
            'options' => ['class' => 'col-sm-2 col-md-2 col-lg-2'],
        ],
        'isi_pertanyaan',
        //'isi_jawaban',
        //'sales_pic',
        //'is_active',
        //'created_by',
        //'created_at',
        //'updated_by',
        //'updated_at',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}',
            'options' => ['style' => 'width:70px;'],
            'buttons' => [
                'view' => function($url, $model, $key) {
                    return Html::button('<span class="fa fa-eye"></span>', [
                                'class' => 'btn btn-xs btn-success btn-view',
                                'title' => Yii::t('yii', 'View Data'),
                                'value' => $key,
                    ]);
                },         
                'update' => function($url, $model, $key) {
                    return Html::a('<i class="fa fa-pencil"></i>', ['update','id'=>$model->id], ['class' => 'btn btn-xs btn-warning']);
                    /*
                    return Html::button('<span class="fa fa-pencil"></span>', [
                                'class' => 'btn btn-xs btn-warning btn-update',
                                'title' => Yii::t('yii', 'Update Data'),
                                'value' => $key,
                    ]);*/
                },
            ],
        ],
    ],
]);
?>
<?php //Pjax::end(); ?>