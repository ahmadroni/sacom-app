<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\MstLocation;
use app\models\MstArea;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\TrxConversation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-conversation-form">

    <?php
    $form = ActiveForm::begin([
                //'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-2 col-md-2 col-lg-2',
                        'offset' => '',
                        'wrapper' => 'col-sm-10 col-md-10 col-lg-10',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    if ($model->hasErrors()) :
        ?>
        <div class="callout callout-warning">
            <?= $model->errorSummary($form) ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model->trxConversation, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model->trxConversation, 'created_at',[
                'inputTemplate' => '<div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div> {input}</div>',
            ])->textInput(['maxlength' => true,'placeholder'=>'Tanggal','class'=>'form-control tanggal']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxConversation, 'location_id')->dropDownList(ArrayHelper::map(MstLocation::listData2(), 'id', 'location_name'), ['prompt' => '= Select Lokasi ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxConversation, 'areas')->dropDownList(ArrayHelper::map(MstArea::listData(), 'id', 'area_name'), ['prompt' => '= Select Area ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxConversation, 'risks')->dropDownList(ArrayHelper::map(MstLookup::listData("SafetyRisk"), 'code', 'name'), ['prompt' => '= Select Area =', 'class' => 'form-control risk'])
            ?> 
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxConversation, 'category')->dropDownList(ArrayHelper::map(MstLookup::listData("SafetyCategory"), 'code', 'name'), ['prompt' => '= Select Kategori ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxConversation, 'types')->dropDownList(ArrayHelper::map(MstLookup::listData("SafetyType"), 'code', 'name'), ['prompt' => '= Select Tipe ='])
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <?=
            $form->field($model->trxConversation, 'posibility')->dropDownList(ArrayHelper::map(MstLookup::listData("SafetyPosibility"), 'code', 'name'), ['prompt' => '= Select Kemungkinan =', 'class' => 'form-control posibility'])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model->trxConversation, 'conversation')->textArea(['maxlength' => true, 'rows' => 5]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-md-4">
            <?=
            $form->field($model->trxConversation, 'risk_level')->textInput(['maxlength' => true, 'class' => 'form-control risk_level'])
            ?>

            <?=
            $form->field($model->trxConversation, 'level')->dropDownList(ArrayHelper::map(MstLookup::listData("SafetyLevel"), 'code', 'name'), ['class' => 'form-control kelas'])
            ?>
        </div>
    </div>
    <div class="row" id="appreciation" style="display: none;">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model->trxConversation, 'appreciation')->textArea(['maxlength' => true, 'rows' => 4]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <?= $form->field($model->trxConversation, 'action_deal')->textArea(['maxlength' => true, 'rows' => 6]) ?> 
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-md-4">
            <?=
            $form->field($model->trxConversation, 'pic_comunicate')->dropDownList(ArrayHelper::map(app\models\MstUser::listData(), 'id', 'username'), ['prompt' => '= Select Salah Satu =', 'class'=>'form-control select2'])
            ?>
        </div>
    </div>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Button + to add more than one person</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-primary btn-sm" id="btn-add-item"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="box-body no-padding">
            <table class="table table">
                <?php
                $detail = new app\models\TrxConversationDetail();
                $detail->loadDefaultValues();
                ?>
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th><?= $detail->getAttributeLabel('conversation_notes') ?></th>
                        <th class="col-sm-3 col-md-3 col-lg-3"><?= $detail->getAttributeLabel('for') ?></th>
                        <th class="col-sm-2 col-md-2 col-lg-2"><?= $detail->getAttributeLabel('status') ?></th>
                        <th style="width: 20px;">#</th>
                    </tr>
                </thead>
                <tbody id="list-detail">
                    <?php
                    echo '<tr id="new-detail-block" style="display: none;">';
                    echo $this->render('_form-detail', [
                        'key' => '__id__',
                        'form' => $form,
                        'detail' => $detail,
                    ]);
                    echo '</tr>';
                    foreach ($model->trxConversationDetails as $key => $_detail) {
                        echo '<tr>';
                        echo $this->render('_form-detail', [
                            'key' => $_detail->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_detail->id,
                            'form' => $form,
                            'detail' => $_detail,
                        ]);
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php ob_start(); // output buffer the javascript to register later   ?>
<script>
    //Initialize Select2 Elements
    $('.select2').select2();
    
    $('.select2-detail').select2();
    
    // buat nomor urut
    function nomorUrut() {
        $.each($('#list-detail >tr'), function (index, item) {
            $(this).find('.nomor').text(index);
        });
    }
    // add tembusan button
    var detail_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
    $(document).on('click', '#btn-add-item', function () {
        var index = $("#list-item >tr").length;
        index -= 1;
        detail_k += 1;
        $('#list-detail').append('<tr>' + $('#new-detail-block').html().replace(/__id__/g, 'new' + detail_k) + '</tr>');
        $('#TrxConversationDetails_new' + detail_k + '_dead_line').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            setDate: new Date(),
        });
        // select2
        $('#TrxConversationDetails_new' + detail_k + '_for_user').select2();
        //$('#modal-input').find('#list-detail').find("input[type='hidden'][name*='new" + detail_k + "']").val(index);
        nomorUrut();
    });

    // remove tembusan button
    $(document).on('click', '.btn-remove-detail', function () {
        $(this).closest('tbody tr').remove();
        nomorUrut();
    });

    $(document).ready(function () {
        nomorUrut();

        $(".tanggal").datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            setDate: new Date(),
        });
    });

    function loadRisk(codeFrom, codeTo) {
        $.ajax({
            url: '<?= Url::to(['ajax/get-lookup-map']) ?>',
            type: 'get',
            data: {'codeFrom': codeFrom, 'codeTo': codeTo},
            dataType: 'json',
            success: function (result) {
                $('.risk_level').val(result.data.code_value);
            },
        });
    }
    $('.posibility').change(function () {
        var codeFrom = $('.risk').val();
        var codeTo = $(this).val();
        if (codeFrom != '' && codeFrom != null && codeTo != '' && codeTo != null) {
            loadRisk(codeFrom, codeTo);
        }
    });

    $('.risk').change(function () {
        var codeFrom = $(this).val();
        var codeTo = $('.posibility').val();
        if (codeFrom != '' && codeFrom != null && codeTo != '' && codeTo != null) {
            loadRisk(codeFrom, codeTo);
        }
    });

    $('.kelas').change(function () {
        var code = $(this).val();
        if (code == 'SafetyLevel01') {
            $('#appreciation').show();
        } else {
            $('#appreciation').hide();
        }
    });

    $('#list-detail').on('change', '.agreement', function () {
        var tanggal = $(this).closest('tbody tr').find('.dead-line');
        if ($(this).is(':checked')) {
            tanggal.show();
        } else {
            tanggal.hide();
        }
    });
<?php
// OPTIONAL: click add when the form first loads to display the first new row
if (!Yii::$app->request->isPost && $model->trxConversation->isNewRecord){
    //echo "$('#btn-add-item').click();";
}else {
    echo "$('.agreement').change();";
}
?>
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 