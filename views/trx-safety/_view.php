<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model app\models\TrxConversation */

$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="trx-conversation-view">
    <!-- Post -->
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?= $imageUrl; ?>" alt="user image">
                <span class="username">
                    <a href="#"><?= $model->postBy->fullname ?></a>
                </span>
                <span class="description">
                    <i>Lokasi :</i>
                    <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                    , <i>Area :</i>
                    <strong><?= $model->area->area_name; ?> </strong>
                    , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
                </span>
            </div>
            <strong> 
                Resiko : <i><?= $model->riskLookup->name ?> </i> | Kemungkinan : <i><?= $model->posibilityLookup->name ?> </i> |
                Kategori : <i><?= $model->categoryLookup->name ?> </i> 
            </strong> <br/>
            <strong>
                Tipe : <i><?= $model->typeLookup->name ?></i> | Kelas : <i><?= $model->levelLookup->name ?> </i> |
                Tingkatan Resiko : <i><?= $model->risk_level ?></i>
            </strong>
            <br/>
            
            <strong><i class="fa fa-comment-o margin-r-5"></i> Pembicaraan : </strong>
            <p> 
                <!-- /.user-block -->
                <?= HtmlPurifier::process($model->conversation) ?> 
            </p>
            <strong><i class="fa fa-comments-o margin-r-5"></i>Tindakan Yang Disepakati :</strong>
            <p>
                <?= HtmlPurifier::process($model->action_deal) ?> 
            </p>
            <strong><i class="fa fa-users margin-r-5"></i> Person Comunicated :</strong>
            <!-- The timeline -->
            <ul class="timeline timeline-inverse">
                <?php foreach ($model->trxConversationDetails as $item): ?>
                <li>
                    <i class="fa fa-user bg-aqua"></i>
                    <div class="timeline-item">
                        <h3 class="timeline-header"><a href="#"><?= $item->forUser->username; ?></a></h3>
                        <div class="timeline-body">
                            <?= $item->notes; ?>
                        </div>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <!-- /.post -->

    <div class="box-footer">   
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

</div>
