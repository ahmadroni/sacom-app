<?php

use app\models\MstLookup;
use app\models\MstUser;
use yii\helpers\ArrayHelper;
?>
<td class="nomor">
</td>
<td>
    <?=
    $form->field($detail, 'notes', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->textArea([
        'id' => "TrxConversationDetails_{$key}_notes",
        'name' => "TrxConversationDetails[$key][notes]",
        'rows' => 4,
    ])->label(false)
    ?>
</td>
<td>
    <?=
    $form->field($detail, 'for_user', [
        'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-12',
        ]
    ])->dropDownList(ArrayHelper::map(MstUser::listConversation(), 'id', 'username'), [
        'prompt' => '= Select Person =',
        'id' => "TrxConversationDetails_{$key}_for_user",
        'name' => "TrxConversationDetails[$key][for_user]",
    ])->label(false)
    ?>

    <?=
    $form->field($detail, 'agreement')->checkbox([
        'id' => "TrxConversationDetails_{$key}_agreement",
        'name' => "TrxConversationDetails[$key][agreement]",
        'class' => 'agreement',
    ])
    ?>
</td>
<td>
    <div class="dead-line" style="display: none;">
        <?=
        $form->field($detail, 'status', [
            'template' => "{beginWrapper}\n{input}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-12',
            ]
        ])->dropDownList(ArrayHelper::map(MstLookup::listData('SafetyStatus'), 'code', 'name'), [
            'prompt' => '= Select Status =',
            'id' => "TrxConversationDetails_{$key}_status",
            'name' => "TrxConversationDetails[$key][status]",
        ])->label(false)
        ?>

        <?=
        $form->field($detail, 'dead_line', [
            'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-12',
            ]
        ])->textInput([
            'id' => "TrxConversationDetails_{$key}_dead_line",
            'name' => "TrxConversationDetails[$key][dead_line]",
            'class' => 'form-control tanggal',
        ])->label(false)
        ?>
    </div>
</td>
<td>
    <button type="button" class="btn btn-danger btn-remove-detail btn-xs"><i class="fa fa-trash"></i></button>
</td>