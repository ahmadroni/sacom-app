<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\Inflector;

$imageUrl = Yii::getAlias('@web').'/img/user1-128x128.jpg';
if(!empty($model->posUser->image_path) && file_exists(Yii::getAlias('@web').$model->posUser->image_path)){
    $imageUrl = Yii::getAlias('@web').$model->posUser->image_path;
}
?>
<div class="box box-info trx-conversation-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::a(Inflector::camel2words($model->title), ['detail', 'id' => $model->id]) ?></h3>       
    </div>
    <div class="box-body">
        <!-- Post -->
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?= $imageUrl; ?>" alt="user image">
                <span class="username">
                    <a href="#"><?= $model->postBy->fullname ?></a>
                </span>
                <span class="description">
                    <i>Lokasi :</i>
                    <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                    , <i>Area :</i>
                    <strong><?= $model->area->area_name; ?> </strong>
                    , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
                </span>
            </div>
            <strong><i class="fa fa-comment-o margin-r-5"></i> Pembicaraan : </strong>
            <p> 
                <!-- /.user-block -->
                <?= HtmlPurifier::process($model->conversation) ?> 
            </p>
            <ul class="list-inline">
                <li><a href="<?= Url::to(['trx-safety/detail','id'=>$model->id])?>" class="btn btn-info btn-xs"><i class="fa fa-eye margin-r-5"></i> Detail</a></li>
                <?php if($model->hasFollowUp) : ?>
                <li><a href="<?= Url::to(['trx-safety/response','id'=>$model->id])?>" class="btn btn-success btn-xs"><i class="fa fa-reply-all margin-r-5"></i> Need Response</a></li>
                <?php endif;?>
                <li class="pull-right">
                    <a href="<?= Url::to(['trx-safety/detail','id'=>$model->id])?>" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Follow Up(<?= $model->commentCount ?>)</a>
                </li>
            </ul>
        </div>
        <!-- /.post -->
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>