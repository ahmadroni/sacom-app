<?php
use yii\widgets\ListView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrxConversationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Safety Conversation');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callout callout-info" style="padding: 5px 10px 20px 10px;">
    <h5><i class="fa fa-info"></i> Note : </h5>
    Click Add button to create new safety conversation
    <?= Html::a('<i class="fa fa-plus"></i> Add',['create'],['class'=>'btn btn-sm btn-primary pull-right']); ?>
</div>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'summary'=>'',
]);
?>

<?php ob_start(); // output buffer the javascript to register later  ?>
<script>


</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 