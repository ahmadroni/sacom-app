<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\Inflector;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Safety Conversation'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detail')];

$currentController = Yii::$app->controller->id;
?>
<div class="box box-info trx-conversation-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Inflector::camel2words($model->title) ?></h3>        
        <div class="box-tools">
            <?php if($model->allowComment) : ?>
            <a href="<?= Url::to(['trx-safety/response','id'=>$model->id])?>" class="btn btn-sm btn-success"><i class="fa fa-reply-all margin-r-5"></i> Response</a>
            <?php endif; ?>
            <?php if($model->isAuthor && !$model->isClose) : ?>
            <a href="<?= Url::to(['trx-safety/closing','id'=>$model->id])?>" class="btn btn-sm btn-warning"><i class="fa fa-close margin-r-5"></i> Closing</a>
            <?php endif; ?>
        </div>
        
    </div>
    <div class="box-body">
        <!-- Post -->
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?= \Yii::$app->request->BaseUrl . '/img/user1-128x128.jpg' ?>" alt="user image">
                <span class="username">
                    <a href="#"><?= $model->postBy->fullname ?></a>
                </span>
                <span class="description">
                    <i>Lokasi :</i>
                    <strong><i class="fa fa fa-map-marker margin-r-5"></i><?= $model->location->location_name ?></strong>
                    , <i>Area :</i>
                    <strong><?= $model->area->area_name; ?> </strong>
                    , <i>Posting :</i> <?= date('d-m-Y H:i:s', strtotime($model->created_at)); ?>
                </span>
            </div>
            <strong> 
                Resiko : <i><?= $model->riskLookup->name ?> </i> | Kemungkinan : <i><?= $model->posibilityLookup->name ?> </i> |
                Kategori : <i><?= $model->categoryLookup->name ?> </i> 
            </strong> <br/>
            <strong>
                Tipe : <i><?= $model->typeLookup->name ?></i> | Kelas : <i><?= $model->levelLookup->name ?> </i> |
                Tingkatan Resiko : <i><?= $model->risk_level ?></i>
            </strong>
            <br>
            <strong><i class="fa fa-comment-o margin-r-5"></i> Pembicaraan : </strong>
            <p> 
                <?= HtmlPurifier::process($model->conversation) ?> 
            </p>
            <strong><i class="fa fa-comments-o margin-r-5"></i>Tindakan Yang Disepakati :</strong>
            <p>
                <?= HtmlPurifier::process($model->action_deal) ?> 
            </p>
            <strong><i class="fa fa-users margin-r-5"></i> Person Comunicated :</strong>
            <!-- The timeline -->
            <ul class="timeline timeline-inverse">
                <?php foreach ($model->trxConversationDetails as $item): ?>
                    <li>
                        <i class="fa fa-user bg-aqua"></i>
                        <div class="timeline-item">
                            <h3 class="timeline-header"><a href="#"><?= $item->forUser->username; ?></a></h3>
                            <div class="timeline-body">
                                <?= $item->notes; ?>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="list-inline">
                <li class="pull-right">
                    <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Follow Up(<?= $model->commentCount ?>)</a>
                </li>
            </ul>
            <!-- /.post -->  
            <strong><i class="fa fa-list margin-r-5"></i> List Follow Up :</strong>
            <ul class="timeline timeline-inverse">
                <?php foreach ($model->trxConversationComments as $comment): ?>
                    <li>
                        <i class="fa fa-comments bg-yellow"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> <?= date('d-m-Y H:i:s', strtotime($comment->created_at)); ?></span>
                            <h3 class="timeline-header"><a href="#"><?= $comment->postBy->username ?></a> [<?= $comment->status ?>]</h3>
                            <div class="timeline-body"><?= HtmlPurifier::process($comment->content) ?> </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div> 
    </div>


    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"><?= $this->title; ?> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>