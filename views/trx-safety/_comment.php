<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MstPropinsi;
use app\models\MstLookup;

/* @var $this yii\web\View */
/* @var $model app\models\search\MstTersangkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin([
            'id' => 'form-safety-comment',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'offset' => 'col-sm-offset-0',
                    'wrapper' => 'col-sm-12 col-md-12',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]);
?>
<?php if ($model->hasErrors()) : ?>
    <div class="callout callout-warning">
        <?= $form->errorSummary($model); ?>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-sm-8 col-md-8 col-lg-8">
        <?= $form->field($model, 'content')->textArea(['placeholder' => 'Type Comment', 'rows' => 4]) ?>
    </div>
    <div class="col-sm-2 col-md-2 col-lg-2">
        <?=
        $form->field($model, 'status')->dropDownList(ArrayHelper::map(\app\models\TrxConversation::responseStatus(), 'code', 'name'))
        ?>
    </div>
    <div class="col-sm-2 col-md-2 col-lg-2">
        <?= $form->field($model, 'conversation_id')->hiddenInput()->label(false) ?>
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

</div>
