<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MstPropinsiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// current Contrroler
$currentController = Yii::$app->controller->id;
$this->title = 'Master Negara';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info mst-negara-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>        
        <div class="box-tools">
            <button type="button" class="btn btn-info btn-sm btn-add"><i class="fa fa-plus"></i> Add</button>            
        </div>
    </div>
    <div class="box-body no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary'=>'',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'kode_negara',
                'nama_negara',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'options' => ['style' => 'width:100px;'],
                    'buttons' => [
                        'view' => function($url, $model, $key) {
                            return Html::button('<span class="fa fa-eye"></span>', [
                                        'class' => 'btn btn-xs btn-success btn-view',
                                        'title' => Yii::t('yii', 'View Data'),
                                        'value' => $key,
                            ]);
                        },
                        'update' => function($url, $model, $key) {
                            return Html::button('<span class="fa fa-pencil"></span>', [
                                        'class' => 'btn btn-xs btn-warning btn-update',
                                        'title' => Yii::t('yii', 'Update Data'),
                                        'value' => $key,
                            ]);
                        },
                        'delete' => function($url, $model, $key) {
                            return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-danger',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                            ]);
                        }
                    ],
                ],
            ],
        ]);
        ?>
            </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

<div id="modal-input" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="modal-title" class="modal-title"> </h4>
            </div>
            <div id="modal-data" class="modal-body">

            </div>
        </div>
    </div>
</div>


<?php ob_start(); ?>
<script>
    // add button
    $('.btn-add').on('click', function () {
        $.ajax({
            url: '<?=  Url::to([$currentController . '/create']) ?>',
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('New <?=  Html::encode($this->title) ;?>');                
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // update
    $(document).on('click', '.btn-update', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?=  Url::to([$currentController . '/update']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('Update <?=  Html::encode($this->title) ;?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    // view
    $(document).on('click', '.btn-view', function () {
        var id = $(this).val();
        $.ajax({
            url: '<?=  Url::to([$currentController . '/view']) ?>',
            data: {'id': id},
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $('#modal-data').html(data);
                $('#modal-title').html('View <?=  Html::encode($this->title) ;?>');
                $('#modal-input').modal('show');
                $('.overlay').hide();
            },
            beforeSend:function(){
                $('.overlay').show();
            },
        });
    });

    $('#modal-input').on('submit', 'form', function () {
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: function (data) {
                if (data.result == 'success') {
                    $.pjax.reload({container: '#<?= $currentController ?>-pjax'});
                    $('#modal-input').modal('hide');
                    $(this).trigger('reset');
                } else {
                    $('#modal-data').html(data);
                    $('#modal-input').modal('show');
                }
                $('.overlay').hide();
                $("#modal-input").find(".btn-save").prop('disabled',false);
            },
            beforeSend:function(){
                $("#modal-input").find(".btn-save").prop('disabled',true);
                $('.overlay').show();
            },
        });
        return false;
    });
</script>
<?php  $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?> 