<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MstPropinsi */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="mst-negara-form">

    <?php $form = ActiveForm::begin([
                'id' =>'form-mst-negara',
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3 col-md-3 col-lg-3',
                        'offset' => '',
                        'wrapper' => 'col-sm-8 col-md-8 col-lg-8',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]); ?>
    
    <?= $form->field($model, 'kode_negara')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'nama_negara')->textInput(['maxlength' => true]) ?>

    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', '<i class="fa fa-save"></i>  Save'), ['class' => 'btn btn-success btn-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
