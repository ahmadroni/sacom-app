<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MstLookup;
use app\models\MstLocation;

/* @var $this yii\web\View */
/* @var $model app\models\search\MstTersangkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
        'id' =>'form-report',
    ]); ?>
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <?=
            $form->field($model, 'type')->dropDownList(ArrayHelper::map(MstLookup::listData("SafetyType"), 'code', 'name'), ['prompt' => '= Select Type ='])
            ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?=
            $form->field($model, 'location')->dropDownList(ArrayHelper::map(MstLocation::listData(), 'id', 'location_name'), ['prompt' => '= Select Lokasi ='])
            ?>
        </div>
        <div class="col-md-3 col-sm-2">
            <?= $form->field($model, 'tgl_awal',[
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
            ])->textInput(['placeholder'=>'dd-mm-yy','class'=>'form-control tanggal']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <?= $form->field($model, 'tgl_akhir',[
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
            ])->textInput(['placeholder'=>'dd-mm-yy','class'=>'form-control tanggal']) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <br/>
            <?= Html::submitButton('<i class="fa fa-search"></i> Search', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
