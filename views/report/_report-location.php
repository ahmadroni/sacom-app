<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\MstLocation;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\search\MstTersangkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
        'id' =>'form-report',
    ]); ?>
    <div class="row">
        <div class="col-md-3 col-sm-2">
            <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(MstLocation::listData(), 'id', 'location_name'), ['class'=>'form-control', 'prompt' => '= Select Lokasi =']) ?>
        </div>
        <div class="col-md-3 col-sm-2">
            <?= $form->field($model, 'tgl_awal',[
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
            ])->textInput(['placeholder'=>'dd-mm-yy','class'=>'form-control tanggal tgl-awal']) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <?= $form->field($model, 'tgl_akhir',[
                'inputTemplate' => '<div class="input-group">{input}<div class="input-group-addon"><i class="fa fa-calendar"></i></div></div>',
            ])->textInput(['placeholder'=>'dd-mm-yy','class'=>'form-control tanggal tgl-akhir']) ?>
        </div>
        <div class="col-md-3 col-sm-3">
            <br/>
            <?= Html::submitButton('<i class="fa fa-search"></i> Search', ['name'=>'submit','value'=>'search', 'class' => 'btn btn-primary']) ?>
            <?= Html::submitButton('<i class="fa fa-file-excel-o"></i> Export', ['name'=>'submit','value'=>'export', 'class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
