<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=report-excel.xls");
//echo '<pre>'. print_r($dataProvider->models) .'</pre>';
?>
<table border="1">
    <thead>
        <tr>
            <th>No.</th>
             <?php foreach($dataProvider[0] as $key => $value){
                if($key != 'id'){
                    echo "<th>". $key."</th>";
                 }
            } ?>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
    <?php 
    $no=1;
    $totalSum[0]=0;
    foreach($dataProvider as $models) {
        echo "<tr>";
        echo "<td>".$no."</td>";
        $cols = 0;
        $totalTd = 0;
        foreach($models as $key => $value){
            if($key != 'id'):
                if($cols==0){
                    echo "<td>". $value."</td>";
                } else{
                    echo "<td class='text-right'>". number_format($value,2,',','.')."</td>";
                    $totalTd+=$value;
                    if(!array_key_exists($cols, $totalSum)){
                        array_push($totalSum, $value);
                    }else {
                       $totalSum[$cols]+=$value;  
                    }
                }
                $cols++;
            endif;
        }
        if(!array_key_exists($cols, $totalSum)){
            array_push($totalSum, $totalTd);
        }else {
           $totalSum[$cols]+=$totalTd;  
        }
        echo "<td>". $totalTd."</td>";
        echo "</tr>";
    $no++; 
    }?>
    <tr>
        <?php foreach($totalSum as $key => $total){ 
            if($key == 0) {
                echo "<td colspan='2'>Total</td>";
            }
            else{
                echo "<td>".$total."</td>";
            }
        }?>
    </tr>
    </tbody>
</table>