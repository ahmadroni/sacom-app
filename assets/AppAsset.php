<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bootstrap/dist/css/bootstrap.min.css',       
        'plugins/bootstrap-toggle/css/bootstrap-toggle.min.css',
        'plugins/font-awesome/css/font-awesome.min.css',
        'plugins/ionicons/css/ionicons.min.css',
        'plugins/select2/dist/css/select2.min.css',
        'plugins/adminlte/css/AdminLTE.min.css',
        'plugins/adminlte/css/skins/_all-skins.min.css',
        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        'plugins/bootstrap-daterangepicker/daterangepicker.css',
        'plugins/iCheck/square/blue.css',
    ];
    public $js = [
        //'plugins/jquery/jquery-3.3.1.min.js',
        'plugins/jquery-ui/jquery-ui.min.js',
        //'plugins/jquery-ui/jquery-migrate-3.0.0.min.js',
        'plugins/bootstrap/dist/js/bootstrap.min.js',        
        'plugins/moment/min/moment.min.js',
        'plugins/bootstrap-daterangepicker/daterangepicker.js',
        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'plugins/bootstrap-toggle/js/bootstrap-toggle.min.js',
        'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'plugins/fastclick/lib/fastclick.js',
        'plugins/select2/dist/js/select2.full.min.js',
        'plugins/adminlte/js/adminlte.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

}
