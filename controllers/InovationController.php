<?php

namespace app\controllers;

use Yii;
use app\models\TrxInovation;
use app\models\MstUser;
use app\models\search\TrxInovationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InovationController implements the CRUD actions for TrxInovation model.
 */
class InovationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrxInovation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrxInovationSearch();
        $userId =  Yii::$app->user->identity->id;
        $user = MstUser::findOne($userId);
        
        $query = "SELECT t1.* FROM trx_inovation t1 ". 
                "INNER JOIN mst_location t2 ON t1.location_id=t2.id ".
                "WHERE t1.created_by=". $userId ." OR t2.inovation_email LIKE '%". $user->email ."%' ".
                "ORDER BY t1.created_at DESC";
        $count = "SELECT COUNT(*) FROM (" . $query . ") tt";
        $totalCount = Yii::$app->db->createCommand($count)->queryScalar();
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => [
                'attributes' => [
                    'location_name',
                ]
            ]
        ]);
        
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $userId);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrxInovation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('item', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrxInovation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrxInovation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // send mail
            $model->sendEmail();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TrxInovation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TrxInovation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxInovation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxInovation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrxInovation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
