<?php

namespace app\controllers;

use Yii;
use app\models\MstWorkflow;
use app\models\TrxInvoice;
use app\models\TrxInvoiceProblem;
use app\models\form\TrxInvoiceForm;
use app\models\search\TrxInvoiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxInvoiceController implements the CRUD actions for TrxInvoice model.
 */
class TrxInvoiceController extends Controller {
    /*
     * Query contoh dengan SQL
     *
     * 
      $queryCount = "SELECT COUNT(*) FROM trx_invoice i ".
      "INNER JOIN trx_invoice_wf wf on wf.invoice_id=i.id ".
      "WHERE wf.role_from='". $role ."' AND wf.submited=0  AND wf.ready=1 ";
      /*
      "UNION ".
      "SELECT i.* FROM trx_invoice i ".
      "INNER JOIN trx_invoice_wf wf on wf.invoice_id=i.id ".
      "WHERE wf.role_to='". $role ."' AND wf.submited=1 AND wf.ready=1) as count";

      $totalCount = Yii::$app->db->createCommand($queryCount)->queryScalar();

      $queryAll = "SELECT i.* FROM trx_invoice i ".
      "INNER JOIN trx_invoice_wf wf on wf.invoice_id=i.id ".
      "WHERE wf.role_from='". $role ."' AND wf.submited=0 AND wf.ready=1 ";
      /*
      "UNION ".
      "SELECT i.* FROM trx_invoice i ".
      "INNER JOIN trx_invoice_wf wf on wf.invoice_id=i.id ".
      "WHERE wf.role_to='". $role ."' AND wf.submited=1 AND wf.ready=1 ";


      $dataProvider = new \yii\data\SqlDataProvider([
      'sql' => $queryAll,
      'totalCount'=>$totalCount,
      'pagination'=>[
      'pageSize' => 10
      ],
      'sort' => [
      'attributes' => [
      'id',
      'username',
      'roleName',
      ]
      ]
      ]);
     * 
     */

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrxInvoice models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TrxInvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all TrxInvoice models.
     * @return mixed
     */
    public function actionList() {
        $model = new TrxInvoiceSearch();
        $userId = Yii::$app->user->identity->id;
        $where = "";
        if($model->load(Yii::$app->request->post())){
            $createdAt = ($model->created_at=="" ? "" : date('Y-m-d',strtotime($model->created_at)));
            $where="AND t1.supplier_id LIKE '%". $model->supplier_id ."%' AND t1.created_at LIKE '%".$createdAt ."%' ".
                    "AND t1.recieve_from LIKE '%". $model->recieve_from ."%' AND t1.status LIKE '%". $model->status ."%' ";
        }
        $query = "SELECT t1.*, t5.supplier_name,COUNT(t6.invoice_id) as detail_count FROM trx_invoice t1 ". 
                "INNER JOIN trx_invoice_wf t2 on t1.id=t2.invoice_id ".
                "INNER JOIN auth_assignment t3 on t2.role_from = t3.item_name ".
                "INNER JOIN mst_user t4 on t3.user_id=t4.id ".
                "INNER JOIN mst_supplier t5 on t1.supplier_id=t5.id ".
                "LEFT JOIN trx_invoice_detail t6 on t6.invoice_id=t1.id ".
                "WHERE t2.submited=0 AND t2.ready=1 AND t3.user_id='". $userId ."' AND t1.status!='". TrxInvoice::COMPLETED ."' ".$where .
                "GROUP BY t1.id ORDER BY t1.created_at DESC";
        $count = "SELECT COUNT(*) FROM (" . $query . ") tt";
        $totalCount = Yii::$app->db->createCommand($count)->queryScalar();
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 150
            ],
            'sort' => [
                'attributes' => [
                    'location_name',
                ]
            ]
        ]);

        return $this->render('list', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSubmitAll() {
        $datas = Yii::$app->request->post('selection');
        $role = Yii::$app->user->identity->role;
        $userId = Yii::$app->user->identity->id;
        foreach($datas as $data){
            TrxInvoice::updateWorkflow($data, $role);
        }
        return $this->redirect(['list']);
    }
    
    public function actionApproved(){
        $model = new TrxInvoiceSearch();
        $userId = Yii::$app->user->identity->id;
        $where = "";
        if($model->load(Yii::$app->request->post())){
            $createdAt = ($model->created_at=="" ? "" : date('Y-m-d',strtotime($model->created_at)));
            $where="AND t1.supplier_id LIKE '%". $model->supplier_id ."%' AND t1.created_at LIKE '%".$createdAt ."%' ".
                    "AND t1.recieve_from LIKE '%". $model->recieve_from ."%' AND t1.status LIKE '%". $model->status ."%' ";
        }
        $query = "SELECT t1.*, t5.supplier_name,t6.tcount as detail_count FROM trx_invoice t1 ". 
                "INNER JOIN trx_invoice_wf t2 on t1.id=t2.invoice_id ".
                "INNER JOIN auth_assignment t3 on t2.role_from = t3.item_name ".
                "INNER JOIN mst_user t4 on t3.user_id=t4.id ".
                "INNER JOIN mst_supplier t5 on t1.supplier_id=t5.id ".
                "INNER JOIN (SELECT invoice_id, count(invoice_id) as tcount FROM trx_invoice_detail GROUP BY invoice_id) t6 on t6.invoice_id=t1.id ".
                "WHERE t2.submited=0 AND t2.ready=1 AND t3.user_id='". $userId ."' AND t1.status!='". TrxInvoice::COMPLETED ."' ".$where .
                "ORDER BY t1.created_at DESC";
        $count = "SELECT COUNT(*) FROM (" . $query . ") tt";
        $totalCount = Yii::$app->db->createCommand($count)->queryScalar();
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 150
            ],
            'sort' => [
                'attributes' => [
                    'location_name',
                ]
            ]
        ]);

        return $this->render('approve', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProblem($id){
        $model = new TrxInvoiceProblem();
        $model->invoice_id = $id;
        $model->user_id = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $role = Yii::$app->user->identity->role;
            TrxInvoice::rejectWorkflow($id, $role);
            TrxInvoice::addHistory($id, Yii::$app->user->identity->id,"Invoice Problem - ". $model->notes);

            // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['list']);
        }

        return $this->renderPartial('_form-problem',['model'=>$model]);
    }

    public function actionProses($id) {
        
        $model = $this->findModel($id);
        $role = Yii::$app->user->identity->role;
        $workflow = MstWorkflow::invoiceWfGroup('APPROVAL','INVOICE',$model->category,$role);
        $proses = $workflow == null ? '' : $workflow->status_label;
        $reject = (MstWorkflow::invoiceWfGroup("REJECTION", "INVOICE", $model->category, $role) == null ? false : true);
        
        if (Yii::$app->request->post()) {
            TrxInvoice::updateWorkflow($id, $role);
            return $this->redirect(['list']);
        }
        return $this->renderPartial('_proses', ['model' => $model, 'proses' => $proses, 'reject' => $reject], false);
    }

    public function actionReject($id) {
        $role = Yii::$app->user->identity->role;
        TrxInvoice::rejectWorkflow($id, $role);

        return $this->redirect(['list']);
    }

    /**
     * Displays a single TrxInvoice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->renderPartial('_view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrxInvoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $role = Yii::$app->user->identity->role;

        $model = new TrxInvoiceForm();
        $model->trxInvoice = new TrxInvoice;
        $model->trxInvoice->loadDefaultValues();

        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            TrxInvoice::addWorkflow($model->trxInvoice->id, $role);
            TrxInvoice::addHistory($model->trxInvoice->id, Yii::$app->user->identity->id,TrxInvoice::OPEN);

            return $this->redirect(['list']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing TrxInvoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $role = Yii::$app->user->identity->role;

        $model = new TrxInvoiceForm();
        $model->trxInvoice = $this->findModel($id);
        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            $model->trxInvoice->addWorkflow($id, $role);
            return $this->redirect(['list']);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing TrxInvoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = TrxInvoice::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
