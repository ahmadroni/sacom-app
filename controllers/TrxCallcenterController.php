<?php

namespace app\controllers;

use Yii;
use app\models\TrxCallCenter;
use app\models\form\TrxCallCenterForm;
use app\models\search\TrxCallCenterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MstLookup;
use app\models\MstCustomer;
use app\models\MstCompany;
use app\models\form\ReportForm;

/**
 * TrxCallcenterController implements the CRUD actions for TrxCallcenter model.
 */
class TrxCallcenterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all TrxCallcenter models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrxCallCenterSearch();
        $role = Yii::$app->user->identity->role;
        $dataProvider = $searchModel->searchByRole(Yii::$app->request->queryParams, $role); 

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionProses($id){
        
        $model = $this->findModel($id);
        
        $role = Yii::$app->user->identity->role;
        $proses = \app\models\MstWorkflow::labelProses('CALL-CENTER', $role);
                
        if (Yii::$app->request->post() && TrxCallCenter::updateWorkflow($id, $role)) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['list']);

        }
        return $this->renderPartial('_proses', ['model' => $model, 'proses' =>$proses], false);
    }
    
    public function actionReject($id)
    {
        $role = Yii::$app->user->identity->role;
        TrxCallCenter::rejectWorkflow($id, $role);

        return $this->redirect(['list']);
    }

    /**
     * Displays a single TrxCallcenter model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderPartial('_view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrxCallcenter model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $role = Yii::$app->user->identity->role;
        
        $model = new TrxCallCenterForm();
        $model->trxCallCenter = new TrxCallCenter;
        $model->trxCallCenter->created_at = date('Y-m-d');
        $model->trxCallCenter->loadDefaultValues();  
        
        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {  
            TrxCallCenter::addWorkflow($model->trxCallCenter->id, $role);
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }
    
    /**
     * Updates an existing TrxCallcenter model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $role = Yii::$app->user->identity->role;
        
        $model = new TrxCallCenterForm();
        $model->trxCallCenter = $this->findModel($id);
        $model->trxCallCenter->created_at = date('Y-m-d',strtotime($model->trxCallCenter->created_at));
        $model->mstCustomer = $this->findCustomer($model->trxCallCenter->customer_id);
        $model->mstCustomer->nama_propinsi = $model->mstCustomer->propinsi->nama_propinsi;
        $model->mstCustomer->nama_kota = $model->mstCustomer->kota->nama_kota;

        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {  
            $model->trxCallCenter->addWorkflow($id,$role);
            return $this->redirect(['index']);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing TrxCallcenter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxCallcenter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxCallcenter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = \app\models\TrxCallCenter::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    protected function findCustomer($id)
    {
        if (($model = \app\models\MstCustomer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    public function actionReport() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" WHERE created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        if(Yii::$app->request->post('search')=='export'){
            
            $query = "SELECT cc.created_at as 'tgl_input', cus.customer_name as 'nama_customer', cus.address as 'alamat_customer', l3.name as 'jenis_customer', "
                    . "kt1.nama_kota as 'kota_customer', prop1.nama_propinsi as 'propinsi_customer',cus.email as 'email_customer', "
                    . "cus.nomor_hp as 'nomo_hp_customer', cus.nomor_telp as 'nomor_telp_customer', cus.sales_pic as 'sales_pic_customer', "
                    . "com.company_name as 'nama_company', com.company_desc as 'desc_company', com.address as 'address_company', "
                    . "kt2.nama_kota as 'kota_company', prop2.nama_propinsi as 'propinsi_company',"
                    . "com.email as 'email_company', com.no_telp as 'telp_company', "
                    . "l1.name as 'jenis_panggilan', l2.name as 'jenis_media', "
                    . "cc.isi_pertanyaan, cc.isi_jawaban,up1.full_name as 'sales_pic' "
                    . "FROM trx_callcenter cc "
                    . "LEFT JOIN mst_customer cus ON cus.id=cc.customer_id "
                    . "LEFT JOIN mst_company com ON com.id=cc.company_id "
                    . "LEFT JOIN mst_lookup l1 ON l1.code=cc.jenis_panggilan "
                    . "LEFT JOIN mst_lookup l2 ON l2.code=cc.jenis_media "
                    . "LEFT JOIN mst_lookup l3 ON l3.code=cus.category_id "
                    . "LEFT JOIN mst_user_profile up1 ON up1.user_id=cc.sales_pic "
                    . "LEFT JOIN mst_user_profile up2 ON up2.user_id=cc.created_by "
                    . "LEFT JOIN mst_propinsi prop1 ON cus.propinsi_id=prop1.id "
                    . "LEFT JOIN mst_kota kt1 ON cus.kota_id=kt1.id "
                    . "LEFT JOIN mst_propinsi prop2 ON com.propinsi_id=prop2.id "
                    . "LEFT JOIN mst_kota kt2 ON com.kota_id=kt2.id "
                    . "WHERE cc.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND cc.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' "
                    . "ORDER BY cc.created_at ASC";
            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }else{
            $query = "SELECT lu.name as JenisPanggilan, count(cc.id) as Jumlah, ".
                " ( (count(cc.id)/ (SELECT COUNT(id) FROM trx_callcenter ". $where ."  )*100)) as Percen ".
                " FROM ( SELECT name, code FROM mst_lookup WHERE type='CCCallType') lu ".
                " LEFT JOIN (SELECT id, jenis_panggilan FROM trx_callcenter ". $where.") cc on cc.jenis_panggilan=lu.code GROUP by lu.name";
            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
            return $this->render('report', ['model'=>$model, 'dataProvider' => $dataProvider]);
        }         
    }
}
