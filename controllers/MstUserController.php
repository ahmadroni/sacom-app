<?php

namespace app\controllers;

use Yii;
use app\models\MstUserProfile;
use app\models\MstUser;
use app\models\form\MstUserForm;
use app\models\search\MstUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MstUserProfileController implements the CRUD actions for MstUserProfile model.
 */
class MstUserController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MstUserProfile models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MstUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MstUserProfile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->renderPartial('_view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MstUserProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MstUserForm();
        $model->mstUser = New MstUser;
        $model->mstUser->scenario = 'insert';
        $model->mstUserProfile = new MstUserProfile;
        //$model->trxHazard->loadDefaultValues();

        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionSignup() {
        $model = new MstUser();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['index']);
        }
        return $this->render('signup', ['model' => $model]);
    }

    /**
     * Updates an existing MstUserProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = new MstUserForm();
        $model->mstUser = $this->findModel($id);

        if ($model->mstUser->mstUserProfile === null) {
            $model->mstUserProfile = new MstUserProfile();
        } else {
            $model->mstUserProfile = $model->mstUser->mstUserProfile;
        }

        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }
    
    public function actionDisabled($id) {
        $data = $this->findModel($id);
        $data->setScenario('disabled');
        $data->setAttributes(Yii::$app->request->post());
        $data->status=MstUser::STATUS_INACTIVE;
        if (Yii::$app->request->post() && $data->save()) {
            //$data->save();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['result' => 'success'];
            //return $this->redirect(['index']);
        }
        return $this->renderPartial('_disabled', [
                    'model' => $data,
        ], false);
    }

    /**
     * Deletes an existing MstUserProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        {
            //$this->findModel($id)->delete();
            $model->status = MstUser::STATUS_INACTIVE;
            $model->save();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the MstUserProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MstUserProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MstUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
