<?php

namespace app\controllers;

use Yii;
use app\models\form\ReportForm;
use app\models\form\ReportSafetyForm;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\MstUser;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class ReportSafetyController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }   
    // Report Safety Follow Up
    public function actionByFollowUp() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
      
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.POS) as POS, SUM(jo.NEG) as NEG, SUM(jo.FU) as FU
                FROM mst_location loc 
                INNER JOIN (
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.levels = 'SafetyLevel01' THEN 1 END) as 'POS',
                    COUNT(CASE WHEN t.levels = 'SafetyLevel02' THEN 1 END) as 'NEG',
                    COUNT(conversation_id) as 'FU'
                    FROM
                    (
                        SELECT lc.id,lc.parent_id, cm.conversation_id, ms.level as levels
                        FROM mst_location lc
                        INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                        LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                        WHERE lc.parent_id<>0 ". $where."
                    ) t
                    GROUP by t.id
                ) jo ON loc.id = jo.parent_id
                GROUP BY loc.id
                UNION
                SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.levels = 'SafetyLevel01' THEN 1 END) as 'POS',
                COUNT(CASE WHEN t.levels = 'SafetyLevel02' THEN 1 END) as 'NEG',
                COUNT(conversation_id) as 'FU'
                FROM
                (
                    SELECT lc.id,lc.location_name, lc.parent_id, cm.conversation_id, ms.level as levels
                    FROM mst_location lc
                    INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                    LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where."
                ) t
                GROUP by t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'SC Follow Up',
                'link'=>'follow-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }         
    }
    
    public function actionFollowByLocation($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.levels = 'SafetyLevel01' THEN 1 END) as 'POS',
                    COUNT(CASE WHEN t.levels = 'SafetyLevel02' THEN 1 END) as 'NEG',
                    COUNT(conversation_id) as 'FU'
                    FROM
                    (
                        SELECT lc.id,lc.location_name, lc.parent_id, cm.conversation_id, ms.level as levels
                        FROM mst_location lc
                        INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                        LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                        WHERE lc.parent_id=". $id . $where."
                    ) t
                    GROUP by t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'SC Follow Up Location :: '.$location->location_name,
                    'link'=>'follow-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            }       
        }else {
            return $this->redirect(['follow-by-user','id'=>$id]);
        }
        
    }
    
    public function actionFollowByUser($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
            COUNT(CASE WHEN t.level = 'SafetyLevel01' THEN 1 END) as 'POS',
            COUNT(CASE WHEN t.level = 'SafetyLevel02' THEN 1 END) as 'NEG',
            COUNT(conversation_id) as 'FU'
            FROM
            (
                SELECT up.user_id, up.full_name, cm.conversation_id, ms.level as level
                FROM mst_user_profile up
                INNER JOIN trx_conversation ms ON ms.created_by=up.user_id
                LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                WHERE ms.location_id=".$id . $where ."
            ) t
            GROUP BY t.user_id
            ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'SC Follow Up Location :: '.$location->location_name,
                'link'=>'follow-by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionFollowByPost($id, $locId) {
        $model = new ReportForm();        
        $where ="";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT ms.id, ms.created_at as 'Date', ms.title,
                COUNT(CASE WHEN ms.level = 'SafetyLevel01' THEN 1 END) as 'POS',
                COUNT(CASE WHEN ms.level = 'SafetyLevel02' THEN 1 END) as 'NEG',
                COUNT(conversation_id) as 'FU'
                FROM trx_conversation ms
                LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                WHERE ms.location_id=". $locId ." AND ms.created_by=".$id . $where ." 
                GROUP BY ms.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('by-post', ['title'=>'SC Follow Up By :: '.$user->full_name, 'model'=>$model, 'dataProvider' => $dataProvider]); 
    }
    
    public function actionByStatus() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND trx.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND trx.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.OPEN) as 'OPEN', 
                SUM(jo.IN_PROGRESS) as 'IN_PROGRESS',
                SUM(jo.CLOSE) as 'CLOSE'
                FROM mst_location loc 
                INNER JOIN (
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                    COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                    COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, trx.status
                        FROM mst_location lc
                        LEFT JOIN trx_conversation trx ON lc.id = trx.location_id
                        WHERE lc.parent_id<>0 ". $where."
                    ) t
                    GROUP BY t.id
                )jo ON loc.id=jo.parent_id
                GROUP BY loc.id
                UNION
                SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT lc.id, lc.location_name, trx.status
                    FROM mst_location lc
                    LEFT JOIN trx_conversation trx ON lc.id = trx.location_id
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where."
                ) t
                GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Safety By Status',
                'link'=>'status-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }  
    }
    
    public function actionStatusByLocation($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                    COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                    COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                    FROM
                    (
                        SELECT lc.id,lc.location_name, lc.parent_id, ms.status
                        FROM mst_location lc
                        INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                        WHERE lc.parent_id=". $id . $where."
                    ) t
                    GROUP by t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Safety Status at Location :: '.$location->location_name,
                    'link'=>'status-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            }       
        }else {
            return $this->redirect(['status-by-user','id'=>$id]);
        } 
    }
    
    public function actionStatusByUser($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
            COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
            COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
            COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
            FROM
            (
                SELECT up.user_id, up.full_name, ms.status
                FROM mst_user_profile up
                INNER JOIN trx_conversation ms ON ms.created_by=up.user_id
                WHERE ms.location_id=".$id . $where ."
            ) t
            GROUP BY t.user_id
            ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Safety Status at Location :: '.$location->location_name,
                'link'=>'status-by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionStatusByPost($id, $locId) {
        $model = new ReportForm();        
        $where ="";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT ms.id, ms.created_at as 'Date', ms.title,
                COUNT(CASE WHEN ms.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN ms.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN ms.status = 'CLOSE' THEN 1 END) as 'CLOSE',
                COUNT(conversation_id) as 'FU'
                FROM trx_conversation ms
                LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                WHERE ms.location_id=".$locId." AND ms.created_by=".$id . $where ." 
                GROUP BY ms.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('by-post', [
            'title'=>'Safety Status By :: '.$user->full_name, 
            'model'=>$model, 'dataProvider' => $dataProvider]); 
    }
    
    public function actionByType() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" WHERE ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.SafetyType01) as 'Perilaku Berbahaya', 
                SUM(jo.SafetyType02) as 'Perilaku Aman',
                SUM(jo.SafetyType03) as 'Others'
                FROM mst_location loc 
                INNER JOIN (
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.types = 'SafetyType01' THEN 1 END) as 'SafetyType01',
                    COUNT(CASE WHEN t.types = 'SafetyType02' THEN 1 END) as 'SafetyType02',
                    COUNT(CASE WHEN t.types = 'SafetyType03' THEN 1 END) as 'SafetyType03'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id,ms.types
                        FROM mst_location lc
                        INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                        WHERE lc.parent_id<>0 ". $where."
                    ) t
                    GROUP BY t.id
                ) jo ON jo.parent_id=loc.id
                GROUP BY loc.id
                UNION
                SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.types = 'SafetyType01' THEN 1 END) as 'Perilaku Berbahaya',
                COUNT(CASE WHEN t.types = 'SafetyType02' THEN 1 END) as 'Perilaku Aman',
                COUNT(CASE WHEN t.types = 'SafetyType03' THEN 1 END) as 'Others'
                FROM
                (
                    SELECT lc.id, lc.location_name,ms.types
                    FROM mst_location lc
                    INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where."
                ) t
                GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Safety Type',
                'link'=>'type-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }        
    }
    
    public function actionTypeByLocation($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.types = 'SafetyType01' THEN 1 END) as 'Perilaku Berbahaya',
                    COUNT(CASE WHEN t.types = 'SafetyType02' THEN 1 END) as 'Perilaku Aman',
                    COUNT(CASE WHEN t.types = 'SafetyType03' THEN 1 END) as 'Others'
                    FROM
                    (
                        SELECT lc.id,lc.location_name, lc.parent_id, ms.types
                        FROM mst_location lc
                        INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                        WHERE lc.parent_id=". $id . $where."
                    ) t
                    GROUP by t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Safety Type at Location :: '.$location->location_name,
                    'link'=>'status-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            }       
        }else {
            return $this->redirect(['type-by-user','id'=>$id]);
        } 
    }
    
    public function actionTypeByUser($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
            COUNT(CASE WHEN t.types = 'SafetyType01' THEN 1 END) as 'Perilaku Berbahaya',
            COUNT(CASE WHEN t.types = 'SafetyType02' THEN 1 END) as 'Perilaku Aman',
            COUNT(CASE WHEN t.types = 'SafetyType03' THEN 1 END) as 'Others'
            FROM
            (
                SELECT up.user_id, up.full_name, ms.types
                FROM mst_user_profile up
                INNER JOIN trx_conversation ms ON ms.created_by=up.user_id
                WHERE ms.location_id=".$id . $where ."
            ) t
            GROUP BY t.user_id
            ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Safety Type at Location :: '.$location->location_name,
                'link'=>'type-by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionTypeByPost($id, $locId) {
        $model = new ReportForm();        
        $where ="";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT ms.id, ms.created_at as 'Date', ms.title,
                COUNT(CASE WHEN ms.types = 'SafetyType01' THEN 1 END) as 'Perilaku Berbahaya',
                COUNT(CASE WHEN ms.types = 'SafetyType02' THEN 1 END) as 'Perilaku Aman',
                COUNT(CASE WHEN ms.types = 'SafetyType03' THEN 1 END) as 'Others,'
                COUNT(conversation_id) as 'FU'
                FROM trx_conversation ms
                LEFT JOIN trx_conversation_comment cm ON ms.id = cm.conversation_id
                WHERE ms.location_id=".$locId." AND ms.created_by=".$id . $where ." 
                GROUP BY ms.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('by-post', [
            'title'=>'Safety Type By :: '.$user->full_name, 
            'model'=>$model, 'dataProvider' => $dataProvider]); 
    }
    
     public function actionByCategory() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $where =" WHERE ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
            SUM(jo.SafetyCategory01) as 'Alat Perilindungan Diri', 
            SUM(jo.SafetyCategory02) as 'Prosedur Kerja',
            SUM(jo.SafetyCategory03) as 'Peralatan Kerja',
            SUM(jo.SafetyCategory04) as 'Kebersihan',
            SUM(jo.SafetyCategory05) as 'Position of People',
            SUM(jo.SafetyCategory06) as 'Ergonomics',
            SUM(jo.SafetyCategory07) as 'Lingkungan'
            FROM mst_location loc 
            INNER JOIN (
                SELECT t.id, t.parent_id,
                COUNT(CASE WHEN t.category = 'SafetyCategory01' THEN 1 END) as 'SafetyCategory01',
                COUNT(CASE WHEN t.category = 'SafetyCategory02' THEN 1 END) as 'SafetyCategory02',
                COUNT(CASE WHEN t.category = 'SafetyCategory03' THEN 1 END) as 'SafetyCategory03',
                COUNT(CASE WHEN t.category = 'SafetyCategory04' THEN 1 END) as 'SafetyCategory04',
                COUNT(CASE WHEN t.category = 'SafetyCategory05' THEN 1 END) as 'SafetyCategory05',
                COUNT(CASE WHEN t.category = 'SafetyCategory06' THEN 1 END) as 'SafetyCategory06',
                COUNT(CASE WHEN t.category = 'SafetyCategory07' THEN 1 END) as 'SafetyCategory07'
                FROM
                (
                    SELECT lc.id, lc.parent_id, ms.category
                    FROM mst_location lc
                    INNER JOIN trx_conversation ms ON lc.id=ms.location_id    
                    WHERE lc.parent_id<>0 ". $where."
                ) t
                GROUP BY t.id
            ) jo ON jo.parent_id=loc.id
            GROUP BY loc.id
            UNION
            SELECT t.id, t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.category = 'SafetyCategory01' THEN 1 END) as 'Alat Perilindungan Diri',
            COUNT(CASE WHEN t.category = 'SafetyCategory02' THEN 1 END) as 'Prosedur Kerja',
            COUNT(CASE WHEN t.category = 'SafetyCategory03' THEN 1 END) as 'Peralatan Kerja',
            COUNT(CASE WHEN t.category = 'SafetyCategory04' THEN 1 END) as 'Kebersihan',
            COUNT(CASE WHEN t.category = 'SafetyCategory05' THEN 1 END) as 'Position of People',
            COUNT(CASE WHEN t.category = 'SafetyCategory06' THEN 1 END) as 'Ergonomics',
            COUNT(CASE WHEN t.category = 'SafetyCategory07' THEN 1 END) as 'Lingkungan'
            FROM
            (
                SELECT lc.id, lc.location_name, ms.category
                FROM mst_location lc
                INNER JOIN trx_conversation ms ON lc.id=ms.location_id    
                WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where."
            ) t
            GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'SC Category',
                'link'=>'category-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }
    } 
    
    public function actionCategoryByLocation($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.category = 'SafetyCategory01' THEN 1 END) as 'Alat Perilindungan Diri',
                    COUNT(CASE WHEN t.category = 'SafetyCategory02' THEN 1 END) as 'Prosedur Kerja',
                    COUNT(CASE WHEN t.category = 'SafetyCategory03' THEN 1 END) as 'Peralatan Kerja',
                    COUNT(CASE WHEN t.category = 'SafetyCategory04' THEN 1 END) as 'Kebersihan',
                    COUNT(CASE WHEN t.category = 'SafetyCategory05' THEN 1 END) as 'Position of People',
                    COUNT(CASE WHEN t.category = 'SafetyCategory06' THEN 1 END) as 'Ergonomics',
                    COUNT(CASE WHEN t.category = 'SafetyCategory07' THEN 1 END) as 'Lingkungan'
                    FROM
                    (
                        SELECT lc.id,lc.location_name, lc.parent_id, ms.category
                        FROM mst_location lc
                        INNER JOIN trx_conversation ms ON ms.location_id=lc.id
                        WHERE lc.parent_id=". $id . $where."
                    ) t
                    GROUP by t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Safety Category at Location :: '.$location->location_name,
                    'link'=>'category-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            }       
        }else {
            return $this->redirect(['category-by-user','id'=>$id]);
        } 
    }
    
    public function actionCategoryByUser($id) {
        $model = new ReportForm();        
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
            COUNT(CASE WHEN t.category = 'SafetyCategory01' THEN 1 END) as 'Alat Perilindungan Diri',
            COUNT(CASE WHEN t.category = 'SafetyCategory02' THEN 1 END) as 'Prosedur Kerja',
            COUNT(CASE WHEN t.category = 'SafetyCategory03' THEN 1 END) as 'Peralatan Kerja',
            COUNT(CASE WHEN t.category = 'SafetyCategory04' THEN 1 END) as 'Kebersihan',
            COUNT(CASE WHEN t.category = 'SafetyCategory05' THEN 1 END) as 'Position of People',
            COUNT(CASE WHEN t.category = 'SafetyCategory06' THEN 1 END) as 'Ergonomics',
            COUNT(CASE WHEN t.category = 'SafetyCategory07' THEN 1 END) as 'Lingkungan'
            FROM
            (
                SELECT up.user_id, up.full_name, ms.category
                FROM mst_user_profile up
                INNER JOIN trx_conversation ms ON ms.created_by=up.user_id
                WHERE ms.location_id=".$id . $where ."
            ) t
            GROUP BY t.user_id
            ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Safety Category at Location :: '.$location->location_name,
                'link'=>'category-by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionCategoryByPost($id, $locId) {
        $model = new ReportForm();        
        $where ="";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT ms.id, ms.created_at as 'Date', ms.title,
                COUNT(CASE WHEN ms.category = 'SafetyCategory01' THEN 1 END) as 'Alat Perilindungan Diri',
                COUNT(CASE WHEN ms.category = 'SafetyCategory02' THEN 1 END) as 'Prosedur Kerja',
                COUNT(CASE WHEN ms.category = 'SafetyCategory03' THEN 1 END) as 'Peralatan Kerja',
                COUNT(CASE WHEN ms.category = 'SafetyCategory04' THEN 1 END) as 'Kebersihan',
                COUNT(CASE WHEN ms.category = 'SafetyCategory05' THEN 1 END) as 'Position of People',
                COUNT(CASE WHEN ms.category = 'SafetyCategory06' THEN 1 END) as 'Ergonomics',
                COUNT(CASE WHEN ms.category = 'SafetyCategory07' THEN 1 END) as 'Lingkungan'
                FROM trx_conversation ms
                WHERE ms.location_id=".$locId." AND ms.created_by=".$id . $where ." 
                GROUP BY ms.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('by-post', [
            'title'=>'Safety Type By :: '.$user->full_name, 
            'model'=>$model, 'dataProvider' => $dataProvider]); 
    }
}
