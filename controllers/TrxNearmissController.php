<?php

namespace app\controllers;

use Yii;
use app\models\TrxNearmiss;
use app\models\TrxNearmissDetail;
use app\models\TrxNearmissComment;
use app\models\search\TrxNearmissSearch;
use app\models\form\TrxNearmissForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxNearmissController implements the CRUD actions for TrxNearmiss model.
 */
class TrxNearmissController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrxNearmiss models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TrxNearmissSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionList() {
        $searchModel = new TrxNearmissSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrxNearmiss model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetail($id) {
        return $this->render('detail', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionView($id) {
        return $this->renderPartial('_view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionClosing($id) {
        $model = New TrxNearmissComment();
        $model->loadDefaultValues();
        $model->nearmiss_id = $id;
        $model->status = TrxNearmiss::CLOSE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $parent = TrxNearmiss::findOne($id);
            if ($parent !== null) {
                $parent->status = TrxNearmiss::CLOSE;
                $parent->update();
            }
            return $this->redirect(['index']);
        }
        return $this->render('closing', [
                    'model' => $model,
        ]);
    }

    public function actionResponse($id) {
        $userId = Yii::$app->user->identity->id;
        $model = $this->findModel($id);
        $detail = $this->findDetail($id, $userId);

        $comment = New TrxNearmissComment();
        $comment->nearmiss_id = $id;
        if ($comment->load(Yii::$app->request->post()) && $comment->save()) {
            $this->updateStatus($detail->id, $comment->status);

            return $this->redirect(['index']);
        }

        return $this->render('response', [
                    'model' => $model,
                    'detail' => $detail,
                    'comment' => $comment,
        ]);
    }

    /**
     * Creates a new TrxNearmiss model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new TrxNearmissForm();
        $model->trxNearmiss = new TrxNearmiss;
        $model->trxNearmiss->loadDefaultValues();        
        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            if($model->trxNearmiss->risk_level=='HIGH'){
                $model->trxNearmiss->sendMail();
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing TrxNearmiss model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = new TrxNearmissForm();
        $model->trxNearmiss = $this->findModel($id);
        $model->setAttributes(Yii::$app->request->post());

        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['index']);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing TrxNearmiss model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxNearmiss model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxNearmiss the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function updateStatus($id, $status) {
        $model = TrxNearmissDetail::findOne($id);
        if ($model !== null) {
            $model->status = $status;
            $model->update();
        }
    }

    protected function findModel($id) {
        if (($model = TrxNearmiss::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findDetail($id, $userId) {
        if (($model = TrxNearmissDetail::find()->where(['nearmiss_id' => $id, 'for_user' => $userId])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
