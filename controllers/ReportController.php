<?php

namespace app\controllers;

use Yii;
use app\models\form\ReportForm;
use app\models\form\ReportSafetyForm;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\MstUser;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class ReportController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionDdManual() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" WHERE created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.status = 'Open' THEN 1 END) as 'Open',
            COUNT(CASE WHEN t.status = 'In Progress' THEN 1 END) as 'In Progress',
            COUNT(CASE WHEN t.status = 'Close' THEN 1 END) as 'Close'
            FROM
            (
                SELECT lc.id, lc.location_name, h.status
                FROM mst_location lc
                LEFT JOIN (SELECT location_id,status FROM trx_manual ". $where .") h on lc.id = h.location_id
            ) t
            GROUP BY t.id
            ORDER BY t.location_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('dd-manual', ['model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionCallCenter() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" WHERE created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT lu.name as JenisPanggilan, count(cc.id) as Jumlah, ".
                " ( (count(cc.id)/ (SELECT COUNT(id) FROM trx_callcenter ". $where ."  )*100)) as Percen ".
                " FROM ( SELECT name, code FROM mst_lookup WHERE type='CCCallType') lu ".
                " LEFT JOIN (SELECT id, jenis_panggilan FROM trx_callcenter ". $where.") cc on cc.jenis_panggilan=lu.code GROUP by lu.name";
 
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('call-center', ['model'=>$model, 'dataProvider' => $dataProvider]);
        }         
    }
}
