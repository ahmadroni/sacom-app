<?php

namespace app\controllers;

use Yii;
use app\models\form\ReportForm;
use app\models\TrxManual;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class ReportInvoiceController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" WHERE t1.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND t1.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $query = "SELECT t1.created_at as 'Tgl Invoice Masuk',t2.supplier_name as 'Nama Supplier', t1.invoice_desc as 'Description', t1.recieve_from as 'Recieve From', COUNT(t3.invoice_id) as 'Jml invoice', ".
                "t1.status, SUM(t3.amount) as 'Total Amount'".
                "FROM trx_invoice t1 INNER JOIN mst_supplier t2 ON t1.supplier_id=t2.id ".
                "INNER JOIN trx_invoice_detail t3 on t1.id=t3.invoice_id ". $where ." GROUP BY t1.id";
        
        //$count = "SELECT COUNT(*) FROM (" . $query . ") tt";
        //$totalCount = Yii::$app->db->createCommand($count)->queryScalar();
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('_excel', ['dataProvider' => $dataProvider]);
        }
        return $this->render('index', ['model'=>$model, 'dataProvider' => $dataProvider]);
    }

    public function actionProblem() {
        $model = new ReportForm();        
        $tglAwal =date('Y-m-01');
        $tglAkhir = date('Y-m-t');
        if ($model->load(Yii::$app->request->post())){
            $tglAwal = date('Y-m-d',strtotime($model->tgl_awal));
            $tglAkhir = date('Y-m-d',strtotime($model->tgl_akhir));
        }
        $where =" ms.created_at >='". $tglAwal."' AND ms.created_at <='". $tglAkhir ."' ";

        $query = "SELECT t.supplier_name as 'Supplier',
                COUNT(CASE WHEN t.category = 'KONTRAK' THEN 1 END) as 'CTR',
                COUNT(CASE WHEN t.category = 'FAKTUR_PAJAK' THEN 1 END) as 'FPJ',
                COUNT(CASE WHEN t.category = 'GRN' THEN 1 END) as 'GRN',
                COUNT(CASE WHEN t.category = 'INTERNAL' THEN 1 END) as 'INT',
                COUNT(CASE WHEN t.category = 'INVOICE' THEN 1 END) as 'INV',
                COUNT(CASE WHEN t.category = 'TRANSPORTER_RATE' THEN 1 END) as 'RAT',
                COUNT(CASE WHEN t.category = 'OTHERS' THEN 1 END) as 'OTH'
                FROM
                (
                    select su.supplier_name, ip.category from trx_invoice_problem ip
                    INNER JOIN trx_invoice ms ON ip.invoice_id= ms.id
                    INNER JOIN mst_supplier su ON ms.supplier_id=su.id
                    WHERE ". $where."
                ) t
                GROUP by t.supplier_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('problem', [
                'title'=>'Invoice Problem ',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }     
        
    }
}
