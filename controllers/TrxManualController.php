<?php

namespace app\controllers;

use Yii;
use app\models\TrxManual;
use app\models\search\TrxManualSeach;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxManualController implements the CRUD actions for TrxManual model.
 */
class TrxManualController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionProses($id){
        
        $model = $this->findModel($id);
        
        $role = Yii::$app->user->identity->role;
        $proses = \app\models\MstWorkflow::labelProses('DD-MANUAL', $role);
                
        if (Yii::$app->request->post() && TrxManual::updateWorkflow($id, $role)) {
            return $this->redirect(['index']);

        }
        return $this->renderPartial('_proses', ['model' => $model, 'proses' =>$proses], false);
    }
    
    public function actionReject($id)
    {
        $role = Yii::$app->user->identity->role;
        TrxManual::rejectWorkflow($id, $role);

        return $this->redirect(['index']);
    }

    /**
     * Lists all TrxManual models.
     * @return mixed
     */
    public function actionList()
    {
        $searchModel = new TrxManualSeach();
        $role = Yii::$app->user->identity->role;
        $dataProvider = $searchModel->searchByRole(Yii::$app->request->queryParams, $role); 

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndex()
    {
        $searchModel = new TrxManualSeach();
        $role = Yii::$app->user->identity->role;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $role); 

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrxManual model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderPartial('_view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDetail($id)
    {
        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrxManual model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $role = Yii::$app->user->identity->role;
        $model = new TrxManual();

        if(Yii::$app->request->post()){
            $model->setAttributes(Yii::$app->request->post());
            $model->created_at = date('Y-m-d H:i:s', strtotime($model->created_at));
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TrxManual::addWorkflow($model->id, $role);
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing TrxManual model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $role = Yii::$app->user->identity->role;
        $model = $this->findModel($id);       
        $model->created_at = date('Y-m-d', strtotime($model->created_at));
        
        if(Yii::$app->request->post()){
            $model->setAttributes(Yii::$app->request->post());
            $model->created_at = date('Y-m-d H:i:s', strtotime($model->created_at));
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success']   
            $model->addWorkflow($id,$role);
            return $this->redirect(['index']);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing TrxManual model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxManual model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxManual the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrxManual::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
