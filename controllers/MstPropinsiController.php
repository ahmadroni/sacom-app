<?php

namespace app\controllers;

use Yii;
use app\models\MstPropinsi;
use app\models\search\MstPropinsiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MstPropinsiController implements the CRUD actions for MstPropinsi model.
 */
class MstPropinsiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MstPropinsi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MstPropinsiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MstPropinsi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderPartial('_view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MstPropinsi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MstPropinsi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return $this->redirect(['view', 'id' => $model->id]);
            return ['result' => 'success'];
        }

        return $this->renderPartial('_form', ['model' => $model,], false);
    }

    /**
     * Updates an existing MstPropinsi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['result' => 'success'];
        }

        return $this->renderPartial('_form', ['model' => $model,], false);
    }

    /**
     * Deletes an existing MstPropinsi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MstPropinsi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MstPropinsi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MstPropinsi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
