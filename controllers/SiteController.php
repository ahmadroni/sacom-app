<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\form\Login;
use app\models\ContactForm;

use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout='//mainLogin';
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionProfile(){
        $userId = Yii::$app->user->id;
        $model = \app\models\MstUserProfile::findOne($userId);        
        $model->setScenario("profile");
        if ($model->load(Yii::$app->request->post()))
        {   
            $model->image = \yii\web\UploadedFile::getInstance($model, 'image');
             // open image
            $image = Image::getImagine()->open($model->image->tempName);

            // rendering information about crop of ONE option 
            $temp = Json::decode($model->crop_info)[0];
            $cropInfo = array();
            $cropInfo['dWidth'] = $temp['dWidth']; //new width image
            $cropInfo['dHeight'] = $temp['dHeight']; //new height image
            $cropInfo['x'] = abs($temp['x']); //begin position of frame crop by X
            $cropInfo['y'] = abs($temp['y']); //begin position of frame crop by Y
            $cropInfo['width'] = $temp['width']; //width of cropped image
            $cropInfo['height'] = $temp['height']; //height of cropped image
            // Properties bolow we don't use in this example
            //$cropInfo['ratio'] = $cropInfo['ratio'] == 0 ? 1.0 : (float)$cropInfo['ratio']; //ratio image. 

            //delete old images
            $oldImages = FileHelper::findFiles(Yii::getAlias('@webroot/profile'), [
                'only' => [
                    $model->user_id . '.*',
                    'user_' . $model->user_id . '.*',
                ], 
            ]);
            for ($i = 0; $i != count($oldImages); $i++) {
                @unlink($oldImages[$i]);
            }

            //saving thumbnail
            $newSizeThumb = new Box($cropInfo['dWidth'], $cropInfo['dHeight']);
            $cropSizeThumb = new Box($cropInfo['width'], $cropInfo['height']); //frame size of crop
            $cropPointThumb = new Point($cropInfo['x'], $cropInfo['y']);
            $pathThumbImage = Yii::getAlias('@webroot/profile') . '/user_' . $userId . '.' . $model->image->getExtension();  

            $image->resize($newSizeThumb)
                ->crop($cropPointThumb, $cropSizeThumb)
                ->save($pathThumbImage, ['quality' => 100]);
            $model->image_path = '/profile/user_'. $userId . '.' . $model->image->getExtension();
            if($model->save()){
                return $this->goHome();
            }else {
                 return $this->render('profile',['model' => $model, 'data'=>$cropInfo]);
            }
        }
        return $this->render('profile',['model' => $model]);
    }
}
