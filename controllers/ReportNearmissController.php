<?php

namespace app\controllers;

use Yii;
use app\models\form\ReportForm;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class ReportNearmissController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionByStatus() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
            SUM(jo.OPEN) as 'OPEN', 
            SUM(jo.IN_PROGRESS) as 'IN_PROGRESS', 
            SUM(jo.CLOSE) as 'CLOSE'
            FROM mst_location loc
            INNER JOIN (
                SELECT t.id, t.parent_id,
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT lc.id, lc.parent_id, ms.status
                    FROM mst_location lc 
                    LEFT JOIN trx_nearmiss ms on ms.location_id=lc.id 
                    WHERE lc.parent_id<>0 ". $where ." 
                ) t
                GROUP BY t.id
            ) jo ON jo.parent_id = loc.id
            GROUP BY loc.id
            UNION
            SELECT t.id, t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
            COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
            COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
            FROM
            (
                SELECT lc.id, lc.location_name, ms.status
                FROM mst_location lc 
                LEFT JOIN trx_nearmiss ms on ms.location_id=lc.id 
                WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."  
            ) t
            GROUP BY t.id";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Status',
                'link'=>'status-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }

    public function actionStatusByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                    COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                    COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, lc.location_name, ms.status
                        FROM mst_location lc 
                        INNER JOIN trx_nearmiss ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id."".$where ."                 
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Near Miss - by Status at Location :: '. $location->location_name,
                    'link'=>'status-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['status-by-user','id'=>$id]);
        }
    }

    public function actionStatusByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT up.user_id, up.full_name,ms.status
                    FROM mst_user_profile up
                    INNER JOIN trx_nearmiss ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id."".$where ."                 
                ) t
                GROUP BY t.user_id  ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - by Status at Location :: '. $location->location_name,
                'link'=>'by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionByInvolved() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
            SUM(jo.NearmissInv01) as 'C1 Contractors Person',
            SUM(jo.NearmissInv02) as 'C2 Contractors Person',
            SUM(jo.NearmissInv03) as 'C3 Contractors Person',
            SUM(jo.NearmissInv04) as 'Employees',
            SUM(jo.NearmissInv05) as 'Transporters',
            SUM(jo.NearmissInv06) as 'Visitor',
            SUM(jo.NearmissInv07) as 'Others'
            FROM mst_location loc
            INNER JOIN (
                SELECT t.id, t.parent_id,
                COUNT(CASE WHEN t.involved = 'NearmissInv01' THEN 1 END) as 'NearmissInv01',
                COUNT(CASE WHEN t.involved = 'NearmissInv02' THEN 1 END) as 'NearmissInv02',
                COUNT(CASE WHEN t.involved = 'NearmissInv03' THEN 1 END) as 'NearmissInv03',
                COUNT(CASE WHEN t.involved = 'NearmissInv04' THEN 1 END) as 'NearmissInv04',
                COUNT(CASE WHEN t.involved = 'NearmissInv05' THEN 1 END) as 'NearmissInv05',
                COUNT(CASE WHEN t.involved = 'NearmissInv06' THEN 1 END) as 'NearmissInv06',
                COUNT(CASE WHEN t.involved = 'NearmissInv07' THEN 1 END) as 'NearmissInv07'
                FROM
                (
                    SELECT lc.id, lc.parent_id, ms.involved
                    FROM mst_location lc 
                    LEFT JOIN trx_nearmiss ms on ms.location_id=lc.id 
                    WHERE lc.parent_id<>0 ". $where ." 
                ) t
                GROUP BY t.id
            ) jo ON jo.parent_id = loc.id
            GROUP BY loc.id
            UNION
            SELECT t.id, t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.involved = 'NearmissInv01' THEN 1 END) as 'C1 Contractors Person',
            COUNT(CASE WHEN t.involved = 'NearmissInv02' THEN 1 END) as 'C2 Contractors Person',
            COUNT(CASE WHEN t.involved = 'NearmissInv03' THEN 1 END) as 'C3 Contractors Person',
            COUNT(CASE WHEN t.involved = 'NearmissInv04' THEN 1 END) as 'Employees',
            COUNT(CASE WHEN t.involved = 'NearmissInv05' THEN 1 END) as 'Transporters',
            COUNT(CASE WHEN t.involved = 'NearmissInv06' THEN 1 END) as 'Visitor',
            COUNT(CASE WHEN t.involved = 'NearmissInv07' THEN 1 END) as 'Others'
            FROM
            (
                SELECT lc.id, lc.location_name, ms.involved
                FROM mst_location lc 
                LEFT JOIN trx_nearmiss ms on ms.location_id=lc.id 
                WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."  
            ) t
            GROUP BY t.id";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Involved',
                'link'=>'involved-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionInvolvedByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.involved = 'NearmissInv01' THEN 1 END) as 'C1 Contractors Person',
                    COUNT(CASE WHEN t.involved = 'NearmissInv02' THEN 1 END) as 'C2 Contractors Person',
                    COUNT(CASE WHEN t.involved = 'NearmissInv03' THEN 1 END) as 'C3 Contractors Person',
                    COUNT(CASE WHEN t.involved = 'NearmissInv04' THEN 1 END) as 'Employees',
                    COUNT(CASE WHEN t.involved = 'NearmissInv05' THEN 1 END) as 'Transporters',
                    COUNT(CASE WHEN t.involved = 'NearmissInv06' THEN 1 END) as 'Visitor',
                    COUNT(CASE WHEN t.involved = 'NearmissInv07' THEN 1 END) as 'Others'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, lc.location_name, ms.involved
                        FROM mst_location lc 
                        INNER JOIN trx_nearmiss ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id."".$where ."                 
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Near Miss - Involved at Location :: '. $location->location_name,
                    'link'=>'involved-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['involved-by-user','id'=>$id]);
        }
    }
    
    public function actionInvolvedByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.involved = 'NearmissInv01' THEN 1 END) as 'C1 Contractors Person',
                COUNT(CASE WHEN t.involved = 'NearmissInv02' THEN 1 END) as 'C2 Contractors Person',
                COUNT(CASE WHEN t.involved = 'NearmissInv03' THEN 1 END) as 'C3 Contractors Person',
                COUNT(CASE WHEN t.involved = 'NearmissInv04' THEN 1 END) as 'Employees',
                COUNT(CASE WHEN t.involved = 'NearmissInv05' THEN 1 END) as 'Transporters',
                COUNT(CASE WHEN t.involved = 'NearmissInv06' THEN 1 END) as 'Visitor',
                COUNT(CASE WHEN t.involved = 'NearmissInv07' THEN 1 END) as 'Others'
                FROM
                (
                    SELECT up.user_id, up.full_name,ms.involved
                    FROM mst_user_profile up
                    INNER JOIN trx_nearmiss ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id."".$where ."                 
                ) t
                GROUP BY t.user_id  ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Involved at Location :: '. $location->location_name,
                'link'=>'by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionByRisk() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
            SUM(jo.NearmissRisk01) as 'Catastropic',
            SUM(jo.NearmissRisk02) as 'Fatal',
            SUM(jo.NearmissRisk03) as 'Critical',
            SUM(jo.NearmissRisk04) as 'Marginal',
            SUM(jo.NearmissRisk05) as 'Negligible'
            FROM mst_location loc
            INNER JOIN (
                SELECT t.id, t.parent_id,
                COUNT(CASE WHEN t.risk = 'NearmissRisk01' THEN 1 END) as 'NearmissRisk01',
                COUNT(CASE WHEN t.risk = 'NearmissRisk02' THEN 1 END) as 'NearmissRisk02',
                COUNT(CASE WHEN t.risk = 'NearmissRisk03' THEN 1 END) as 'NearmissRisk03',
                COUNT(CASE WHEN t.risk = 'NearmissRisk04' THEN 1 END) as 'NearmissRisk04',
                COUNT(CASE WHEN t.risk = 'NearmissRisk05' THEN 1 END) as 'NearmissRisk05'
                FROM
                (
                    SELECT lc.id, lc.parent_id, ms.risk
                    FROM mst_location lc 
                    INNER JOIN trx_nearmiss ms ON ms.location_id=lc.id 
                    WHERE lc.parent_id<>0 ". $where ."
                ) t
                GROUP BY t.id
            ) jo ON jo.parent_id = loc.id
            GROUP BY loc.id
            UNION
            SELECT t.id, t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.risk = 'NearmissRisk01' THEN 1 END) as 'Catastropic',
            COUNT(CASE WHEN t.risk = 'NearmissRisk02' THEN 1 END) as 'Fatal',
            COUNT(CASE WHEN t.risk = 'NearmissRisk03' THEN 1 END) as 'Critical',
            COUNT(CASE WHEN t.risk = 'NearmissRisk04' THEN 1 END) as 'Marginal',
            COUNT(CASE WHEN t.risk = 'NearmissRisk05' THEN 1 END) as 'Negligible'
            FROM
            (
                SELECT lc.id, lc.location_name, ms.risk
                FROM mst_location lc 
                INNER JOIN trx_nearmiss ms ON ms.location_id=lc.id 
                WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."                               
            ) t
            GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Risk', 
                'link'=>'risk-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }   
    }
    
    public function actionRiskByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.risk = 'NearmissRisk01' THEN 1 END) as 'Catastropic',
                    COUNT(CASE WHEN t.risk = 'NearmissRisk02' THEN 1 END) as 'Fatal',
                    COUNT(CASE WHEN t.risk = 'NearmissRisk03' THEN 1 END) as 'Critical',
                    COUNT(CASE WHEN t.risk = 'NearmissRisk04' THEN 1 END) as 'Marginal',
                    COUNT(CASE WHEN t.risk = 'NearmissRisk05' THEN 1 END) as 'Negligible'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, lc.location_name, ms.risk
                        FROM mst_location lc 
                        INNER JOIN trx_nearmiss ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id."".$where ."                 
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Near Miss - Risk at Location :: '. $location->location_name,
                    'link'=>'risk-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['involved-by-user','id'=>$id]);
        }
    }
    
    public function actionRiskByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.risk = 'NearmissRisk01' THEN 1 END) as 'Catastropic',
                COUNT(CASE WHEN t.risk = 'NearmissRisk02' THEN 1 END) as 'Fatal',
                COUNT(CASE WHEN t.risk = 'NearmissRisk03' THEN 1 END) as 'Critical',
                COUNT(CASE WHEN t.risk = 'NearmissRisk04' THEN 1 END) as 'Marginal',
                COUNT(CASE WHEN t.risk = 'NearmissRisk05' THEN 1 END) as 'Negligible'
                FROM
                (
                    SELECT up.user_id, up.full_name,ms.risk
                    FROM mst_user_profile up
                    INNER JOIN trx_nearmiss ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id."".$where ."                 
                ) t
                GROUP BY t.user_id  ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Risk at Location :: '. $location->location_name,
                'link'=>'by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }

    public function actionByFollowUp() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.NearmissType01) as 'Bridges Safety Rule',
                SUM(jo.NearmissType02) as 'Environtment Issue',
                SUM(jo.NearmissType03) as 'Guarding Not In Place',
                SUM(jo.NearmissType04) as 'Near Hit',
                SUM(jo.NearmissType05) as 'Not Follow Safety Rule',
                SUM(jo.NearmissType06) as 'Not Wearing Safety PPE',
                SUM(jo.NearmissType07) as 'Poperty Damaged',
                SUM(jo.NearmissType08) as 'Security Concern',
                SUM(jo.NearmissType09) as 'Short Cut',
                SUM(jo.NearmissType10) as 'Spilages',
                SUM(jo.NearmissType11) as 'Vehicle And Forklift',
                SUM(jo.NearmissType12) as 'Any Other'
                FROM mst_location loc
                INNER JOIN (
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.types = 'NearmissType01' THEN 1 END) as 'NearmissType01',
                    COUNT(CASE WHEN t.types = 'NearmissType02' THEN 1 END) as 'NearmissType02',
                    COUNT(CASE WHEN t.types = 'NearmissType03' THEN 1 END) as 'NearmissType03',
                    COUNT(CASE WHEN t.types = 'NearmissType04' THEN 1 END) as 'NearmissType04',
                    COUNT(CASE WHEN t.types = 'NearmissType05' THEN 1 END) as 'NearmissType05',
                    COUNT(CASE WHEN t.types = 'NearmissType06' THEN 1 END) as 'NearmissType06',
                    COUNT(CASE WHEN t.types = 'NearmissType07' THEN 1 END) as 'NearmissType07',
                    COUNT(CASE WHEN t.types = 'NearmissType08' THEN 1 END) as 'NearmissType08',
                    COUNT(CASE WHEN t.types = 'NearmissType09' THEN 1 END) as 'NearmissType09',
                    COUNT(CASE WHEN t.types = 'NearmissType10' THEN 1 END) as 'NearmissType10',
                    COUNT(CASE WHEN t.types = 'NearmissType11' THEN 1 END) as 'NearmissType11',
                    COUNT(CASE WHEN t.types = 'NearmissType12' THEN 1 END) as 'NearmissType12'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, ms.types
                        FROM mst_location lc 
                        INNER JOIN trx_nearmiss ms ON lc.id = ms.location_id
                        WHERE lc.parent_id<>0 ". $where ."                
                    ) t
                    GROUP BY t.id
                ) jo ON jo.parent_id = loc.id
                GROUP BY loc.id
                UNION
                SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.types = 'NearmissType01' THEN 1 END) as 'Bridges Safety Rule',
                COUNT(CASE WHEN t.types = 'NearmissType02' THEN 1 END) as 'Environtment Issue',
                COUNT(CASE WHEN t.types = 'NearmissType03' THEN 1 END) as 'Guarding Not In Place',
                COUNT(CASE WHEN t.types = 'NearmissType04' THEN 1 END) as 'Near Hit',
                COUNT(CASE WHEN t.types = 'NearmissType05' THEN 1 END) as 'Not Follow Safety Rule',
                COUNT(CASE WHEN t.types = 'NearmissType06' THEN 1 END) as 'Not Wearing Safety PPE',
                COUNT(CASE WHEN t.types = 'NearmissType07' THEN 1 END) as 'Poperty Damaged',
                COUNT(CASE WHEN t.types = 'NearmissType08' THEN 1 END) as 'Security Concern',
                COUNT(CASE WHEN t.types = 'NearmissType09' THEN 1 END) as 'Short Cut',
                COUNT(CASE WHEN t.types = 'NearmissType10' THEN 1 END) as 'Spilages',
                COUNT(CASE WHEN t.types = 'NearmissType11' THEN 1 END) as 'Vehicle And Forklift',
                COUNT(CASE WHEN t.types = 'NearmissType12' THEN 1 END) as 'Any Other'
                FROM
                (
                    SELECT lc.id, lc.parent_id, lc.location_name, ms.types
                    FROM mst_location lc 
                    INNER JOIN trx_nearmiss ms ON lc.id = ms.location_id
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."                 
                ) t
                GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Follow Up',
                'link'=>'follow-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }         
    }
    
    public function actionFollowByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.types = 'NearmissType01' THEN 1 END) as 'Bridges Safety Rule',
                    COUNT(CASE WHEN t.types = 'NearmissType02' THEN 1 END) as 'Environtment Issue',
                    COUNT(CASE WHEN t.types = 'NearmissType03' THEN 1 END) as 'Guarding Not In Place',
                    COUNT(CASE WHEN t.types = 'NearmissType04' THEN 1 END) as 'Near Hit',
                    COUNT(CASE WHEN t.types = 'NearmissType05' THEN 1 END) as 'Not Follow Safety Rule',
                    COUNT(CASE WHEN t.types = 'NearmissType06' THEN 1 END) as 'Not Wearing Safety PPE',
                    COUNT(CASE WHEN t.types = 'NearmissType07' THEN 1 END) as 'Poperty Damaged',
                    COUNT(CASE WHEN t.types = 'NearmissType08' THEN 1 END) as 'Security Concern',
                    COUNT(CASE WHEN t.types = 'NearmissType09' THEN 1 END) as 'Short Cut',
                    COUNT(CASE WHEN t.types = 'NearmissType10' THEN 1 END) as 'Spilages',
                    COUNT(CASE WHEN t.types = 'NearmissType11' THEN 1 END) as 'Vehicle And Forklift',
                    COUNT(CASE WHEN t.types = 'NearmissType12' THEN 1 END) as 'Any Other'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, lc.location_name, ms.types
                        FROM mst_location lc 
                        INNER JOIN trx_nearmiss ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id."".$where ."                 
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Near Miss - Follow Up at Location :: '. $location->location_name,
                    'link'=>'follow-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['follow-by-user','id'=>$id]);
        }
    }
    
    public function actionFollowByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.types = 'NearmissType01' THEN 1 END) as 'Bridges Safety Rule',
                COUNT(CASE WHEN t.types = 'NearmissType02' THEN 1 END) as 'Environtment Issue',
                COUNT(CASE WHEN t.types = 'NearmissType03' THEN 1 END) as 'Guarding Not In Place',
                COUNT(CASE WHEN t.types = 'NearmissType04' THEN 1 END) as 'Near Hit',
                COUNT(CASE WHEN t.types = 'NearmissType05' THEN 1 END) as 'Not Follow Safety Rule',
                COUNT(CASE WHEN t.types = 'NearmissType06' THEN 1 END) as 'Not Wearing Safety PPE',
                COUNT(CASE WHEN t.types = 'NearmissType07' THEN 1 END) as 'Poperty Damaged',
                COUNT(CASE WHEN t.types = 'NearmissType08' THEN 1 END) as 'Security Concern',
                COUNT(CASE WHEN t.types = 'NearmissType09' THEN 1 END) as 'Short Cut',
                COUNT(CASE WHEN t.types = 'NearmissType10' THEN 1 END) as 'Spilages',
                COUNT(CASE WHEN t.types = 'NearmissType11' THEN 1 END) as 'Vehicle And Forklift',
                COUNT(CASE WHEN t.types = 'NearmissType12' THEN 1 END) as 'Any Other'
                FROM
                (
                    SELECT up.user_id, up.full_name,ms.types
                    FROM mst_user_profile up
                    INNER JOIN trx_nearmiss ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id."".$where ."                 
                ) t
                GROUP BY t.user_id  ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Near Miss - Follow Up at Location :: '. $location->location_name,
                'link'=>'by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionByPost($id, $locId) {
        $model = new ReportForm();
        $where =" ";
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $query = "SELECT ms.id, ms.created_at as 'Date', ms.title as 'Title'
                FROM trx_nearmiss ms
                WHERE ms.location_id=".$locId." AND ms.created_by=".$id."".$where ."  ";
        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('by-post', [
            'title'=>'Near Miss By User :: '. $user->full_name,
            'model'=>$model, 'dataProvider' => $dataProvider]);
    }
}
