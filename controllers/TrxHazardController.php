<?php

namespace app\controllers;

use Yii;
use app\models\TrxHazard;
use app\models\TrxHazardDetail;
use app\models\TrxHazardComment;
use app\models\search\TrxHazardSearch;
use app\models\form\TrxHazardForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxHazardController implements the CRUD actions for TrxHazard model.
 */
class TrxHazardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrxHazard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrxHazardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionList()
    {
        $searchModel = new TrxHazardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionClosing($id) {
        $model = New TrxHazardComment();
        $model->loadDefaultValues();
        $model->hazard_id = $id;
        $model->status = TrxHazard::CLOSE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $parent = TrxHazard::findOne($id);
            if ($parent !== null) {
                $parent->status = TrxHazard::CLOSE;
                $parent->update();
            }
            return $this->redirect(['index']);
        }
        return $this->render('closing', [
                    'model' => $model,
        ]);
    }
    
    public function actionResponse($id) {
        $userId = Yii::$app->user->identity->id;
        $model = $this->findModel($id);
        $detail = $this->findDetail($id, $userId);

        $comment = New TrxHazardComment();
        if ($comment->load(Yii::$app->request->post()) && $comment->save()) {
            $this->updateStatus($detail->id, $comment->status);

            return $this->redirect(['index']);
        }

        return $this->render('response', [
                    'model' => $model,
                    'detail' => $detail,
        ]);
    }

    /**
     * Displays a single TrxHazard model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetail($id)
    {
        return $this->render('detail', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionView($id)
    {
        return $this->renderPartial('_view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrxHazard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrxHazardForm();
        $model->trxHazard = new TrxHazard;
        $model->trxHazard->loadDefaultValues();        
        $model->setAttributes(Yii::$app->request->post());
        
        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            if($model->trxHazard->risk_level=='TINGGI'){
                $model->trxHazard->sendMail();
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing TrxHazard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = new TrxHazardForm();
        $model->trxHazard = $this->findModel($id);
        $model->setAttributes(Yii::$app->request->post());
       
        if (Yii::$app->request->post() && $model->save()) {
            return $this->redirect(['index']);
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing TrxHazard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxHazard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxHazard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function updateStatus($id, $status) {
        $model = TrxHazardDetail::findOne($id);
        if ($model !== null) {
            $model->status = $status;
            $model->update();
        }
    }

    protected function findModel($id) {
        if (($model = TrxHazard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findDetail($id, $userId) {
        if (($model =TrxHazardDetail::find()->where(['hazard_id' => $id, 'for_user' => $userId])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
