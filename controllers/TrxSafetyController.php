<?php

namespace app\controllers;

use Yii;
use app\models\form\TrxConversationForm;
use app\models\TrxConversation;
use app\models\TrxConversationDetail;
use app\models\search\TrxConversationSearch;
use app\models\TrxConversationComment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrxConversationController implements the CRUD actions for TrxConversation model.
 */
class TrxSafetyController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrxConversation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TrxConversationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all TrxConversation models.
     * @return mixed
     */
    public function actionList() {
        $searchModel = new TrxConversationSearch();
        $dataProvider = $searchModel->searchByUser(Yii::$app->request->queryParams);

        return $this->render('list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrxConversation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->renderPartial('_view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionClosing($id) {
        $model = New TrxConversationComment();
        $model->loadDefaultValues();
        $model->conversation_id = $id;
        $model->status = TrxConversation::CLOSE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $parent = TrxConversation::findOne($id);
            if ($parent !== null) {
                $parent->status = TrxConversation::CLOSE;
                $parent->update();
            }
            return $this->redirect(['index']);
        }
        return $this->render('closing', [
                    'model' => $model,
        ]);
    }

    public function actionDetail($id) {
        $comment = New TrxConversationComment();

        $userId = Yii::$app->user->identity->id;
        //$detail = $this->findDetail($id, $userId);

        if ($comment->load(Yii::$app->request->post()) && $comment->save()) {
            //$this->updateStatus($detail->id, $comment->status);
            $this->refresh();
        }
        return $this->render('_detail', [
                    'model' => $this->findModel($id),
                    'comment' => $comment,
        ]);
    }

    public function actionResponse($id) {
        $userId = Yii::$app->user->identity->id;
        $model = $this->findModel($id);
        $detail = $this->findDetail($id, $userId);

        $comment = New TrxConversationComment();
        if ($comment->load(Yii::$app->request->post()) && $comment->save()) {
            $this->updateStatus($detail->id, $comment->status);

            return $this->redirect(['index']);
        }

        return $this->render('response', [
                    'model' => $model,
                    'detail' => $detail,
        ]);
    }

    /**
     * Creates a new TrxConversation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new TrxConversationForm();
        $model->trxConversation = new TrxConversation;
        $model->trxConversation->loadDefaultValues();
        $model->setAttributes(Yii::$app->request->post());
        
        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            if($model->trxConversation->risk_level=='TINGGI'){
                $model->trxConversation->sendEmail();
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing TrxConversation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = new TrxConversationForm();
        $model->trxConversation = $this->findModel($id);
        $model->setAttributes(Yii::$app->request->post());
        
        if (Yii::$app->request->post() && $model->save()) {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return ['result' => 'success'];
            return $this->redirect(['index']);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing TrxConversation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrxConversation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrxConversation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function updateStatus($id, $status) {
        $model = TrxConversationDetail::findOne($id);
        if ($model !== null) {
            $model->status = $status;
            $model->update();
        }
    }

    protected function findModel($id) {
        if (($model = TrxConversation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findDetail($id, $userId) {
        if (($model = \app\models\TrxConversationDetail::find()->where(['conversation_id' => $id, 'for_user' => $userId])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
