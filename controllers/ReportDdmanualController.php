<?php

namespace app\controllers;

use Yii;
use app\models\form\ReportForm;
use app\models\TrxManual;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class ReportDdmanualController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionCountsum() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" WHERE t2.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND t2.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $query = "SELECT ms.location_name, ".
                "COUNT(CASE WHEN ms.reason='ManualReason01' THEN 1 END) as 'COUNT - Asset Sales', ".
                "SUM(if(ms.reason = 'ManualReason01', ms.quantity, 0)) as 'SUM - Asset Sales', ".
                "COUNT(CASE WHEN ms.reason='ManualReason02' THEN 1 END) as 'COUNT - JDE System Down', ".
                "SUM(if(ms.reason = 'ManualReason02', ms.quantity, 0)) as 'SUM - JDE System Down', ".
                "COUNT(CASE WHEN ms.reason='ManualReason03' THEN 1 END) as 'COUNT - Scrapt Sales', ".
                "SUM(if(ms.reason = 'ManualReason03', ms.quantity, 0)) as 'SUM - Scrapt Sales', ".
                "COUNT(CASE WHEN ms.reason='ManualReason04' THEN 1 END) as 'COUNT - Waste Landfilled', ".
                "SUM(if(ms.reason = 'ManualReason04', ms.quantity, 0)) as 'SUM - Waste Landfilled', ".
                "COUNT(CASE WHEN ms.reason='ManualReason05' THEN 1 END) as 'COUNT - Third Parties Process', ".
                "SUM(if(ms.reason = 'ManualReason05', ms.quantity, 0)) as 'SUM - Third Parties Process', ".
                "COUNT(CASE WHEN ms.reason='ManualReason06' THEN 1 END) as 'COUNT - Others', ".
                "SUM(if(ms.reason = 'ManualReason06', ms.quantity, 0)) as 'SUM - Others' ".
                "FROM ( ".
                "SELECT t1.location_name, t2.reason, t2.quantity ".
                "FROM mst_location t1 ".
                "LEFT JOIN trx_manual t2 on t1.id=t2.location_id ". $where .                
                ") ms ".
                "GROUP BY ms.location_name ";
        
        $count = "SELECT COUNT(*) FROM (" . $query . ") tt";
        $totalCount = Yii::$app->db->createCommand($count)->queryScalar();
        
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 100
            ],
            'sort' => [
                'attributes' => [
                    'location_name',
                ]
            ]
        ]);
        
        if(Yii::$app->request->post('search')=='export'){
            $columns = ['location_name','COUNT - Asset Sales','SUM - Asset Sales',
                'COUNT - JDE System Down','SUM - JDE System Down',
                'COUNT - Scrapt Sales','SUM - Scrapt Sales',
                'COUNT - Waste Landfilled','SUM - Waste Landfilled',
                'COUNT - Third Parties Process','SUM - Third Parties Process',
                'COUNT - Others','SUM - Others'
            ];
            return $this->renderPartial('//report/_excel', ['columns'=>$columns, 'dataProvider' => $dataProvider]);
        }
        return $this->render('count-sum', ['model'=>$model, 'dataProvider' => $dataProvider]);
    }

    public function actionByStatus() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
            SUM(jo.OPEN) as 'OPEN', 
            SUM(jo.IN_PROGRESS) as 'IN_PROGRESS', 
            SUM(jo.CLOSE) as 'CLOSE'
            FROM mst_location loc
            INNER JOIN (
                SELECT t.id, t.parent_id,
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT lc.id, lc.parent_id, ms.status
                    FROM mst_location lc 
                    LEFT JOIN trx_manual ms on ms.location_id=lc.id 
                    WHERE lc.parent_id<>0 ". $where ." 
                ) t
                GROUP BY t.id
            ) jo ON jo.parent_id = loc.id
            GROUP BY loc.id
            UNION
            SELECT t.id, t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
            COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
            COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
            FROM
            (
                SELECT lc.id, lc.location_name, ms.status
                FROM mst_location lc 
                LEFT JOIN trx_manual ms on ms.location_id=lc.id 
                WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."  
            ) t
            GROUP BY t.id";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual - Status',
                'link'=>'status-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }

    public function actionStatusByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                    COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                    COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, lc.location_name, ms.status
                        FROM mst_location lc 
                        INNER JOIN trx_manual ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id."".$where ."                 
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'DD Manual - by Status at Location :: '. $location->location_name,
                    'link'=>'status-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['status-by-user','id'=>$id]);
        }
    }

    public function actionStatusByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT up.user_id, up.full_name,ms.status
                    FROM mst_user_profile up
                    INNER JOIN trx_manual ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id."".$where ."                 
                ) t
                GROUP BY t.user_id  ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual - by Status at Location :: '. $location->location_name,
                'link'=>'by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }

    public function actionByPost($id, $locId) {
        $model = new ReportForm();
        $where =" ";
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $query = "SELECT ms.id, ms.created_at as 'Date', ms.number as 'Number', ms.description as 'Title',
                t1.name as 'REASON',t2.name as 'Category',t3.name as 'Gold Category',ms.quantity as 'Quantity',ms.unit as 'Unit', ms.amount as 'Amount'
                FROM trx_manual ms
                INNER JOIN mst_lookup as t1 ON ms.reason=t1.code
                INNER JOIN mst_lookup as t2 ON ms.category=t2.code
                INNER JOIN mst_lookup as t3 ON ms.gold_category=t3.code
                WHERE ms.location_id=".$locId." AND ms.created_by=".$id."".$where ."  ";
        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        return $this->render('by-post', [
            'title'=>'DD Manual By User :: '. $user->full_name,
            'model'=>$model, 'dataProvider' => $dataProvider]);
    }

    public function actionByReason() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
            SUM(jo.ManualReason01) as 'Asset Sales',
            SUM(jo.ManualReason02) as 'JDE System Down',
            SUM(jo.ManualReason03) as 'Scrapt Sales',
            SUM(jo.ManualReason04) as 'Waste Landfilled',
            SUM(jo.ManualReason05) as 'Third Parties Process',
            SUM(jo.ManualReason06) as 'Others'
            FROM mst_location loc
            INNER JOIN (
                SELECT t.id, t.parent_id,
                COUNT(CASE WHEN t.reason = 'ManualReason01' THEN 1 END) as 'ManualReason01',
                COUNT(CASE WHEN t.reason = 'ManualReason02' THEN 1 END) as 'ManualReason02',
                COUNT(CASE WHEN t.reason = 'ManualReason03' THEN 1 END) as 'ManualReason03',
                COUNT(CASE WHEN t.reason = 'ManualReason04' THEN 1 END) as 'ManualReason04',
                COUNT(CASE WHEN t.reason = 'ManualReason05' THEN 1 END) as 'ManualReason05',
                COUNT(CASE WHEN t.reason = 'ManualReason06' THEN 1 END) as 'ManualReason06'
                FROM
                (
                    SELECT lc.id, lc.parent_id, ms.reason
                    FROM mst_location lc 
                    LEFT JOIN trx_manual ms on ms.location_id=lc.id 
                    WHERE lc.parent_id<>0 ". $where ." 
                ) t
                GROUP BY t.id
            ) jo ON jo.parent_id = loc.id
            GROUP BY loc.id
            UNION
            SELECT t.id, t.location_name as 'Nama Lokasi',
            COUNT(CASE WHEN t.reason = 'ManualReason01' THEN 1 END) as 'Asset Sales',
            COUNT(CASE WHEN t.reason = 'ManualReason02' THEN 1 END) as 'JDE System Down',
            COUNT(CASE WHEN t.reason = 'ManualReason03' THEN 1 END) as 'Scrapt Sales',
            COUNT(CASE WHEN t.reason = 'ManualReason04' THEN 1 END) as 'Waste Landfilled',
            COUNT(CASE WHEN t.reason = 'ManualReason05' THEN 1 END) as 'Third Parties Process',
            COUNT(CASE WHEN t.reason = 'ManualReason06' THEN 1 END) as 'Others'
            FROM
            (
                SELECT lc.id, lc.location_name, ms.reason
                FROM mst_location lc 
                LEFT JOIN trx_manual ms on ms.location_id=lc.id 
                WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."  
            ) t
            GROUP BY t.id";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual by Reason',
                'link'=>'reason-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }

    public function actionReasonByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.reason = 'ManualReason01' THEN 1 END) as 'Asset Sales',
                    COUNT(CASE WHEN t.reason = 'ManualReason02' THEN 1 END) as 'JDE System Down',
                    COUNT(CASE WHEN t.reason = 'ManualReason03' THEN 1 END) as 'Scrapt Sales'
                    COUNT(CASE WHEN t.reason = 'ManualReason04' THEN 1 END) as 'Waste Landfilled'
                    COUNT(CASE WHEN t.reason = 'ManualReason05' THEN 1 END) as 'Third Parties Process'
                    COUNT(CASE WHEN t.reason = 'ManualReason06' THEN 1 END) as 'Others'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, lc.location_name, ms.reason
                        FROM mst_location lc 
                        INNER JOIN trx_manual ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id."".$where ."                 
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'DD Manual - by Reason at Location :: '. $location->location_name,
                    'link'=>'reason-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['reason-by-user','id'=>$id]);
        }
    }

    public function actionReasonByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.reason = 'ManualReason01' THEN 1 END) as 'Asset Sales',
                COUNT(CASE WHEN t.reason = 'ManualReason02' THEN 1 END) as 'JDE System Down',
                COUNT(CASE WHEN t.reason = 'ManualReason03' THEN 1 END) as 'Scrapt Sales'
                COUNT(CASE WHEN t.reason = 'ManualReason04' THEN 1 END) as 'Waste Landfilled'
                COUNT(CASE WHEN t.reason = 'ManualReason05' THEN 1 END) as 'Third Parties Process'
                COUNT(CASE WHEN t.reason = 'ManualReason06' THEN 1 END) as 'Others'
                FROM
                (
                    SELECT up.user_id, up.full_name,ms.reason
                    FROM mst_user_profile up
                    INNER JOIN trx_manual ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id."".$where ."                 
                ) t
                GROUP BY t.user_id  ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual - by Reason at Location :: '. $location->location_name,
                'link'=>'by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionManualReason() {
        $model = new ReportForm();
        $tglAwal =date('Y-m-01');
        $tglAkhir = date('Y-m-t');
        if ($model->load(Yii::$app->request->post())){
            $tglAwal = date('Y-m-d',strtotime($model->tgl_awal));
            $tglAkhir = date('Y-m-d',strtotime($model->tgl_akhir));
        }
        $where =" ms.created_at >='". $tglAwal."' AND ms.created_at <='". $tglAkhir ."' ";

        
        $query = "SELECT lk.code as id, lk.name as 'Reason', 
        t.CCILEGON as 'Count of Cilegon', 
        t.SCILEGON as 'Sum of Quantity at Cilegon',
        t.CGERSIK as 'Count of Gersik', 
        t.SGERSIK as 'Sum of Quantity at Gersik'
        FROM mst_lookup lk
        LEFT JOIN (
        SELECT ms.reason,
        (COUNT(CASE WHEN ms.location_id=2 THEN 1 END) + COUNT(CASE WHEN ms.location_id=4 THEN 1 END))  as 'CCILEGON',
        (SUM(IF(ms.location_id = 2, ms.quantity, 0)) + SUM(IF(ms.location_id = 4, ms.quantity, 0))) as 'SCILEGON',
        COUNT(CASE WHEN ms.location_id=3 THEN 1 END) as 'CGERSIK',
        SUM(IF(ms.location_id = 3, ms.quantity, 0)) as 'SGERSIK'
        FROM trx_manual ms
        WHERE ". $where ."
        GROUP BY ms.reason
        ) t ON t.reason = lk.code
        WHERE lk.type='ManualReason'";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual by Reason',
                'link'=>'manual-category',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }
    }

    public function actionManualCategory($id) {
        $model = new ReportForm();
        $tglAwal =date('Y-m-01');
        $tglAkhir = date('Y-m-t');
        if ($model->load(Yii::$app->request->post())){
            $tglAwal = date('Y-m-d',strtotime($model->tgl_awal));
            $tglAkhir = date('Y-m-d',strtotime($model->tgl_akhir));
        }
        $where ="AND ms.created_at >='". $tglAwal."' AND ms.created_at <='". $tglAkhir ."' ";

        
        $query = "SELECT lk.code as id, lk.name as 'Category', 
        t.CCILEGON as 'Count of Cilegon', 
        t.SCILEGON as 'Sum of Quantity at Cilegon',
        t.CGERSIK as 'Count of Gersik', 
        t.SGERSIK as 'Sum of Quantity at Gersik'
        FROM mst_lookup lk
        LEFT JOIN (
        SELECT ms.category,
        (COUNT(CASE WHEN ms.location_id=2 THEN 1 END) + COUNT(CASE WHEN ms.location_id=4 THEN 1 END))  as 'CCILEGON',
        (SUM(IF(ms.location_id = 2, ms.quantity, 0)) + SUM(IF(ms.location_id = 4, ms.quantity, 0))) as 'SCILEGON',
        COUNT(CASE WHEN ms.location_id=3 THEN 1 END) as 'CGERSIK',
        SUM(IF(ms.location_id = 3, ms.quantity, 0)) as 'SGERSIK'
        FROM trx_manual ms
        WHERE ms.reason='". $id ."' ". $where ."
        GROUP BY ms.category
        ) t ON t.category = lk.code
        WHERE lk.type='". $id ."'";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual by Category',
                'link'=>'manual-gold-category',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }
    }

    public function actionManualGoldCategory($id) {
        $model = new ReportForm();
        $tglAwal =date('Y-m-01');
        $tglAkhir = date('Y-m-t');
        if ($model->load(Yii::$app->request->post())){
            $tglAwal = date('Y-m-d',strtotime($model->tgl_awal));
            $tglAkhir = date('Y-m-d',strtotime($model->tgl_akhir));
        }
        $where ="AND ms.created_at >='". $tglAwal."' AND ms.created_at <='". $tglAkhir ."' ";

        
        $query = "SELECT lk.code as id, lk.name as 'Gold Category', 
        t.CCILEGON as 'Count of Cilegon', 
        t.SCILEGON as 'Sum of Quantity at Cilegon',
        t.CGERSIK as 'Count Of Gersik', 
        t.SGERSIK as 'Sum of Quantity at Gersik'
        FROM mst_lookup lk
        LEFT JOIN (
        SELECT ms.gold_category,
        (COUNT(CASE WHEN ms.location_id=2 THEN 1 END) + COUNT(CASE WHEN ms.location_id=4 THEN 1 END))  as 'CCILEGON',
        (SUM(IF(ms.location_id = 2, ms.quantity, 0)) + SUM(IF(ms.location_id = 4, ms.quantity, 0))) as 'SCILEGON',
        COUNT(CASE WHEN ms.location_id=3 THEN 1 END) as 'CGERSIK',
        SUM(IF(ms.location_id = 3, ms.quantity, 0)) as 'SGERSIK'
        FROM trx_manual ms
        WHERE ms.category='". $id ."' ". $where ."
        GROUP BY ms.gold_category
        ) t ON t.gold_category = lk.code
        WHERE lk.type='". $id ."'";        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'DD Manual by Gold Category',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }
    }
}
