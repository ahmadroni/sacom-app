<?php

namespace app\controllers;

use Yii;
use app\models\form\ReportForm;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class ReportHazardController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionByFollowUp() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.Besar) as 'Besar', SUM(jo.Mungkin) as 'Mungkin', SUM(jo.Kecil) as 'Kecil'
                FROM mst_location loc
                INNER JOIN (
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.posibility = 'HazardPosibility01' THEN 1 END) as 'Besar',
                    COUNT(CASE WHEN t.posibility = 'HazardPosibility02' THEN 1 END) as 'Mungkin',
                    COUNT(CASE WHEN t.posibility = 'HazardPosibility03' THEN 1 END) as 'Kecil'
                    FROM
                    (
                        SELECT lc.id,lc.parent_id, ms.posibility
                        FROM mst_location lc
                        INNER JOIN trx_hazard ms ON lc.id = ms.location_id
                        WHERE lc.parent_id<>0 ". $where ."         
                    ) t
                    GROUP BY t.id
                ) jo ON loc.id = jo.parent_id
                GROUP BY loc.id
                UNION
                SELECT t.id,t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility01' THEN 1 END) as 'Besar',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility02' THEN 1 END) as 'Mungkin',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility03' THEN 1 END) as 'Kecil'
                FROM
                (
                    SELECT lc.id, lc.location_name,lc.parent_id, ms.posibility
                    FROM mst_location lc
                    INNER JOIN trx_hazard ms ON lc.id = ms.location_id
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0 ". $where ."         
                ) t
                GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Hazard By Follow Up',
                'link' => 'follow-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionFollowByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id,t.location_name as 'Nama Lokasi',
                    COUNT(CASE WHEN t.posibility = 'HazardPosibility01' THEN 1 END) as 'Besar',
                    COUNT(CASE WHEN t.posibility = 'HazardPosibility02' THEN 1 END) as 'Mungkin',
                    COUNT(CASE WHEN t.posibility = 'HazardPosibility03' THEN 1 END) as 'Kecil'
                    FROM
                    (
                        SELECT lc.id, lc.location_name,lc.parent_id, ms.posibility
                        FROM mst_location lc
                        INNER JOIN trx_hazard ms ON lc.id = ms.location_id
                        WHERE lc.parent_id=".$id . $where ."         
                    ) t
                    GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Hazard Follow Up Location :: '.$location->location_name, 
                    'link' => 'follow-by-user',
                    'model'=>$model, 'dataProvider' => $dataProvider]);
            } 
        }else {
            return $this->redirect(['follow-by-user','id'=>$id]);
        }
    }
    
    public function actionFollowByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id,t.full_name as 'Nama User',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility01' THEN 1 END) as 'Besar',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility02' THEN 1 END) as 'Mungkin',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility03' THEN 1 END) as 'Kecil'
                FROM
                (
                    SELECT up.user_id, up.full_name, ms.posibility
                    FROM mst_user_profile up
                    INNER JOIN trx_hazard ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id . $where ."         
                ) t
                GROUP BY t.user_id ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Hazard Follow Up Location :: '.$location->location_name, 
                'link' => 'follow-by-post',
                'locId'=>$id,
                'model'=>$model, 'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionFollowByPost($id, $locId) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND t.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND t.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT t.id, t.created_at as 'Date', t.title,
                COUNT(CASE WHEN t.posibility = 'HazardPosibility01' THEN 1 END) as 'Besar',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility02' THEN 1 END) as 'Mungkin',
                COUNT(CASE WHEN t.posibility = 'HazardPosibility03' THEN 1 END) as 'Kecil',
                COUNT(cm.hazard_id) as 'FU'
                FROM trx_hazard t
                LEFT JOIN trx_hazard_comment cm ON cm.hazard_id=t.id
                WHERE t.created_by=".$id ." AND t.location_id=".$locId. $where ." 
                GROUP BY t.id 
                ORDER BY t.created_at DESC";
        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        return $this->render('by-post', ['title'=>'Hazard Follow Up By :: '. $user->full_name, 'model'=>$model, 'dataProvider' => $dataProvider]);
    }
    
    public function actionByCategory() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.HazardRisk01) as 'Cedera Parah', 
                SUM(jo.HazardRisk02) as 'Cedera Sedang', 
                SUM(jo.HazardRisk03) as 'Cedera Ringan'
                FROM mst_location loc
                INNER JOIN (
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.risk = 'HazardRisk01' THEN 1 END) as 'HazardRisk01',
                    COUNT(CASE WHEN t.risk = 'HazardRisk02' THEN 1 END) as 'HazardRisk02',
                    COUNT(CASE WHEN t.risk = 'HazardRisk03' THEN 1 END) as 'HazardRisk03'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, ms.risk
                        FROM mst_location lc
                        LEFT JOIN trx_hazard ms on ms.location_id=lc.id
                        WHERE lc.parent_id<>0". $where ."     
                    ) t
                    GROUP BY t.id
                ) jo ON loc.id = jo.parent_id
                GROUP BY loc.id
                UNION
                SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.risk = 'HazardRisk01' THEN 1 END) as 'Cedera Parah',
                COUNT(CASE WHEN t.risk = 'HazardRisk02' THEN 1 END) as 'Cedera Sedang',
                COUNT(CASE WHEN t.risk = 'HazardRisk03' THEN 1 END) as 'Cedera Ringan'
                FROM
                (
                    SELECT lc.id, lc.location_name, ms.risk
                    FROM mst_location lc
                    LEFT JOIN trx_hazard ms on ms.location_id=lc.id
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0". $where ."     
                ) t
                GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Hazard By Category', 
                'link'=>'category-by-location',
                'model'=>$model, 'dataProvider' => $dataProvider]);
        }  
    }
    
    public function actionCategoryByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.risk = 'HazardRisk01' THEN 1 END) as 'Cedera Parah',
                COUNT(CASE WHEN t.risk = 'HazardRisk02' THEN 1 END) as 'Cedera Sedang',
                COUNT(CASE WHEN t.risk = 'HazardRisk03' THEN 1 END) as 'Cedera Ringan'
                FROM
                (
                    SELECT lc.id, lc.location_name, ms.risk
                    FROM mst_location lc
                    LEFT JOIN trx_hazard ms on ms.location_id=lc.id
                    WHERE lc.parent_id=".$id . $where ."  
                ) t
                GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Hazard By Category at Location :: '.$location->location_name,
                    'link'=>'category-by-user',
                    'model'=>$model, 
                    'dataProvider' => $dataProvider
                ]);
            } 
        }else {
            return $this->redirect(['category-by-user','id'=>$id]);
        }
    }
    
    public function actionCategoryByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT ms.created_by as id, up.full_name as 'Nama User',
                COUNT(CASE WHEN ms.risk = 'HazardRisk01' THEN 1 END) as 'Cedera Parah',
                COUNT(CASE WHEN ms.risk = 'HazardRisk02' THEN 1 END) as 'Cedera Sedang',
                COUNT(CASE WHEN ms.risk = 'HazardRisk03' THEN 1 END) as 'Cedera Ringan'
                FROM mst_user_profile up
                LEFT JOIN trx_hazard ms ON ms.created_by=up.user_id
                WHERE ms.location_id=".$id . $where ." 
                GROUP BY ms.created_by ORDER BY up.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('by-category', [
                'title'=>'Hazard Category by User :: '.$location->location_name, 
                'link'=>'category-by-post',
                'locId'=>$id,
                'model'=>$model, 
                'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionCategoryByPost($id, $locId) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND t.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND t.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT t.id, t.created_at as 'Date', t.title,
                COUNT(CASE WHEN t.risk = 'HazardRisk01' THEN 1 END) as 'Cedera Parah',
                COUNT(CASE WHEN t.risk = 'HazardRisk02' THEN 1 END) as 'Cedera Sedang',
                COUNT(CASE WHEN t.risk = 'HazardRisk03' THEN 1 END) as 'Cedera Ringan',
                COUNT(cm.hazard_id) as 'FU'
                FROM trx_hazard t
                LEFT JOIN trx_hazard_comment cm ON cm.hazard_id=t.id
                WHERE t.created_by=".$id ." AND t.location_id=".$locId. $where ." 
                GROUP BY t.id 
                ORDER BY t.created_at DESC";
        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        return $this->render('by-post', ['title'=>'Hazard category Up By :: '. $user->full_name, 'model'=>$model, 'dataProvider' => $dataProvider]);
    }
    
    public function actionByType() {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }
        
        $query = "SELECT loc.id, loc.location_name as 'Nama Lokasi', 
                SUM(jo.OPEN) as 'OPEN', 
                SUM(jo.IN_PROGRESS) as 'IN_PROGRESS', 
                SUM(jo.CLOSE) as 'CLOSE'
                FROM mst_location loc 
                INNER JOIN(
                    SELECT t.id, t.parent_id,
                    COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                    COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                    COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                    FROM
                    (
                        SELECT lc.id, lc.parent_id, ms.status
                        FROM mst_location lc
                        LEFT JOIN trx_hazard ms on lc.id = ms.location_id   
                        WHERE lc.parent_id<>0". $where ."  
                    ) t
                    GROUP BY t.id
                ) jo ON loc.id = jo.parent_id
                GROUP BY loc.id
                UNION
                SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT lc.id, lc.location_name, ms.status
                    FROM mst_location lc
                    LEFT JOIN trx_hazard ms on lc.id = ms.location_id   
                    WHERE lc.id NOT IN (SELECT parent_id FROM mst_location WHERE parent_id <>0) AND lc.parent_id=0". $where ."  
                ) t
                GROUP BY t.id";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();
        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Hazard By Type',
                'link'=>'type-by-location',
                'model'=>$model, 
                'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionTypeByLocation($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $locationCount = \app\models\MstLocation::find()->where(['parent_id'=>$id])->count();
        if($locationCount > 0){
            $location = \app\models\MstLocation::findOne($id);
            $query = "SELECT t.id, t.location_name as 'Nama Lokasi',
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT lc.id, lc.location_name, ms.status
                    FROM mst_location lc
                    LEFT JOIN trx_hazard ms on ms.location_id=lc.id
                    WHERE lc.parent_id=".$id . $where ."  
                ) t
                GROUP BY t.id";

            $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

            if(Yii::$app->request->post('search')=='export'){
                return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
            }else{
                return $this->render('index', [
                    'title'=>'Hazard By Type at Location :: '.$location->location_name,
                    'link'=>'type-by-user',
                    'model'=>$model, 
                    'dataProvider' => $dataProvider
                ]);
            } 
        }else {
            return $this->redirect(['type-by-user','id'=>$id]);
        }
    }
    
    public function actionTypeByUser($id) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND ms.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND ms.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $location = \app\models\MstLocation::findOne($id);
        $query = "SELECT t.user_id as id, t.full_name as 'Nama User',
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE'
                FROM
                (
                    SELECT up.user_id, up.full_name, ms.status
                    FROM mst_user_profile up
                    INNER JOIN trx_hazard ms ON ms.created_by=up.user_id
                    WHERE ms.location_id=".$id . $where ." 
                ) t
                GROUP BY t.user_id ORDER BY t.full_name";

        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        if(Yii::$app->request->post('search')=='export'){
            return $this->renderPartial('//report/_excel', ['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'title'=>'Hazard Type by User :: '.$location->location_name, 
                'link'=>'type-by-post',
                'locId'=>$id,
                'model'=>$model, 
                'dataProvider' => $dataProvider]);
        } 
    }
    
    public function actionTypeByPost($id, $locId) {
        $model = new ReportForm();
        $where =" ";
        if ($model->load(Yii::$app->request->post())){
            $where =" AND t.created_at >='". date('Y-m-d',strtotime($model->tgl_awal))."' AND t.created_at <='". date('Y-m-d',strtotime($model->tgl_akhir))."' ";
        }     
        $user = \app\models\MstUserProfile::find()->where(['user_id'=>$id])->one();
        $query = "SELECT t.id, t.created_at as 'Date', t.title,
                COUNT(CASE WHEN t.status = 'OPEN' THEN 1 END) as 'OPEN',
                COUNT(CASE WHEN t.status = 'IN_PROGRESS' THEN 1 END) as 'IN_PROGRESS',
                COUNT(CASE WHEN t.status = 'CLOSE' THEN 1 END) as 'CLOSE',
                COUNT(cm.hazard_id) as 'FU'
                FROM trx_hazard t
                LEFT JOIN trx_hazard_comment cm ON cm.hazard_id=t.id
                WHERE t.created_by=".$id ." AND t.location_id=".$locId. $where ." 
                GROUP BY t.id 
                ORDER BY t.created_at DESC";
        
        $dataProvider = Yii::$app->db->createCommand($query)->queryAll();

        return $this->render('by-post', ['title'=>'Hazard Type Up By :: '. $user->full_name, 'model'=>$model, 'dataProvider' => $dataProvider]);
    }
}
