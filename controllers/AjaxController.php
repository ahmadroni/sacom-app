<?php

namespace app\controllers;

use Yii;
use app\models\MstPropinsi;
use app\models\MstKota;
use app\models\MstKecamatan;
use app\models\MstDesa;
use app\models\MstLookup;
use app\models\MstCustomer;
use app\models\MstCompany;
use app\models\search\MstCustomerSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * InstansiController implements the CRUD actions for MstInstansi model.
 */
class AjaxController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all MstInstansi models.
     * @return mixed
     */
    public function actionGetLookup($type) {
        $model = \app\models\MstLookup::listData($type);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $model, 'status' => 'success'];
        return $result;
    }
    
    public function actionGetLookupMap($codeFrom, $codeTo) {
        $model = \app\models\MstLookupMap::find()->where(['code_from' => $codeFrom, 'code_to'=>$codeTo])->one();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $model, 'status' => 'success'];
        return $result;
    }
    

    /**
     * Lists all MstInstansi models.
     * @return mixed
     */
    public function actionGetPropinsi() {
        $model = MstPropinsi::find()->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['result' => $model, 'status' => 'success'];
        return $result;
    }

    public function actionGetKota($id) {
        $model = MstKota::find()->where(['propinsi_id' => $id])->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $model, 'status' => 'success'];
        return $result;
    }
    
    public function actionGetCustomerById($id) {
        $query="SELECT c.*, p.nama_propinsi, k.nama_kota FROM mst_customer c "
                . "INNER JOIN mst_propinsi p ON p.id=c.propinsi_id "
                . "INNER JOIN mst_kota k ON k.id=c.kota_id "
                . "WHERE c.id=".$id;
        $data = Yii::$app->db->createCommand($query)->queryOne();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $data, 'status' => 'success'];
        return $result;
    }
    
    public function actionGetCompanyById($id) {
        $query="SELECT c.*, p.nama_propinsi, k.nama_kota FROM mst_company c "
                . "INNER JOIN mst_propinsi p ON p.id=c.propinsi_id "
                . "INNER JOIN mst_kota k ON k.id=c.kota_id "
                . "WHERE c.id=".$id;
        $data = Yii::$app->db->createCommand($query)->queryOne();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $data, 'status' => 'success'];
        return $result;
    }
    
    public function actionCustomerToCompany($id){
        $customer = MstCustomer::findOne($id);
        
        $data=null;
        $status='failed';
        $items= array();
        if($customer !== null){
            $company = new MstCompany();
            $company->company_name = ($customer->category==null ? $customer->customer_name :$customer->category->name);
            $company->company_desc = 'Copy From Customer';
            $company->company_type = MstCompany::CALL_CENTER_TYPE;
            $company->propinsi_id = $customer->propinsi_id;
            $company->kota_id = $customer->kota_id;
            $company->address = $customer->address;
            $company->email = $customer->email;
            $company->no_telp = $customer->nomor_telp;
            $company->is_active = 1;
            if($company->save()){
                $data =$company;
                $status='success';
                $items = MstCompany::listDataByType(MstCompany::CALL_CENTER_TYPE);
            }
        }        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $data, 'status' => $status,'items'=>$items];
        return $result;
    }

    public function actionGetKecamatan($id) {
        $model = MstKecamatan::find()->where(['kota_id' => $id])->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $model, 'status' => 'success'];
        return $result;
    }

    public function actionGetDesa($id) {
        $model = MstDesa::find()->where(['kecamatan_id' => $id])->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['data' => $model, 'status' => 'success'];
        return $result;
    }

    public function actionGetCustomer() {
        $searchModel = new MstCustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($searchModel->load(Yii::$app->request->post())) {
            $searchModel->setAttributes(Yii::$app->request->post());
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }
        return $this->renderPartial('_customer-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionGetCompany() {
        $searchModel = new \app\models\search\MstCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($searchModel->load(Yii::$app->request->post())) {
            $searchModel->setAttributes(Yii::$app->request->post());
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }
        return $this->renderPartial('_company-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
     public function actionAddLookup($type){
        $model = new MstLookup();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $items = MstLookup::listData($type);
            $result = ['data' => $model,'items'=>$items,'status' => 'success'];
            return $result;
        }else{
            $query="SELECT max(code) as code, position FROM mst_lookup WHERE type='".$type."'";
            $data = Yii::$app->db->createCommand($query)->queryOne();
            $code = (int) filter_var($data['code'], FILTER_SANITIZE_NUMBER_INT);
            $code++;
            if($code <10)
                $model->code= $type."0".$code;
            else
                $model->code= $type.$code;

            $model->type = $type;
            $model->position = $data['position']+1;
        
            $title =($type=='CCCallType'?'Jenis Panggilan' : 'Jenis Media');
     
            return $this->renderPartial('_form-lookup',[
                'model'=>$model,
                'title'=>$title,
            ]);
        }
    }
    
    public function actionAddCustomer(){
        $model = new MstCustomer();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $items = MstCustomer::listData2();
            
            $result = ['data' => $model,'items'=>$items,'status' => 'success'];
            return $result;
        }
        return $this->renderPartial('_form-customer',[
            'model'=>$model,
        ]);
    }
    
    public function actionAddCompany(){
        $model = new MstCompany();
        $model->company_type = MstCompany::CALL_CENTER_TYPE;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $items = MstCompany::listDataByType(MstCompany::CALL_CENTER_TYPE);
            
            $result = ['data' => $model,'items'=>$items,'status' => 'success'];
            return $result;
        }
        return $this->renderPartial('_form-company',[
            'model'=>$model,
        ]);
    }

}
